//Videomocha Start
export const DOMAIN = 'http://video.mocha.com.vn';
export const DOMAIN_IMAGE_STATIC = 'http://live84.keeng.net/playnow/images/static/web/';
export const DEFAULT_IMG = DOMAIN_IMAGE_STATIC + "bg_lazy.jpg";
export const DEFAULT_AVATAR = DOMAIN_IMAGE_STATIC + "template/UserImg.png";

export const MV_SERVICE = 'http://apivideo.mocha.com.vn:8081/onMediaBackendBiz';
// export const MV_SERVICE = 'http://testapivideo.mocha.com.vn:8083/onMediaBackendBiz';

export const MV_SERVICE_UPLOAD = 'http://uploadvideo.mocha.com.vn:8085/onMediaBackendBiz';
export const API_UPDATE_VIDEO = MV_SERVICE + '/mochavideo/updateVideo';
export const API_LOGIN = MV_SERVICE + '/mochavideo/login';
export const API_CHANGEPASS = MV_SERVICE + '/mochavideo/changePassword';
export const API_TOP_VIDEO_OF_CATE = '/mochavideo/getTopVideoOfCate';
export const API_TOP_CHANNEL_OF_CATE = '/mochavideo/getTopChannelOfCate';
export const API_LIST_HIDDEN_CATEGORY = '/mochavideo/listHiddenCategory';
export const API_TOP_VIDEO_OF_FILM = '/mochavideo/getTopVideoOfFilm';
export const API_LIST_VIDEO_BY_CATE = '/mochavideo/listVideoByCate';
export const API_LIST_FILM_GROUPS = '/mochavideo/listFilmGroups';
export const API_LIST_VIDEO_BY_CHANNEL = '/mochavideo/listVideoByChannel';
export const API_LIST_VIDEO_MY_CHANNEL = '/mochavideo/listVideoMyChannel';
export const API_DEL_VIDEO_CHANNEL = '/mochavideo/delVideo';
export const API_GET_MY_CHANNEL_INFO = '/mochavideo/getMyChannelInfo';
export const API_GET_CHANNEL_INFO = '/mochavideo/getChannelInfo';
export const API_TOP_VIDEO_OF_CHANNEL = '/mochavideo/getTopVideoOfChannel';
export const API_LIST_FILM_OF_CHANNEL = '/mochavideo/listFilmGroupsOfChannel';
export const API_LIST_CHANNEL_FOLLOW_OF_CHANNEL = '/mochavideo/listChannelFollowOfChannel';
export const API_GET_CHANNEL_RELATED = '/mochavideo/listChannelRelated';
export const API_CREATE_CHANNEL = '/mochavideo/createChannel';
export const API_LIST_VIEO_NEWPUBLISH = '/mochavideo/listVideoNewPublish';
export const API_LIST_CHANNEL_FOLLOWED = '/mochavideo/listChannelfollowed';
export const API_GET_MY_CHANNEL = '/mochavideo/getMyChannelInfo';
export const API_TOP_VIDEO_OF_HOME = '/mochavideo/getTopVideoOfHome';
export const API_GET_CATEGORY_UPLOAD = '/mochavideo/getCategoryUpload';
export const API_UPLOAD_FILE = '/mochavideo/uploadFile';
export const API_UPLOAD_IMAGE = '/mochavideo/uploadImage';
export const API_CREATE_VIDEO_BY_USER = '/mochavideo/createVideoUploadByUser';
export const API_CREATE_VIDEO_BY_YOUTUBE = '/mochavideo/createVideoByLinkYoutube';
export const API_GET_STAT_OF_CHANNEL = '/mochavideo/getStatOfChannel';
export const API_DELETE_CACHE = '/onmedia/video/clearVideoOfChannel';
export const API_DELETE_CACHE_CHANNEL = '/onmedia/video/clearMemCacheChannelDetail';
export const API_FOLLOW_CHANNEL = '/mochavideo/followChannel';
export const API_GET_VIDEO_DETAIL = '/mochavideo/getVideoDetail';
export const API_GET_VIDEO_RELATED = '/onmedia/video/search/v1';
export const API_GET_FILM_RELATED = '/mochavideo/listSuggestPlayFilm';
export const API_LIST_VOLUME_FILM = '/mochavideo/listVolumeFilmGroups';

export const API_SEARCH_VIDEO = '/mochavideo/searchVideo';
export const API_SEARCH_VIDEO_OFCHANNEL = '/mochavideo/searchVideoOfChannel';
export const API_SEARCH_CHANNEL = '/mochavideo/searchChannel';
export const API_SEARCH_FILM = '/mochavideo/searchFilm';

export const MOCHA_GOOGLE_PLAY_URL = 'https://play.google.com/store/apps/details?id=com.viettel.mocha.app&hl=vi';
export const DECRYPT_KEY = 'test';
export const ID_CHANNEL_NOT_SHOW = 115626;

export const API_PUSH_ACTION = '/mochavideo/pushAction';
export const API_PUSH_ACTION_SHARE = '/mochavideo/pushActionShare';
export const ARR_TIME_LOG = [5, 15, 30];
export const API_PUSH_LOG = "/mochavideo/pushlog";
export const API_PUSH_LOG_TRAFFIC = "/mochavideo/pushLogTraffic";
export const IS_LOG_TRAFFIC = "IS_LOG_TRAFFIC";
export const API_GET_COMMENT = "/mochavideo/listComment";

export const API_GET_LOCATION = "http://location.keeng.net/ApiLocationKeengNet/ws/ip/getLocation?clientType=mocha_web";
export const MOCHAVIDEO_CLIENT_ID = "MOCHAVIDEO_CLIENT_ID";

export const USER_INFO = "userInfo";
export const EXPIRE_TIME = "expireTime";

//Videomocha End




//Proxy
export const HOST_PROXY= "10.61.76.22";
export const PORT_PROXY= 8888;
