import React from "react";
import CategoryApi from "../services/api/Category/CategoryApi";
import Helper from "../utils/helpers/Helper";
import Slider from "react-slick/lib";
//import {Link} from "../routes";
import Link from 'next/link';
import {DEFAULT_IMG} from "../config/Config";
import Router from "next/router";

var dragging = false;

class ComponentMore extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            videos: [],
            videoHai: '',
            idCate: [],
            lastIdStr: '',

            numberSlide: 4
        };
        this.linkTo = this.linkTo.bind(this);

    }

    componentDidMount() {
        CategoryApi.getVideoAll(this.props.item, 0, this.state.lastIdStr).then(
            ({data}) => {
                console.log(data);
                Helper.renewToken(data);

                this.setState({
                    isLoaded: true,
                    videos: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        );
    }
    linkTo(){
        Router.push(
            { pathname: '/categoryvideomocha'},
            "/" +this.props.categoryName + '-cg' + this.props.item + ".html"
        );
    }
    render() {
        const {videos, numberSlide} = this.state;
        videos.slice(11, 0 , "http://live84.keeng.net/playnow/images/static/web/Xemtatca.png");
        if (videos && videos.length > 0) {
            let setLength = videos.length;
            if (videos.length >= 10) {
                setLength = 11;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (

                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                            right: 0,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="box-video-list">
                    <h2 onClick={this.linkTo} className="bvl-h2-title" style={{fontSize: '25px'}}>
                        {this.props.categoryName.toUpperCase()}
                    </h2>

                    <div className="owl-slider-av">
                        <Slider {...settings}>
                            {this.showVideos(videos)}
                        </Slider>
                    </div>
                </div>
            );
        } else {
            return null;
        }


    }

    showVideos(videos) {
        return videos.map((video, index) => {
            videos = Helper.checkIsChannelNotShowVideo(videos);
            if (video && index < videos.length && index < 10) {
                return <div className="av-top-small swiper-slide" key={index}>
                    <div className="playnow-home-item-new">
                        <Link href="/video"
                              as={Helper.replaceUrl(video.link) + ".html?src=more"}
                        >
                            <a className="playnow-home-link-new"
                               onClick={(e) => {
                                   dragging && e.preventDefault();
                               }}
                               draggable={false}>
                                <img className="animated lazy"
                                     alt={video.name}
                                     title={video.name}
                                     data-original={video.image_path}
                                     src={video.image_path}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}
                                     style={{display: 'block'}}
                                     draggable={false}
                                />
                                <span className="icon-play-video"/>
                                <h3 className="playnow-home-h3-new" title={video.name}>
                                    {video.name}
                                </h3>
                            </a>

                        </Link>

                        {this.genInfoChannel(video)}
                    </div>
                </div>
            }else if (index == 10){
                return <div className="av-top-small swiper-slide" key={index}>
                    <div className="playnow-home-item-new">
                        <Link href="/categoryvideomocha" as={"/" +this.props.categoryName + '-cg' + this.props.item + ".html"} className="playnow-home-link-new">
                            <a className="playnow-home-link-new"
                               onClick={(e) => {
                                   dragging && e.preventDefault();
                               }}
                               draggable={false}>
                                <img className="animated lazy"
                                 src="http://live84.keeng.net/playnow/images/static/web/Xemtatca.png"
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                                 style={{display: 'block'}}
                                 draggable={false}
                            />
                            </a>
                        </Link>
                    </div>
                </div>
            }
        });
    }


    genInfoChannel(video) {

        if (Helper.checkArrNotEmpty(video, 'channels') && video.channels[0]) {
            return (
                <div className="playnow-home-chanel-new">

                    <Link href="/channel"
                          as={Helper.replaceUrl(video.channels[0].link) + '.html'}>
                        <a className="playnow-chanel-link-new"
                           onClick={(e) => {
                               dragging && e.preventDefault();
                           }}
                           draggable={false}>
                            <img alt={video.channels[0].name} className="lazy"
                                 src={video.channels[0].url_avatar ? video.channels[0].url_avatar : DEFAULT_IMG}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                                 style={{display: 'inline'}}/>
                        </a>

                    </Link>

                    <div className="play-chanel-info-new">
                        <p className="play-chanel-view-new">
                            {Helper.formatNumber(video.isView)} lượt xem • {Helper.getTimeAgo(video.DatePublish)}
                        </p>

                        <h3 className="play-channel-h3-new">
                            <Link href="/channel"
                                  as={Helper.replaceUrl(video.channels[0].link) + '.html'}
                            >
                                <a onClick={(e) => {
                                    dragging && e.preventDefault();
                                }}
                                   draggable={false}>
                                    {video.channels[0].name}
                                </a>

                            </Link>
                        </h3>

                    </div>
                    <a className="chanel-dot-more"/>
                </div>
            );
        }
        return null;
    }
}

export default ComponentMore;

