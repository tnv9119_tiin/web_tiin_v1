import React, {Component} from 'react';
import Router, {withRouter} from 'next/router';
//import "../../../styles/VideoMocha/VideoItem.css";
import HeaderMochaSuggestion from "../../pages/Home/HeaderMochaSuggestion";
import SearchApi from "../../services/api/Search/SearchApi";
import Helper from "../../utils/helpers/Helper";
import AuthService from "../../services/auth/AuthService";
import {ImageSSR} from "./ImageSSR/ImageSSR";
import LogApi from "../../services/api/Log/LogApi";
import {DOMAIN_IMAGE_STATIC, EXPIRE_TIME, IS_LOG_TRAFFIC, USER_INFO} from "../../config/Config";
import Link from 'next/link';

const logo_mocha_video_dv = DOMAIN_IMAGE_STATIC + "logo_mocha_video_dv.png";
const logo_mocha_video = DOMAIN_IMAGE_STATIC + "logo_mocha_video.png";
const avatarUserDefault = DOMAIN_IMAGE_STATIC + "phim_web_02.png";
const iconUpload = DOMAIN_IMAGE_STATIC + "mocha_video_03.png";
const iconApp = DOMAIN_IMAGE_STATIC + "mocha_video_02.png";


class Header extends Component {
    constructor(props) {
        super(props);

        console.log("constructor--");


        var values = this.props.router.query;
        var query = values.key ? values.key : '';

        this.state = {
            modal: false,
            isError: false,
            isLoggedIn: false,
            user: [],
            action: '',
            isVideo: false,
            query: query,
            isSuggestion: false,
            pathname: this.props.router.asPath,

            videos: [],
            channels: [],
            films: []
        };
        this.timeOutOnBlur = null;

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputKeyPress = this.handleInputKeyPress.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.checkUser = this.checkUser.bind(this);
        this.getMyChannel = this.getMyChannel.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    componentDidMount() {

        //Event keydown
        document.addEventListener('keydown', this.handleKeyPress);
        Helper.setClientId();
        let action = Helper.getActionLogTraffic(this.props.router.asPath);
        let isVideo = Helper.checkIsVideoDetail(this.props.router.asPath);

        console.log("componentDidMount--", isVideo);

        this.setState({
            action: action,
            isVideo: isVideo
        });
        let user = Helper.getUserInfo();
        let isLoggedIn = !!user;

        if (isLoggedIn) {
            //Check user đã login nhưng expire thì xóa cookie
            let expireTime = Helper.getCookie(EXPIRE_TIME);
            let currentTime = new Date().getTime();
            if (!expireTime || expireTime < currentTime) {
                isLoggedIn = false;
                Helper.removeCookie(USER_INFO);
                this.setState({
                    isLoggedIn: false,
                    user: null
                });
            }

            if (isLoggedIn) {
                this.setState({
                    isLoggedIn: true,
                    user: user
                });
            }
        }

        if (!this.state.isVideo) {
            this.pushLogTraffic();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let {router} = this.props;
        if (router.asPath !== prevState.pathname) {
            Helper.setClientId();
            let user = Helper.getUserInfo();
            let isLoggedIn = !!user;
            let isVideo = Helper.checkIsVideoDetail(router.asPath);

            if (isLoggedIn) {
                //Check user đã dectect hoặc login nhưng expire thì xóa cookie
                let expireTime = Helper.getCookie(EXPIRE_TIME);
                let currentTime = new Date().getTime();
                if (!expireTime || expireTime < currentTime) {
                    isLoggedIn = false;
                    Helper.removeCookie(USER_INFO);
                }
            }

            if (router.pathname !== '/search') {
                this.setState({
                    isLoggedIn: isLoggedIn,
                    user: isLoggedIn ? user : null,
                    isVideo: isVideo,

                    pathname: router.asPath,
                    query: '',
                    videos: [],
                    channels: [],
                    films: []
                });
            } else {
                this.setState({
                    isLoggedIn: isLoggedIn,
                    user: isLoggedIn ? user : null,
                    isVideo: isVideo,

                    pathname: router.asPath,
                    videos: [],
                    channels: [],
                    films: []
                });
            }
        }
    }

    checkUser() {
        let channel = Helper.getUserInfo();
        if (Helper.checkObjExist(channel, 'channelInfo')) {
            Router.push(
                {pathname: '/uploadvideo'},
                'uploadvideo.html'
            );
        } else {
            Router.push(
                {pathname: '/createChannel'},
                'createChannel.html'
            );

        }
    }

    getDataSearch() {
        SearchApi.searchVideo(this.state.query).then(
            ({data}) => {
                Helper.renewToken(data);
                this.setState({
                    isSuggestion: true,
                    videos: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });

        SearchApi.searchChannel(this.state.query).then(
            ({data}) => {
                Helper.renewToken(data);
                this.setState({
                    isSuggestion: true,
                    channels: Helper.checkArrNotEmpty(data, 'data.listChannel') ? data.data.listChannel : []
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });

        SearchApi.searchFilm(this.state.query).then(
            ({data}) => {
                Helper.renewToken(data);
                this.setState({
                    isSuggestion: true,
                    films: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });
    }

    handleInputChange() {
        let query = this.search.value;
        let oldQuery = this.state.query;
        this.setState({
            query: query
        }, () => {
            if (query && query.trim() != oldQuery.trim() && query.length >= 2) {
                this.getDataSearch();
            } else if (query.length < 2) {
                this.setState({
                    isSuggestion: false
                });
            }
        });
    }

    handleInputKeyPress(e) {
        let query = this.state.query.trim();
        let result = query.replace(/[%]/g, function (m) {
            return '%' + m.charCodeAt(0).toString(16);
        });

        if (e.key === 'Enter' && query.length > 0) {
            Router.push({
                pathname: '/search',
                query: {key: result},
            }, `/search.html?key=${result}`);
        }
    }

    handleKeyPress(e) {
        if (e.keyCode === 27 || e.key === 'Enter') {

            this.setState({
                videos: [],
                channels: [],
                films: [],
                isSuggestion: false
            });
        }
    }

    _onFocus(e) {
        e.target.placeholder = "";

        var values = this.props.router.query;

        var query = values.key ? values.key : '';


        let isSuggest = this.state.query.length >= 2 && query === '';

        this.setState({
            isSuggestion: isSuggest
        });
    }

    _onBlur(e) {
        e.preventDefault();
        e.target.placeholder = "Tìm kiếm video...";
        this.timeOutOnBlur = setTimeout(() => {
                this.timeOutOnBlur = null;
                this.setState({
                    isSuggestion: false
                });
            }, 200
        );
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleKeyPress);
        if (this.timeOutOnBlur) {
            clearTimeout(this.timeOutOnBlur);
            this.timeOutOnBlur = null;
        }
    }

    getMyChannel() {
        let user = this.state.user;
        if (user && Helper.checkObjExist(user, 'channelInfo') && user.channelInfo.link) {
            Router.replace(
                {pathname: '/channel'},
                Helper.replaceUrl(user.channelInfo.link) + '.html',
                {shallow: true}
            );

        } else {
            Router.push(
                {pathname: '/createChannel'},
                'createChannel.html'
            );
        }
    }

    handleLogout() {
        this.setState({
            isLoggedIn: false,
            user: null
        });
        AuthService.logout();
    }

    pushLogTraffic() {
        if (!Helper.getCookie(IS_LOG_TRAFFIC)) {
            let msisdn = '';
            if (this.state.user && this.state.user.username) {
                msisdn = this.state.user.username;
            }
            let data = {
                "msisdn": msisdn,
                "platform": "WEB",
                "clientId": Helper.getClientId(),
                "currentTime": Helper.getDateTime(),
                "action": this.state.action,
                "ip": "",
                "url": window.location.href,
                "videoUrl": '',
                "useragent": Helper.getUserAgent(),
                "sessionId": ""
            };
            LogApi.pushLogTraffic(data);
        }
    }

    render() {

        const {user, isVideo, query, isLoggedIn, isSuggestion, videos, channels, films} = this.state;

        var self = this;
        return (
            <div id="header" className={isVideo ? 'active' : ''}>
                <div className="container-wrap">

                    <Link href='/' shallow={true}>
                        <a className="phim-keeng-logo"
                           title="http://video.mocha.com.vn - Mạng xã hội chia sẻ video">
                            <img alt="" className="pk-img-logo" src={isVideo ? logo_mocha_video_dv : logo_mocha_video}/>
                        </a>

                    </Link>

                    <div className="phim-keeng-search">
                        {/*<a id="button_search" className="pk-search-btn" onClick={self.SearchClick}></a>*/}
                        <input placeholder="Tìm kiếm video..."
                               className={"pk-search-txt searchInput " + (isVideo ? "placeholderDetail" : "placeholderHome")}
                               id="search_query" type="text" name="key" autoComplete="off"
                               autoFocus
                               onFocus={this._onFocus.bind(this)}
                               onBlur={this._onBlur.bind(this)}
                               value={query}
                               tabIndex={0}
                            // ref={this.textInput}
                               ref={input => this.search = input}
                               onChange={this.handleInputChange}
                               onKeyPress={(e) => this.handleInputKeyPress(e)}/>
                        {(function () {

                            if (query.length > 0) {
                                return (
                                    <Link href={'/search?key=' + query} as={'/search.html?key=' + query}>
                                        <a className="search-btn float-right"
                                           style={{marginTop: '2px', position: 'absolute', right: '0'}}>
                                            <div id="button_search"
                                                 className={isVideo ? "pk-search-btn-video" : "pk-search-btn"}/>
                                        </a>
                                    </Link>
                                );
                            } else {
                                return <div className="search-btn float-right"
                                            style={{marginTop: '2px', position: 'absolute', right: '0'}}>
                                    <a id="button_search"
                                       className={isVideo ? "pk-search-btn-video" : "pk-search-btn"}/>
                                </div>;
                            }

                        })()}

                    </div>

                    <div className="header-mocha-right">
                        {(
                            function () {
                                if (isLoggedIn) {
                                    return (
                                        <p className="phim-keeng-login" style={{marginBottom: '0'}}>
                                            <a className="header-avatar">
                                                <ImageSSR
                                                    src={Helper.checkObjExist(user, 'channelInfo.url_avatar') ? user.channelInfo.url_avatar : avatarUserDefault}
                                                    fallbackSrc={avatarUserDefault}
                                                />
                                                {Helper.checkObjExist(user, 'channelInfo.name') ? user.channelInfo.name : ''}
                                            </a>
                                            <span className="sp-info-user">
                                                 <a id="channelmanagement" onClick={self.getMyChannel}>Kênh của tôi</a>
                                                 <Link href="/profile" as="/profile.html">
                                                     <a>Cài đặt</a>
                                                 </Link>
                                                 <a onClick={self.handleLogout}>Đăng xuất</a>
                                            </span>
                                        </p>
                                    );
                                } else {
                                    return (
                                        <p className="phim-keeng-login" style={{marginBottom: '0'}}>
                                            <Link href="/login" as="/login.html">
                                                <a>
                                                    <img src={avatarUserDefault}/>Đăng nhập
                                                </a>

                                            </Link>

                                        </p>
                                    );
                                }
                            })()}
                        <Link href={isLoggedIn ? '/uploadvideo' : '/login'}
                              as={isLoggedIn ? '/uploadvideo.html' : '/login.html'}>
                            <a className="hmr-phone hmr-down" title="Upload video" id="a_upload">
                                <img src={iconUpload}/>
                            </a>

                        </Link>
                        <Link href="/gioithieuapp" as="/gioithieuapp.html">
                            <a className="hmr-phone" title="giới thiệu app">
                                <img src={iconApp}/>
                            </a>

                        </Link>
                    </div>
                    <div className="clear"/>
                    <div style={{display: 'none'}} id="suggestions" className="suggestion"/>
                </div>

                {(function () {
                    if (isSuggestion && query.length && videos.length === 0 && channels.length === 0 && films.length === 0) {
                        return <div className="suggestion-2" id="suggestions" style={{display: 'block'}}>
                            <div className="text-center" style={{
                                // margin: '30px 10px 30px 10px',
                                fontSize: 15
                            }}>{'Không tìm thấy kết quả với từ khóa "' + query + '"'}</div>
                        </div>;
                    } else if (isSuggestion) {
                        // } else if (true) {
                        return <HeaderMochaSuggestion videos={Helper.checkIsChannelNotShowVideo(videos)} query={query}
                                                      channels={Helper.checkIsChannelNotShow(channels)}
                                                      films={films}/>;
                    }
                })()}

            </div>

        )
    }
}

export default withRouter(Header);


