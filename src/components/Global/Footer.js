import React, { Component } from 'react';
//import '../../../styles/VideoMocha/VideoItem.css';
import Link from 'next/link';

import {DOMAIN_IMAGE_STATIC} from "../../config/Config";
const footer_href_mocha = DOMAIN_IMAGE_STATIC + "footer_href_mocha.png";
const footer_href_keeng = DOMAIN_IMAGE_STATIC + "footer_href_keeng.png";
const footer_href_tiin = DOMAIN_IMAGE_STATIC + "footer_href_tiin.png";
const footer_href_netnews = DOMAIN_IMAGE_STATIC + "footer_href_netnews.png";
const footer_href_songkhoe = DOMAIN_IMAGE_STATIC + "footer_href_songkhoe.png";
const footer_href_vtstudy = DOMAIN_IMAGE_STATIC + "footer_href_vtstudy.png";
const logo_footer = DOMAIN_IMAGE_STATIC + "logo_footer.png";
const footer_downapp_android = DOMAIN_IMAGE_STATIC + "footer_downapp_android.png";
const footer_downapp_ios = DOMAIN_IMAGE_STATIC + "footer_downapp_ios.png";

export default class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <div id="footer">
                <div className="footer-top">
                    <div className="footer-menu">
                        <ul className="footer-menu-ul">
                            <li>
                                <Link href='/category'
                                      as='/moi-cap-nhat-cg1002.html'>
                                    <a>Mới</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/phim-cg10.html'>
                                    <a>Phim</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/the-thao-cg3.html'
                                >
                                    <a>Thể thao</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/sao-cg2.html'
                                >
                                    <a>Sao</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/thoi-cuoc-cg1.html'>
                                    <a>Thời cuộc</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='category'
                                      as='/nhac-cg4.html'>
                                    <a>Nhạc</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/danghot-cg1001.html'
                                >
                                    <a>Hot</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/hai-cg7.html'
                                >
                                    <a>Hài</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/song-cg1010.html'
                                >
                                    <a>Sống</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/dep-cg1020.html'
                                >
                                    <a>Đẹp</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/dan-choi-cg6.html'>
                                    <a>Dân chơi</a>
                                </Link>
                            </li>
                            <li>
                                <Link href='/category'
                                      as='/tips-cg5.html'
                                >
                                    <a>Tips</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="footer-logo-other">
                        <a href="http://video.mocha.com.vn/gioithieuapp" rel="noopener noreferrer" target="_blank">
                            <img src={footer_href_mocha}/></a>
                        <a href="http://keeng.vn/" rel="noopener noreferrer" target="_blank">
                            <img src={footer_href_keeng}/></a>
                        <a href="http://tiin.vn/" rel="noopener noreferrer" target="_blank">
                            <img src={footer_href_tiin}/></a>
                        <a href="http://netnews.vn/" rel="noopener noreferrer" target="_blank">
                            <img src={footer_href_netnews}/></a>
                        <a href="http://songkhoe.vn" rel="noopener noreferrer" target="_blank">
                            <img src={footer_href_songkhoe}/></a>
                        <a href="https://viettelstudy.vn/" rel="noopener noreferrer" target="_blank">
                            <img src={footer_href_vtstudy}/></a>
                    </div>
                </div>
                <div className="footer-bottom">
                    <a href="#" className="logo-footer">
                        <img src={logo_footer}/>
                    </a>
                    {/*<p style={pgovstyle}>*/}
                    <p className="footer_gov">
                        <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=35838" target="_blank">
                            <img width="120" alt="" title="" src="http://online.gov.vn/seals/hJ6YkwTGHdtBRbeauUV0Og==.jpgx"/>
                        </a>
                    </p>
                    <div className="footer-info">
                        <h3 style={{fontWeight: 'bold'}}>
                            Mạng xã hội Mocha</h3>
                        Cơ quan chủ quản: Công ty Truyền thông Viettel (Viettel Media) – Chi nhánh Tập đoàn Công nghiệp – Viễn
                        thông Quân đội.<br/>
                        Trụ sở: Tầng 4, Tòa nhà The Light (CT2 Trung Văn), đường Tố Hữu, Nam Từ Liêm, Hà
                        Nội<br/>
                        Giấy phép: Số 217/GXN-TTĐT do Cục QL Phát Thanh Truyền Hình và TTĐT cấp ngày 28/12/2011<br/>
                        Chịu trách nhiệm nội dung: Ông Võ Thanh Hải<br/>
                        Chăm sóc khách hàng: 198 (Miễn phí) | Quảng cáo:hoptac@viettel.com.vn<br/>
                    </div>
                    <div className="footer-app-keeng">
                        <h3 className="app-keeng-title">
                            Tải ứng dụng</h3>
                        <p className="app-keeng-p">
                            <a href="http://mocha.com.vn/app?ref=clientID" target="_blank">
                                <img src={footer_downapp_android}/></a>
                            <a href="https://itunes.apple.com/vn/app/mocha-sms-call-out-tho%E1%BA%A3i-m%C3%A1i/id946275483?l=vi&mt=8"
                               target="_blank">
                                <img src={footer_downapp_ios}/></a>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}

