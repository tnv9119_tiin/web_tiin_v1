let isImageValid = src => {
    return new Promise(resolve => {
        let img = document.createElement("img");
        img.onerror = () => resolve(false);
        img.onload = () => resolve(true);
        img.src = src;
    });
};

export let ImageSSR = ({ src, fallbackSrc, alt, ...rest }) => {
    const imageEl = React.useRef(null);
    React.useEffect(() => {
        isImageValid(src).then(isValid => {
            if (!isValid) {
                if(imageEl && imageEl.current)
                    imageEl.current.src = fallbackSrc;
            }
        });
    }, [src]);

    return <img {...rest} alt={alt} ref={imageEl} src={src}/>;
};

