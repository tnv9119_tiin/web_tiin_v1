import React, {Component} from 'react';
// /import {Link, Router} from '../../routes';
import Link from 'next/link';
import Router,{ withRouter } from 'next/router';
import queryString from 'query-string';
import HeaderMochaSuggestion from "../../pages/Home/HeaderMochaSuggestion";
import logo_mocha_video_dv from "../../../styles/images/logo_mocha_video_dv.png";

import avatarUserDefault from "../../../styles/images/phim_web_02.png";
import iconUpload from "../../../styles/images/mocha_video_03.png";
import iconApp from "../../../styles/images/mocha_video_02.png";
import SearchApi from "../../services/api/Search/SearchApi";
import Helper from "../../utils/helpers/Helper";
import AuthService from "../../services/auth/AuthService";
import {ImageSSR} from "./ImageSSR/ImageSSR";
import {DEFAULT_AVATAR} from "../../config/Config";


class HeaderDetailVideoMocha extends Component {
    constructor(props) {
        super(props);
        var values = queryString.parse(this.props.router.query);
        var query = values.key ? values.key : '';

        this.state = {
            modal: false,
            isError: false,
            isLoggedIn: false,
            user: [],

            query: query,
            isSuggestion: true,
            pathname: this.props.router.pathname,

            videos: [],
            channels: [],
            films: []
        };
        this.timeOutOnBlur = null;

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputKeyPress = this.handleInputKeyPress.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.checkUser = this.checkUser.bind(this);
        this.getMyChannel = this.getMyChannel.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    componentDidMount() {
        //Event keydown
        document.addEventListener('keydown', this.handleKeyPress);
        Helper.setClientId();
        let user = Helper.getUserInfo();
        if (user) {
            this.setState({
                isLoggedIn: true,
                user: user
            });
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let {router} =this.props;
        if (router.pathname !== prevState.pathname) {
            Helper.setClientId();
            let user = Helper.getUserInfo();
            let isLoggedIn = !!user;
            // this.search.focus();
            if (router.asPath !== '/search') {
                this.setState({
                    isLoggedIn: isLoggedIn,
                    user: user,

                    pathname: router.asPath,
                    query: '',
                    videos: [],
                    channels: [],
                    films: []
                });
            } else {
                this.setState({
                    isLoggedIn: isLoggedIn,
                    user: user,

                    pathname: router.asPath,
                    videos: [],
                    channels: [],
                    films: []
                });
            }
        }
    }
    checkUser(){
        let channel = Helper.getUserInfo();
        if (Helper.checkObjExist(channel,'channelInfo')){
            Router.push(
                { pathname: '/uploadvideo'},
                'uploadvideo.html'
            );
        } else {
            Router.push(
                { pathname: '/createChannel'},
                'createChannel.html'
            );
        }
    }
    getDataSearch() {
        SearchApi.searchVideo(this.state.query).then(
            ({data}) => {
                this.setState({
                    isSuggestion: true,
                    videos: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });

        SearchApi.searchChannel(this.state.query).then(
            ({data}) => {
                this.setState({
                    isSuggestion: true,
                    channels: Helper.checkArrNotEmpty(data, 'data.listChannel') ? data.data.listChannel : []
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });

        SearchApi.searchFilm(this.state.query).then(
            ({data}) => {
                this.setState({
                    isSuggestion: true,
                    films: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            },
            (error) => {
                this.setState({
                    isError: true
                });
            });
    }

    handleInputChange() {
        let query = this.search.value;
        this.setState({
            query: query
        }, () => {
            if (query && query.length >= 2) {
                this.getDataSearch();
            } else if (query.length < 2) {
                this.setState({
                    isSuggestion: false
                });
            }
        });
    }

    handleInputKeyPress(e) {
        if (e.key === 'Enter' && this.state.query.trim().length > 0) {

            Router.push({
                pathname: '/search',
                query: { key: this.state.query.trim() },
            }, `/search.html?key=${this.state.query.trim()}`);

        }
    }

    handleKeyPress(e) {
        if (e.keyCode === 27 || e.key === 'Enter') {
            //console.log('Esc');
            this.setState({
                videos: [],
                channels: [],
                films: [],
                isSuggestion: false
            });
        }
    }

    _onFocus(e) {
        e.target.placeholder = "";

        var values = queryString.parse(this.props.router.query);
        var query = values.key ? values.key : '';
        let isSuggest = this.state.query.length >= 2 && query === '';

        this.setState({
            isSuggestion: isSuggest
        });
    }

    _onBlur(e) {
        e.preventDefault();
        e.target.placeholder = "Tìm kiếm video...";
        this.timeOutOnBlur = setTimeout(() => {
                this.timeOutOnBlur = null;
                this.setState({
                    isSuggestion: false
                });
            }, 200
        );
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleKeyPress);
        if (this.timeOutOnBlur) {
            clearTimeout(this.timeOutOnBlur);
            this.timeOutOnBlur = null;
        }
    }

    getMyChannel() {
        let user = this.state.user;
        if (user && Helper.checkObjExist(user, 'channelInfo') && user.channelInfo.link) {
            Router.push(
                { pathname: Helper.replaceUrl(user.channelInfo.link)},
                Helper.replaceUrl(user.channelInfo.link)+'.html'
            );

        } else {
            Router.push(
                { pathname: '/createChannel'},
                'createChannel.html'
            );
        }
    }

    handleLogout() {
        AuthService.logout();
    }

    render() {
        const {user, query, isLoggedIn, isSuggestion, videos, channels, films} = this.state;
        let regex = /^(.+)-v([0-9]+)/;
        if (regex.test(this.props.router.asPath)) {
            var isVideo = true;
        }
        var self = this;
        return (
            <div id="header" className="active">
                <div className="container-wrap">
                    <Link href='/' shallow={true}>
                        <a  className="phim-keeng-logo"
                            alt="http://video.mocha.com.vn - Mạng xã hội chia sẻ video"
                            title="http://video.mocha.com.vn - Mạng xã hội chia sẻ video">
                            <img alt="" className="pk-img-logo" src={logo_mocha_video_dv}/>
                        </a>
                    </Link>

                    <div className="phim-keeng-search">
                        {/*<a id="button_search" className="pk-search-btn" onClick={self.SearchClick}></a>*/}
                        <input placeholder="Tìm kiếm video..." className="pk-search-txt searchInput placeholderDetail"
                               id="search_query" type="text" name="key" autoComplete="off"
                               autoFocus
                               onFocus={this._onFocus.bind(this)}
                               onBlur={this._onBlur.bind(this)}
                               value={query}
                               tabIndex={0}
                            // ref={this.textInput}
                               ref={input => this.search = input}
                               onChange={this.handleInputChange}
                               onKeyPress={(e) => this.handleInputKeyPress(e)}/>
                        {(function () {

                            if (query.length > 0) {
                                return <Link href={'/search?key=' + query}
                                             as={'/search.html?key=' + query}
                                >
                                    <a className="search-btn float-right"  style={{marginTop: '2px', position: 'absolute', right: '0'}} >
                                        <a id="button_search" className={isVideo ? "pk-search-btn-video" : "pk-search-btn"}/>
                                    </a>

                                </Link>;
                            } else {
                                return <div className="search-btn float-right"
                                            style={{marginTop: '2px', position: 'absolute', right: '0'}}>
                                    <a id="button_search" className={isVideo ? "pk-search-btn-video" : "pk-search-btn"}/>
                                </div>;
                            }
                            up
                        })()}

                    </div>

                    <div className="header-mocha-right">
                        {(
                            function () {
                                if (isLoggedIn) {
                                    return (
                                        <p className="phim-keeng-login" style={{marginBottom: '0'}}>
                                            <a className="header-avatar">
                                                <ImageSSR
                                                    src={Helper.checkObjExist(user, 'channelInfo.url_avatar') ? user.channelInfo.url_avatar : avatarUserDefault}
                                                    fallbackSrc={avatarUserDefault}
                                                />
                                                {Helper.checkObjExist(user, 'channelInfo.name') ? user.channelInfo.name : ''}
                                            </a>
                                            <span className="sp-info-user">
                                                <a href="" onClick={self.getMyChannel}>Kênh của tôi</a>
                                                <Link href="/profile" as ="/profile.html">
                                                    <a>
                                                         Cài đặt
                                                    </a>
                                                </Link>
                                                <a href="" onClick={self.handleLogout}>Đăng xuất</a>
                                            </span>
                                        </p>
                                    );
                                } else {
                                    return (
                                        <p className="phim-keeng-login" style={{marginBottom: '0'}}>
                                            <Link href="/login" as="/login.html">
                                                <a>
                                                    <img src={avatarUserDefault}/>
                                                    Đăng nhập
                                                </a>
                                            </Link>
                                        </p>
                                    );
                                }
                            })()}
                        <a onClick={this.checkUser.bind(this)} className="hmr-phone hmr-down" title="Upload video" id="a_upload">
                            <img src={iconUpload}/>
                        </a>
                        <Link  href="/gioithieuapp"  as="/gioithieuapp.html" >
                            <a title="giới thiệu app" className="hmr-phone">
                                <img src={iconApp}/>
                            </a>

                        </Link>
                    </div>
                    <div className="clear"/>
                    <div style={{display: 'none'}} id="suggestions" className="suggestion"/>
                </div>

                {(function () {
                    if (isSuggestion && query.length && videos.length === 0 && channels.length === 0 && films.length === 0) {
                        return <div className="suggestion-2" id="suggestions" style={{display: 'block'}}>
                            <div className="text-center" style={{
                                // margin: '30px 10px 30px 10px',
                                fontSize: 15
                            }}>{'Không tìm thấy kết quả với từ khóa "' + query + '"'}</div>
                        </div>;
                    } else if (isSuggestion) {
                        // } else if (true) {
                        return <HeaderMochaSuggestion videos={videos} query={query} channels={channels}
                                                      films={films}/>;
                    }
                })()}

            </div>

        )
    }
}

export  default  withRouter(HeaderDetailVideoMocha);


