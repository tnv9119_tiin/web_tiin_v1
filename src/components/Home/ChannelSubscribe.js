import React from 'react';
//import {Link} from "../../routes";
import Link from 'next/link';
import VideoItem from "../Video/partials/VideoItem";
import Slider from "react-slick/lib";
import Helper from "../../utils/helpers/Helper";
import {DEFAULT_IMG} from "../../config/Config";

var dragging = false;

class ChannelSubscribe extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            numberSlide: 7
        };
    }

    render() {
        var {channels, videos} = this.props;
        var {numberSlide} = this.state;

        if (channels && channels.length > 0) {
            let setLength = channels.length;
            // if (channels.length >= 10) {
            //     setLength = 10;
            // }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                            right: -13,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="mocha-video-box">
                    <div className="keeng-cate-hot" id="cate_hot">
                        <div className="wrap-bpk">
                            <h3 className="phim-keeng-h3">
                                <p>KÊNH THEO DÕI</p>
                            </h3>
                            <div className="cate-hot-wrap" id="cate_hot_swiper">
                                {/*<SlideResponsive video={channel} type={'boxChannelSearch'} numberItem={7}/>*/}

                                <div className="owl-slider-av">
                                    <Slider {...settings}>
                                        {this.showChannels(channels)}
                                    </Slider>
                                </div>

                            </div>
                        </div>
                    </div>

                    {videos && videos.length > 0 ?
                        <div className="mocha-video-hot">
                            <div className="wrap-bpk">
                                <div className="wrap-playnow-home">
                                    {this.showVideos(videos)}
                                </div>
                            </div>
                        </div>
                        : ''
                    }
                </div>

            );
        } else {
            return '';
        }
    }

    showVideos(videos) {
        return videos.map((video, index) => {
            if (index < 8) {
                return <VideoItem key={index} video={video}/>
            }
        });
    }

    showChannels(channels) {
        return channels.map((channel, index) => {
            if (channel && index < channels.length) {
                let link = Helper.replaceUrl(channel.link);

                return (
                    <div className="cate-hot-item swiper-slide swiper-slide-active" key={index}>
                        <Link href="/channel" as={link + '.html'}
                        >
                            <a title={channel.name}
                               onClick={(e) => {
                                   dragging && e.preventDefault();
                               }}
                               draggable={false}>
                                <img className="ats-img animated" alt={channel.name} src={channel.url_avatar}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}
                                />
                            </a>

                        </Link>

                        <Link href="/channel" as={link + '.html'}>
                            <a title={channel.name}
                               onClick={(e) => {
                                   dragging && e.preventDefault();
                               }}
                               draggable={false}>
                                <span className="sp-chanel-name">{channel.name}</span>
                                <span className="sp-chanel-follow">
                                    {Helper.formatNumber(channel.numfollow)} followers
                                </span>
                            </a>
                        </Link>
                    </div>

                );
            }
        });
    }

}

export default ChannelSubscribe;