import React, {Component} from 'react';
//import {Link} from "../../routes";
import Link from 'next/link';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
//import 'bootstrap/dist/css/bootstrap.css';
import { withRouter } from 'next/router';


class ModalLogin extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} style={{width: '340px', top: '15%'}}>
                {/*<ModalHeader toggle={this.toggle}></ModalHeader>*/}
                <ModalBody>
                    {this.props.body}
                </ModalBody>
                <ModalFooter className="popup-create-btn" style={{height: '10px'}}>

                    <div>
                        <Link href="/login" as="/login.html">
                            <a>
                                <p className="pcb-continue" style={{margin: '10px 10px 0px 0px'}}>
                                    OK
                                </p>
                            </a>
                        </Link>
                        <p onClick={this.props.toggle} className="pcb-continue">
                            Hủy
                        </p>
                    </div>

                </ModalFooter>
            </Modal>
        );
    }
}

export default withRouter(ModalLogin) ;
