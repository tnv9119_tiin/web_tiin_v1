import React from 'react';
import MyChannel from "./ManagementChannel/MyChannel";
import MyAccount from "./ManagementChannel/MyAccount";
import MyVideo from "./ManagementChannel/MyVideo";
import EditChannel from "./ManagementChannel/EditChannel";
import EditVideo from "./ManagementChannel/EditVideo";
import ChannelApi from "../../services/api/Channel/ChannelApi";
import Helper from "../../utils/helpers/Helper";

import {DOMAIN_IMAGE_STATIC} from "../../config/Config";
const ver4_31 = DOMAIN_IMAGE_STATIC + 'ver4_31.png';
const ver4_32 = DOMAIN_IMAGE_STATIC + 'ver4_32.png';
const ver4_33 = DOMAIN_IMAGE_STATIC + 'ver4_33.png';

class ChannelTabReport extends React.Component {
    constructor(props) {
        super(props);
        let id = this.props.match ? this.props.match.params.id : '';
        this.state = {
            tab: 1,
            item: '',
            idChannel: id,
            point: 0,
            channelRevenue: [],
            VideoRevenue:[]

        };
        this.editChannel = this.editChannel.bind(this);
        this.editVideo = this.editVideo.bind(this);
        this.callBack = this.callBack.bind(this);
        this.showTabVideo = this.showTabVideo.bind(this);
        this.callBackMyVideo = this.callBackMyVideo.bind(this);
    }

    handleClick = (e, tabId) => {
        this.setState({
            tab: tabId
        });
    }

    editChannel() {
        this.setState({
            tab: 2
        });
    }

    callBack(item) {
        this.setState({
            item: item,
            tab: 5
        });
    }

    callBackMyVideo(id) {
        this.setState({
            id: id,
            tab: 3
        });
    }

    editVideo() {
        this.setState({
            tab: 5
        });
    }

    showTabVideo() {
        this.setState({
            tab: 3
        });
    }

    componentDidMount() {
        ChannelApi.getStatOfChannel(this.props.idSearch).then(
            ({data}) => {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                this.setState({
                    isLoaded: true,
                    channelRevenue: Helper.checkObjExist(data, 'data.channelRevenue') ? data.data.channelRevenue : [],
                    VideoRevenue: Helper.checkObjExist(data, 'data.VideoRevenue') ? data.data.VideoRevenue : [],
                    point: Helper.checkObjExist(data, 'data.point') ? data.data.point : 0
                });
            },
            (error) => {
                this.setState({
                    error
                });
            }
        );
    }

    render() {
        const style_tab_1 = this.state.tab === 1 ? 'active' : '';
        const style_tab_2 = this.state.tab === 2 ? 'active' : '';
        const style_tab_3 = this.state.tab === 3 ? 'active' : '';
        const style_tab_4 = this.state.tab === 4 ? 'active' : '';
        const {infoChannel} = this.props;
        return (

            <div id="lstSub" style={{}}>
                <style dangerouslySetInnerHTML={{__html: "\n    #body{overflow:hidden !important;}\n"}}/>
                <div className="wrap-body-login">
                    <div className="persion-list box-shadow-cm">
                        <div className="menu-persion-wrap">
                            <h3 className="menu-persion-h3 menu-persion-title">
                                <a id="a_MyChannel" className={style_tab_1} onClick={((e) => this.handleClick(e, 1))}>
                                    KÊNH CỦA TÔI
                                </a>
                            </h3>
                            <ul>
                                { infoChannel.status && infoChannel.status == 2 ?
                                    <li><a id="a_editChannel" className={style_tab_2}
                                           onClick={((e) => this.handleClick(e, 2))}>
                                        <img src={ver4_31}/>
                                        Chỉnh sửa kênh</a></li> : ''
                                }
                                <li><a id="a_MyVideo" className={style_tab_3} style={{cursor: 'pointer'}}
                                       onClick={((e) => this.handleClick(e, 3))}>
                                    <img src={ver4_32}/>
                                    Quản lý video </a></li>
                                <li><a id="a_MyAccount" className={style_tab_4} style={{cursor: 'pointer'}}
                                       onClick={((e) => this.handleClick(e, 4))}>
                                    <img src={ver4_33}/>
                                    Thông tin tài khoản </a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="boxRight">
                        <div className="body-login-right">
                            {this.genTab()}
                        </div>
                    </div>
                </div>
                {/*<div className="popup-create-chanel" id="image_loadding" style={{display: 'none'}}>
					<img src="../../../styles/VideoMocha/images/image_loading.gif" style={{
						height: '40px',
						position: 'absolute',
						top: '50%',
						left: '50%',
						marginTop: '-20px',
						marginLeft: '-20px'
					}}/>
				</div>*/}
            </div>
        );
    }

    genTab() {
        if (this.state.tab === 1) {
            return (
                <MyChannel infoChannel={this.props.infoChannel} videoNew={this.props.videoNew}
                           editChannel={this.editChannel} editVideo={this.editVideo} callBack={this.callBack}
                           showTabVideo={this.showTabVideo}/>
            );
        } else if (this.state.tab === 2) {
            return (
                <EditChannel infoChannel={this.props.infoChannel} type={''} video={this.state.item}/>
            );
        } else if (this.state.tab === 3) {
            return (
                <MyVideo idSearch={this.props.idSearch} idVideo={this.state.id} callBack={this.callBack}
                         editVideo={this.editVideo} videoNew={this.props.videoNew}/>
            );
        } else if (this.state.tab === 4) {
            return (
                <MyAccount infoChannel={this.props.infoChannel} point={this.state.point} channelRevenue={this.state.channelRevenue} VideoRevenue={this.state.VideoRevenue}/>
            );
        } else if (this.state.tab === 5) {
            return (
                <EditVideo video={this.state.item} callBack={this.callBackMyVideo}/>
            );
        }
    }
}

export default ChannelTabReport;
