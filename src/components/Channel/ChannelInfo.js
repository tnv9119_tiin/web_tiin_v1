import React, {Component} from 'react';
import {DEFAULT_IMG} from "../../config/Config";
import Helper from "../../utils/helpers/Helper";
import ModalSubcribe from "../Modal/ModalSubcribe";
import ChannelApi from "../../services/api/Channel/ChannelApi";
import ModalLogin from "../Modal/ModalLogin";

class ChannelInfo extends Component {
    constructor(props) {
        super(props);
        var infoChannel = this.props.infoChannel;

        this.state = {
            isShowLogin: false,
            isShowSubcribe: false,

            isLoggedIn: false,
            idChannel: infoChannel.id,
            isFollow: infoChannel.is_follow,
            numberFollow: infoChannel.numfollow,
        };

        this.handleFollow = this.handleFollow.bind(this);
        this.handleUnfollow = this.handleUnfollow.bind(this);
        this.callbackUnfollow = this.callbackUnfollow.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        let user = Helper.checkUserLoggedIn();
        if (user) {
            this.setState({
                isLoggedIn: true
            });
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.infoChannel.id !== prevState.idChannel) {
            return {
                isShowLogin: false,
                isShowSubcribe: false,

                idChannel: nextProps.infoChannel.id,
                isFollow: nextProps.infoChannel.is_follow,
                numberFollow: nextProps.infoChannel.numfollow
            };
        } else return null;
    }

    toggle() {
        this.setState({
            isShowLogin: false,
            isShowSubcribe: false
        });
    }

    handleFollow() {
        if (this.state.isLoggedIn) {
            this.setState({
                isShowLogin: false,
                isShowSubcribe: false,
                isFollow: 1,
                numberFollow: this.state.numberFollow + 1
            });
            ChannelApi.followChannel(this.state.idChannel).then(function ({data}) {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);
            }).catch(error => {
            });
        } else {
            this.setState({
                isShowLogin: true
            });
        }
    }

    handleUnfollow() {
        if (this.state.isLoggedIn) {
            this.setState({
                isShowLogin: false,
                isShowSubcribe: true
            });
        } else {
            this.setState({
                isShowLogin: true,
                isShowSubcribe: false
            });
        }
    }

    callbackUnfollow() {
        this.setState({
            isShowSubcribe: false,
            isFollow: 0,
            numberFollow: this.state.numberFollow > 0 ? this.state.numberFollow - 1 : 0
        });
        ChannelApi.unfollowChannel(this.state.idChannel).then(function ({data}) {
            Helper.checkTokenExpired(data);
            Helper.renewToken(data);
        }).catch(error => {
        });
    }

    render() {
        var infoChannel = this.props.infoChannel;
        const {isFollow, numberFollow} = this.state;
        if (infoChannel) {

            return (
                <div>
                    <img className="chanel-top-img" src={infoChannel.url_images_cover}
                         onError={(e) => {
                             e.target.onerror = null;
                             e.target.src = DEFAULT_IMG
                         }}/>
                    <div className="chanel-top-info">
                        <a className="cti-lnk-img img-catenew">
                            <img src={infoChannel.url_avatar} onError={(e) => {
                                e.target.onerror = null;
                                e.target.src = DEFAULT_IMG
                            }}/>
                        </a>
                        <div className="my-chanel-info">
                            <h3 className="my-chanel-h3">
                                {infoChannel.name}
                            </h3>
                            <p className="my-chanel-p">
                                {Helper.formatNumber(numberFollow)} người theo dõi
                            </p>
                            {infoChannel.isMyChannel ? '' :
                                !isFollow ?
                                    <a className="my-chanel-follow" onClick={this.handleFollow}>
                                        Theo dõi {Helper.formatNumber(numberFollow)}
                                    </a>
                                    :
                                    <a className="my-chanel-follow active" style={{color: '#5e48cd'}}
                                       onClick={this.handleUnfollow}>
                                        <span>Đã theo dõi {Helper.formatNumber(numberFollow)}</span>
                                    </a>
                            }

                            <ModalLogin isOpen={this.state.isShowLogin}
                                        body={'Quý khách chưa đăng nhập! Để hoàn thành thao tác, mời quý khách bấm OK để đăng nhập.'}
                                        toggle={this.toggle}/>

                            <ModalSubcribe isOpen={this.state.isShowSubcribe}
                                           body={'Xác nhận bỏ theo dõi kênh?'}
                                           callbackUnfollow={this.callbackUnfollow}
                                           toggle={this.toggle}/>

                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default ChannelInfo;
