import React from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
//import {Link} from '../../../routes';
import Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";
import Slider from "react-slick/lib";
import ModalLogin from "../../Modal/ModalLogin";
import ModalSubcribe from "../../Modal/ModalSubcribe";
import ChannelApi from "../../../services/api/Channel/ChannelApi";

var dragging = false;
 class BoxChannelFollow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            numberSlide: 3,
            idUnfollow: 0,

            isShowLogin: false,
            isShowSubcribe: false,
        };

        this.handleFollow = this.handleFollow.bind(this);
        this.handleUnfollow = this.handleUnfollow.bind(this);
        this.callbackUnfollow = this.callbackUnfollow.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    componentDidUpdate() {
        if (!this.state.isLoaded) {
            let channels = this.props.channels;
            let obj = {};
            if (channels.length > 0) {

                this.setState({
                    isLoaded: true
                }, () => {
                    channels.map((channel, index) => {
                        if (channel && index < channels.length && index < 5) {
                            let id = channel.id;
                            obj['isFollow_' + id] = channel.is_follow;
                            obj['numFollow_' + id] = channel.numfollow;
                        }
                    });

                    if (obj) {
                        this.setState(obj);
                    }
                });
            }
        }
    }

    handleFollow(id) {
        if (Helper.checkUserLoggedIn()) {
            this.setState({
                isShowLogin: false,
                isShowSubcribe: false,
                ['isFollow_' + id]: 1,
                ['numFollow_' + id]: this.state['numFollow_' + id] + 1
            });
            ChannelApi.followChannel(id).then(function ({data}) {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);
            }).catch(error => {
            });
        } else {
            this.setState({
                isShowLogin: true
            });
        }
    }

    handleUnfollow(id) {
        if (Helper.checkUserLoggedIn()) {
            this.setState({
                idUnfollow: id,
                isShowLogin: false,
                isShowSubcribe: true
            });
        } else {
            this.setState({
                isShowLogin: true,
                isShowSubcribe: false
            });
        }
    }

    toggle() {
        this.setState({
            isShowLogin: false,
            isShowSubcribe: false
        });
    }

    callbackUnfollow() {
        let id = this.state.idUnfollow;
        if (id) {
            this.setState({
                isShowSubcribe: false,
                ['isFollow_' + id]: 0,
                ['numFollow_' + id]: this.state['numFollow_' + id] > 0 ? this.state['numFollow_' + id] - 1 : 0
            });
            ChannelApi.unfollowChannel(id).then(function ({data}) {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);
            }).catch(error => {
            });
        }
    }

    render() {
        const {title, channels} = this.props;
        var {numberSlide} = this.state;

        if (channels && channels.length > 0) {
            let setLength = channels.length;
            if (channels.length >= 5) {
                setLength = 5;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                            right: -13,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="bpr-video tabphim box-shadow-cm">
                    <h3 className="bpr-video-h3">
                        {title}
                    </h3>
                    <div className="cate-hot-wrap" id="cate_hot_swiper">
                        <div className="owl-slider-av">
                            <div className="swiper-wrapper">
                                {/*<SlideResponsive video={channels} type="boxChannelFollow" numberItem={3}/>*/}

                                <div className="owl-slider-av">
                                    <Slider {...settings}>
                                        {this.showChannels(channels)}
                                    </Slider>
                                </div>

                            </div>
                        </div>
                    </div>

                    <ModalLogin isOpen={this.state.isShowLogin}
                                body={'Quý khách chưa đăng nhập! Để hoàn thành thao tác, mời quý khách bấm OK để đăng nhập.'}
                                toggle={this.toggle}/>

                    <ModalSubcribe isOpen={this.state.isShowSubcribe}
                                   body={'Xác nhận bỏ theo dõi kênh?'}
                                   callbackUnfollow={this.callbackUnfollow}
                                   toggle={this.toggle}/>
                </div>
            );
        } else {
            return null;
        }
    }

    showChannels(channels) {
        var result = null;
        var valState = this.state;
        return channels.map((channel, index) => {
            if (channel && index < channels.length && index < 5) {
                let id = channel.id;
                return <div className="cate-hot-item swiper-slide" key={index}>
                    <Link href="/channel"
                          as ={Helper.replaceUrl(channel.link)+'.html'}
                    >
                        <a  title={channel.name}
                            onClick={(e) => {
                                dragging && e.preventDefault();
                            }}
                            draggable={false}>
                            <img className="ats-img animated" alt={channel.name} src={channel.url_avatar}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                            />
                        </a>

                    </Link>

                    <Link href="/channel"
                          as={Helper.replaceUrl(channel.link)+'.html'}
                    >
                        <a title={channel.name}
                           onClick={(e) => {
                               dragging && e.preventDefault();
                           }}
                           draggable={false}>
                            <span className="sp-chanel-name">{channel.name}</span>
                            <span className="sp-chanel-follow">
                                    {valState['numFollow_' + id] ? Helper.formatNumber(valState['numFollow_' + id]) : 0} followers
                            </span>
                        </a>

                    </Link>

                    {!valState['isFollow_' + id] ?
                        <a className="chanel-btn-dk cn-catenew" onClick={() => this.handleFollow(id)}>Theo dõi</a>
                        :
                        <a className="chanel-btn-followed cn-catenew cn-catenew-followed"
                           onClick={() => this.handleUnfollow(id)}>Đã theo dõi</a>
                    }
                </div>
            }
        });
    }
}

export default BoxChannelFollow;
