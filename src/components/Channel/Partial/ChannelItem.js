import React  from 'react';
import Helper from "../../../utils/helpers/Helper";
// /import {Link} from "../../../routes";
import Link from 'next/link';
import {DEFAULT_IMG} from "../../../config/Config";
import ImageSSR from "../../Global/ImageSSR/ImageSSR"

class ChannelItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const video = this.props.video;
        if (video) {
            return (
                <div className="cate-hot-item swiper-slide swiper-slide-active">
                    <Link href={Helper.replaceUrl(video.link)}
                          as={Helper.replaceUrl(video.link)+'.html'}
                    >
                        <a title={video.name}>
                            <ImageSSR className="ats-img animated" alt={video.name} src={video.url_avatar}
                                 fallbackSrc={DEFAULT_IMG}
                            />
                        </a>

                    </Link>
                    <a title={video.name}>
                        <span className="sp-chanel-name">{video.name}</span>
                        <span className="sp-chanel-follow">
                            {Helper.formatNumber(video.numfollow)} followers
                        </span>
                    </a>
                </div>

            );
        } else {
            return <div>Loading...</div>;
        }

    }
}

export default ChannelItem;
