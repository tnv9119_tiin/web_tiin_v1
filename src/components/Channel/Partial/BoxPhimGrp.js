import React from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
//import {Link} from "../../../routes";
import Link from 'next/link';
import Slider from "react-slick/lib";
import Helper from "../../../utils/helpers/Helper";

var dragging = false;

class BoxPhimGrp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            numberSlide: 3
        };
    }

    render() {
        const {videoFilm} = this.props;
        const {numberSlide} = this.state;
        var videos = (Helper.checkArrNotEmpty(videoFilm, 'list_volume') && videoFilm.list_volume) ? videoFilm.list_volume : null;

        if (videoFilm && videos) {
            let setLength = videos.length;
            if (videos.length >= 5) {
                setLength = 5;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                            left: -23,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );


            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - numberSlide) ? 'none' : 'block',
                            right: -13,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="wrap-bpk wrap-bpk-chanel box-shadow-cm">
                    <div className="bpk-chanel-header">
                        <h3 className="chanel-header-h3" style={{fontWeight: '600'}}>
                            {Helper.checkObjExist(videoFilm, 'filmGroup.groupName') ? videoFilm.filmGroup.groupName.toUpperCase() : ''}
                            {Helper.checkObjExist(videoFilm, 'filmGroup.currentVideo') && videoFilm.filmGroup.currentVideo != "" ?
                                <span>{videoFilm.filmGroup.currentVideo}</span>
                                : ''
                            }
                        </h3>
                        <p className="chanel-header-p">
                            {Helper.formatNumber(videoFilm.isView)} lượt xem
                            • {Helper.getTimeAgo(videoFilm.DatePublish)}
                        </p>
                        <Link href="/video"
                              as={Helper.replaceUrl(videos[0].link ? videos[0].link : videoFilm.link) + '.html'}
                        >
                            <a className="channel-lnk-all"
                               onClick={(e) => {
                                   dragging && e.preventDefault();
                               }}
                               draggable={false}>
                                Phát tất cả
                            </a>

                        </Link>
                    </div>
                    <div className="owl_slider_av">
                        <Link href="/video"
                              as={Helper.replaceUrl(videos[0].link ? videos[0].link : videoFilm.link) + '.html'}
                        >
                            <a onClick={(e) => {
                                dragging && e.preventDefault();
                            }}
                               draggable={false}>
                                <span className="sp-slider-img cate-img-tab-phim">
                                <img src={Helper.checkObjExist(videoFilm, 'filmGroup.groupImage') ? videoFilm.filmGroup.groupImage : DEFAULT_IMG}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}
                                />
                                </span>
                            </a>

                        </Link>
                        <div className="row owl-slider-av owl-slider-av0 swiper-container-horizontal">
                            <div className="swiper-wrapper" style={{transform: 'translate3d(0px, 0px, 0px)'}}>
                                <Slider {...settings}>
                                    {this.showVideos(videos)}
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else if (videoFilm) {
            return (
                <div className="wrap-bpk wrap-bpk-chanel box-shadow-cm">
                    <div className="bpk-chanel-header">
                        <h3 className="chanel-header-h3" style={{fontWeight: '600'}}>
                            {Helper.checkObjExist(videoFilm, 'filmGroup.groupName') ? videoFilm.filmGroup.groupName.toUpperCase() : ''}
                            {Helper.checkObjExist(videoFilm, 'filmGroup.currentVideo') && videoFilm.filmGroup.currentVideo != "" ?
                                <span>{videoFilm.filmGroup.currentVideo}</span>
                                : ''
                            }
                        </h3>
                        <p className="chanel-header-p">
                            {Helper.formatNumber(videoFilm.isView)} lượt xem
                            • {Helper.getTimeAgo(videoFilm.DatePublish)}
                        </p>
                        <Link href="/video"
                              as={Helper.replaceUrl(videoFilm.link) + '.html'}
                        >
                            <a className="channel-lnk-all"
                                  onClick={(e) => {
                                      dragging && e.preventDefault();
                                  }}
                                  draggable={false}
                            >
                                Phát tất cả
                            </a>
                        </Link>
                    </div>
                    <div className="owl_slider_av">
                        <Link href="/video"
                              as={Helper.replaceUrl(videoFilm.link) + '.html'}
                        >
                            <a onClick={(e) => {
                                dragging && e.preventDefault();
                            }}
                               draggable={false}
                            >
                                <span className="sp-slider-img cate-img-tab-phim">
                                  <img
                                      src={Helper.checkObjExist(videoFilm, 'filmGroup.groupImage') ? videoFilm.filmGroup.groupImage : DEFAULT_IMG}
                                      onError={(e) => {
                                          e.target.onerror = null;
                                          e.target.src = DEFAULT_IMG
                                      }}
                                  />
                                </span>
                            </a>
                        </Link>
                    </div>
                </div>
            );
        } else {
            return '';
        }
    }

    showVideos(videos) {
        let result = null;

        if (videos) {
            result = videos.map((video, index) => {
                if (index < 5) {
                    let src = '';
                    if (Helper.checkArrNotEmpty(video, 'channels') && video.channels[0].id) {
                        src = "?src=cn" + video.channels[0].id;
                    }

                    if (video) {
                        return <div className="av-top-small swiper-slide" key={index}>
                            <div className="playnow-home-item-new">
                                <Link href="/video"
                                      as={Helper.replaceUrl(video.link) + '.html' + src}
                                >

                                    <a className="playnow-home-link-new"
                                       onClick={(e) => {
                                           dragging && e.preventDefault();
                                       }}
                                       draggable={false}>
                                        <img className="animated lazy"
                                             alt={video.name}
                                             title={video.name}
                                             data-original={video.image_path}
                                             src={video.image_path ? video.image_path : DEFAULT_IMG}
                                             onError={(e) => {
                                                 e.target.onerror = null;
                                                 e.target.src = DEFAULT_IMG
                                             }}
                                             style={{display: 'block'}}
                                             draggable={false}
                                        />
                                        <span className="sp-number-film sp-number-filmGrp">{video.epsNum}</span>

                                        <span className="icon-play-video"/>
                                    </a>

                                </Link>

                            </div>
                        </div>
                    } else {
                        return '';
                    }
                }
            });
        }
        return result;
    }
}

export default BoxPhimGrp;