import React, {Component} from 'react';
import Router from 'next/router';
//import {Router} from '../../../routes';
import * as Config from "../../../config/Config";
import qs from 'qs';

import axios from 'axios';
import Helper from "../../../utils/helpers/Helper";
import ModalPartial from "../../Modal/ModalPartial";
import {ImageSSR} from "../../Global/ImageSSR/ImageSSR";


import {DEFAULT_IMG} from "../../../config/Config";
import {DOMAIN_IMAGE_STATIC} from "../../../config/Config";
const UserImg = DOMAIN_IMAGE_STATIC + 'temp/UserImg.png';

export class EditChannel extends Component {
    constructor(props) {
        super(props);
        let isEdit = (this.props.type === 'create') ? 0 : 1;
        let info = this.props.infoChannel;
        this.state = {
            file: '',
            imagePreviewUrl: '',
            fileAvatar: info ? info.url_avatar: '',
            imagePreviewUrlAvatar: '',
            messError: '',
            enabledSave: false,
            nameChannel:  info ?  info.name : '',
            descChannel:  info ? info.description : '',
            bannerImage: info ? info.url_images_cover : '',
            imageWaitting: '',
            isShow: false,
            isEdit: isEdit,
            isShowPopup: false,
            submitted: false

        };

        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleBannerClick = this._handleBannerClick.bind(this);

        this._handleAvatarChange = this._handleAvatarChange.bind(this);
        this._handleAvatarClick = this._handleAvatarClick.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggle = this.toggle.bind(this);
        this.callOk = this.callOk.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        let submitted = this.state.submitted;
        if (!submitted) {
            this.setState({submitted: true});
            let self = this;
            const {nameChannel, descChannel, fileAvatar, bannerImage, enabledSave, isEdit} = this.state;

            if (enabledSave) {
                let dataPost = {
                    channelName: nameChannel,
                    channelDesc: descChannel,
                    channelAvatar: fileAvatar,
                    isEdit: String(isEdit),
                    bannerImage: bannerImage,
                    clientType: 'web',
                    token: Helper.getToken()
                };

                axios({
                    method: 'POST',
                    url: Config.MV_SERVICE + Config.API_CREATE_CHANNEL,
                    data: qs.stringify(dataPost),
                    headers: {'content-type': 'application/x-www-form-urlencoded'}
                }).then(({data}) => {
                    Helper.checkTokenExpired(data);
                    Helper.renewToken(data);
                    if (data.code === 200) {
                        self.setState({
                            isShow: true,
                            submitted: true
                        }, () => {
                            if (Helper.checkArrNotEmpty(data, "result")) {
                                let userInfo = Helper.getUserInfo();
                                userInfo.channelInfo = data.result[0];
                                Helper.setUserInfo(Helper.encryptData(userInfo));
                                // history.push("/");
                            }
                        });
                    } else {
                        self.setState({
                            isShowPopup: true,
                            submitted: false
                        });
                    }
                }).catch(function (result) {
                });

            }
        }
    }

    toggle() {
        this.setState({
            isShow: false,
            enabledSave: false,
            isShowPopup: false,
        });
    }
    callOk() {
        this.setState({
            isShow: false,
        });
        Router.push('/');
    }
    _handleBannerClick() {
        this.myinput.click();
    }

    _handleImageChange(e) {
        e.preventDefault();

        var reader = new FileReader();
        var file = e.target.files[0];

        if (file.type.includes('image/')) {
            reader.onloadend = () => {
                this.setState({
                    imagePreviewUrl: reader.result,
                    messError: ''
                });
            };
            reader.readAsDataURL(file);
            let formData = new FormData();
            formData.set('fileName', file.name);
            formData.append('uploadFile', file);
            axios({
                method: 'POST',
                url: Config.MV_SERVICE_UPLOAD + Config.API_UPLOAD_IMAGE,
                data: formData,
                headers: {'Content-type': 'multipart/form-data'}
            }).then(({data}) => {
                Helper.renewToken(data);

                if (data.code == 200) {
                    this.setState({
                        bannerImage: data.path,
                        enabledSave: true,
                        submitted: false
                    });
                } else {
                    this.setState({
                        file: '',
                        imagePreviewUrl: '',
                        messError: 'Ảnh cover không đúng định dạng.',
                        enabledSave: false
                    });
                }
            }).catch(function (result) {

            });
        } else {
            this.setState({
                file: '',
                imagePreviewUrl: '',
                messError: 'Ảnh cover không đúng định dạng.',
                enabledSave: false
            });
        }
    }

    _handleAvatarClick() {
        this.myInputAvatar.click();
    }

    _handleAvatarChange(e) {
        e.preventDefault();
        var reader = new FileReader();
        var file = e.target.files[0];
        if (file.type.includes('image/')) {
            reader.onloadend = () => {
                this.setState({
                    imagePreviewUrlAvatar: reader.result,
                    messError: ''
                });
            };
            reader.readAsDataURL(file);
            let formData = new FormData();
            formData.set('fileName', file.name);
            formData.append('uploadFile', file);
            axios({
                method: 'POST',
                url: Config.MV_SERVICE_UPLOAD + Config.API_UPLOAD_IMAGE,
                data: formData,
                headers: {'Content-type': 'multipart/form-data'}
            }).then(({data}) => {
                Helper.renewToken(data);

                if (data.code == 200) {
                    this.setState({
                        fileAvatar: data.path,
                        enabledSave: true,
                        submitted: false
                    });
                } else {
                    this.setState({
                        fileAvatar: '',
                        imagePreviewUrlAvatar: '',
                        messError: 'Ảnh đại diện không đúng định dạng.',
                        enabledSave: false
                    });
                }
            }).catch(function (result) {

            });
        } else {
            this.setState({
                fileAvatar: '',
                imagePreviewUrlAvatar: '',
                messError: 'Ảnh đại diện không đúng định dạng.',
                enabledSave: false
            });
        }
    }

    _handleInputName(event) {
        let name = event.target.value;

        const {isEdit, fileAvatar, bannerImage} = this.state;
        let isEnable = false;
        // if (isEdit && fileAvatar.length > 0 && bannerImage.length > 0) {
        //     isEnable = true;
        // } else if (!isEdit && fileAvatar.length > 0) {
        //     isEnable = true;
        // }

        if (name.length > 0) {
            this.setState({
                nameChannel: name,
                enabledSave: true,
                messError: '',
                submitted: false
            });
        } else {
            this.setState({
                enabledSave: false
                // messError: 'Tên kênh không được để trống'
            });
        }

    }

    _handleInputDesc(event) {
        this.setState({
            enabledSave: true,
            descChannel: event.target.value
        });
    }

    render() {
        const infoChannel = this.props.infoChannel;
        const type = this.props.type;
        let {imagePreviewUrl, imagePreviewUrlAvatar, enabledSave, isShowPopup} = this.state;
        let imagePre = null;
        if (imagePreviewUrl) {
            imagePre = (
                <ImageSSR src={imagePreviewUrl ? imagePreviewUrl : DEFAULT_IMG }
                     fallbackSrc={DEFAULT_IMG}
                />
            );
        } else {
            imagePre = (
                <ImageSSR
                    src={type === "create" ? 'http://cdn2.keeng.net/playnow/images/chanels/banner/20171211/9.jpg' : infoChannel.url_images_cover}
                    fallbackSrc={DEFAULT_IMG}
                />
            );
        }

        let imagePreview = null;
        if (imagePreviewUrlAvatar) {
            imagePreview = (
                <ImageSSR src={imagePreviewUrlAvatar} onClick={this._handleAvatarClick}
                     fallbackSrc={DEFAULT_IMG}
                />);
        } else {
            imagePreview = (
                <ImageSSR src={type === "create" ? UserImg : infoChannel.url_avatar} onClick={this._handleAvatarClick}
                     fallbackSrc={DEFAULT_IMG}
                />);
        }

        return (
            <form>
                <div className="blr-wrap box-shadow-cm">
                    <h2 className="blr-wrap-h2">
                        {type === "create" ? "TẠO KÊNH" : "CHỈNH SỬA KÊNH"}
                    </h2>
                    {type === "create" ? '' : <h3 className="blr-img-h3">Ảnh bìa</h3>}
                    {type === "create" ? '' :
                        <p title="" className="blr-timeline">
                            {imagePre}
                            <input type="file" name="imgBanner" onChange={(e) => {
                                this._handleImageChange(e)
                            }} ref={(input) => this.myinput = input} className="hidden" accept="image/*"/>
                            <a className="timeline-edit" onClick={this._handleBannerClick}/>
                        </p>
                    }

                    <div className="mc-form">
                        <div className="blr-form-left">
                            <h3 className="bfl-h3">Ảnh đại diện</h3>
                            <p title={type === "create" ? "Chọn ảnh đại diện" : "Thay đổi ảnh đại diện"}
                               className="bfl-p">
                                {imagePreview}
                                <input type="file" name="imgAvatar" onChange={(e) => {
                                    this._handleAvatarChange(e)
                                }} ref={(input) => this.myInputAvatar = input} className="hidden" accept="image/*"/>
                                <a className="avatar-edit" id="a_avatarEdit" onClick={this._handleAvatarClick}/>
                            </p>
                        </div>
                        <div className="blr-form-right">
                            <p className="bfr-p-text">Tên kênh</p>
                            <p className="bfr-p-input">
                                <input placeholder="Nhập tên kênh" maxLength={100}
                                       defaultValue={type === "create" ? '' : this.state.nameChannel}
                                       onChange={(event) => {
                                           this._handleInputName(event)
                                       }}
                                />
                                <br/>
                            </p>
                            <p className="bfr-p-text">Mô tả kênh</p>
                            <p className="bfr-p-input">
							<textarea placeholder="Nhập mô tả kênh" maxLength={4000}
                                      defaultValue={type === "create" ? '' : this.state.descChannel}
                                      onChange={event => {
                                          this._handleInputDesc(event)
                                      }}
                            />
                            </p>
                            <span style={{color: 'Red'}}>{this.state.messError}</span>
                            <p className="bfr-p-btn">
                                <a className={'chanel-form-btn' + (enabledSave ? ' active' : '')}
                                   title="Lưu lại thông tin kênh"
                                   onClick={this.handleSubmit.bind(this)}  >LƯU</a>
                            </p>
                        </div>
                    </div>
                </div>

                <ModalPartial isOpen={this.state.isShow}
                              body={type ==='create' ? 'Tạo kênh thành công' : 'Chỉnh sửa kênh thành công'}
                              toggle={this.toggle}
                              callOk={this.callOk}
                              type={'editChannel'}
                />

                <ModalPartial isOpen={this.state.isShowPopup}
                              body="Hệ thống thông báo không thành công. Vui lòng thao tác lại."
                              toggle={this.toggle}
                              callOk={this.toggle}
                              type={'editChannel'}
                />
            </form>
        );

    }
}

export default EditChannel;
