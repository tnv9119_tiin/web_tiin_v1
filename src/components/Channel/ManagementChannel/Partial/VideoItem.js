import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Helper from "../../../../utils/helpers/Helper";
import * as Config from "../../../../config/Config";
import axios from 'axios';
import qs from "qs";
import {DEFAULT_IMG} from "../../../../config/Config";
import {ImageSSR} from "../../../Global/ImageSSR/ImageSSR";
import ChannelApi from "../../../../services/api/Channel/ChannelApi";


class VideoItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            modal : false,
            isHidden: false
        };

        // this.modalHanldeOk = this.modalHanldeOk.bind(this);
    }

    handleDelVideo =()=>{
        this.setState({
            modal: !this.state.modal
        });
    }

    modalHanldeCancel=()=>{
        this.setState({
            modal: false
        });
    }
    // modalHanldeOk(idVideo){
    //     console.log('idvideo-----',idVideo);
    // }
    render() {
        var video = this.props.video;
        var idVideo = this.props.idVideo;
        var isHide = this.state.isHidden;
        let display;
        if(isHide
            // && Helper.getCookie('idVideoDeleted') == video.id
            ) {
                display = 'none';
            }else {
                display = 'block';
            }
            // if (Helper.getCookie('idVideoDeleted') == video.id){
            //     var isset = true;
            // }
            // if (isset){
            //     display = 'none';
            // }
        return (
            <div className="chanel-video-small" style={{display: display}}>
                { 0 <= video.active && video.active < 10 ?
                    <a className="cvs-lnk-img" title="Chỉnh sửa video" style={{cursor:'pointer'}}>
                        <ImageSSR
                            alt={video.name}
                            className="lazy img-responsive vi-img"
                            src={video.image_path ? video.image_path : DEFAULT_IMG}
                            fallbackSrc={DEFAULT_IMG}
                        />
                        <span className="icon-play-video"/></a>
                    :
                    <a className="cvs-lnk-img" title="Chỉnh sửa video" onClick={((e) => this.handleClick(video))} style={{cursor:'pointer'}}>
                        <ImageSSR
                            alt={video.name}
                            className="lazy img-responsive vi-img"
                            src={video.image_path ? video.image_path : DEFAULT_IMG}
                            fallbackSrc={DEFAULT_IMG}
                        />
                        <span className="icon-play-video"/></a>
                }

                <div className="cvs-info">
                    <h3 className="cvs-info-h2">
                        <a title="Chỉnh sửa video">
                            {video.name}</a>
                    </h3>
                    {/*<p className="cvs-info-date">11/10/2018</p>*/}
                    <p className="mocha-social">
                        <a title="Số lượt thích" className="ms-like">{video.total_like}</a>
                        <a title="Số lượt bình luận" className="ms-comment">{video.total_comment}</a>
                        {video.active >= 10 ? <a title="Số lượt chia sẻ" className="ms-share">{video.total_share}</a> : ''}
                        <a title="Lượt xem" className="ms-view">{Helper.formatNumber(video.isView)}</a>
                    </p>
                    <p className="chanel-edit">
                        { 0 <= video.active && video.active < 10 ?
                            <a className="waited" title="Đang chờ duyệt">Đang chờ duyệt</a>
                            :
                            <a title="Chỉnh sửa video" onClick={((e) => this.handleClick(video))}>Chỉnh sửa</a>
                        }
                        <a className="delete-video" title="Xóa video" onClick={this.handleDelVideo}>X</a>
                    </p>
                </div>
                <a className="chanel-dot-more"/>
                <Modal isOpen={this.state.modal} toggle={this.handleDelVideo} style={{width: '340px', top: '15%'}}>
                    <ModalBody className="popup-create-p">Bạn có chắc muốn xóa video này?</ModalBody>
                    <ModalFooter className="popup-create-btn">
                        <p className="pcb-continue" style={{cursor: 'pointer'}} color="primary" onClick={((e) => this.handleClickDelVideo(video.id))}>Xóa</p>
                        <p className="pcb-continue" style={{cursor: 'pointer'}} color="secondary" onClick={this.modalHanldeCancel}>Không</p>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

    handleClick = (item) => {
        this.props.callBack(item);
    }

    handleClickDelVideo = (id) => {
        this.setState({
            modal: false,
            isHidden: true
        });
        // Helper.setCookie('idVideoDeleted', id);
        let dataPost = {
            videoId: id,
            token: Helper.getToken()
        };
        return axios.post(Config.MV_SERVICE + Config.API_DEL_VIDEO_CHANNEL,
            qs.stringify(dataPost),
            {
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            }
        ).then(({data}) => {
            Helper.checkTokenExpired(data);
            Helper.renewToken(data);
            ChannelApi.deleteCache(this.props.video.channels[0].id).then(
                ({data}) => {
                }
            );

        });

    }
}

export default VideoItem;
