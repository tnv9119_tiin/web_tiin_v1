import React, {Component} from "react";
import Slider from "react-slick";
import Helper from "../../../utils/helpers/Helper";
//import {Link} from "../../../routes";
import  Link from 'next/link';
import {DEFAULT_IMG} from "../../../config/Config";


var dragging = false;

class FilmSlideForSearch extends React.Component {
    constructor(props) {
        super(props);

        var numberItem = this.props.numberItem;
        var numberSlide = numberItem ? numberItem : 4;

        this.state = {
            numberSlide: numberSlide
        };
    }


    render() {
        const {films, numberItem} = this.props;

        if (films && films.length > 0) {
            let setLength = films.length;
            if (films.length >= 10) {
                setLength = 10;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                        }}>
                </button>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                            right: 13,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberItem,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="owl-slider-av owl-slider-av1 swiper-container-horizontal">
                    <div className="swiper-wrapper"
                         style={{transform: 'translate3d(-27px, 0px, 0px)', transitionDuration: '0ms'}}>

                        <Slider {...settings}>
                            {this.showFilms(films)}
                        </Slider>

                    </div>
                </div>
            );
        } else {
            return null;
        }
    }

    showFilms(films) {
        return films.map((film, index) => {
            if (film && index < films.length && index < 10) {
                let link = Helper.replaceUrl(film.link);

                return <div className="av-top-small swiper-slide swiper-slide-active" key={index}>
                    <Link  href='/video' as ={link +'.html'} >
                        <a className="ats-lnk-img"  title={film.filmGroup.groupName}
                           onClick={(e) => {
                               dragging && e.preventDefault();
                           }}
                           draggable={false}>
                            <img alt={film.filmGroup.groupName} title={film.filmGroup.groupName} className="ats-img"
                                 src={film.filmGroup.groupImage}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                            />
                            {/*// onError="imageDefault(this)"/>*/}
                            {film.filmGroup.currentVideo ?
                                <span className="sp-number-film">{film.filmGroup.currentVideo}</span>
                                : ''
                            }
                        </a>

                    </Link>
                    <div className="ats-info">
                        <h3 className="ats-info-h3">
                            <Link href='/video'as={link+'.html'}
                            >
                                <a className="ats-info-lnk" title={film.filmGroup.groupName}
                                   onClick={(e) => {
                                       dragging && e.preventDefault();
                                   }}
                                   draggable={false}>
                                    {film.filmGroup.groupName}
                                    <span className="sp-chanel-follow">
                                            {Helper.formatNumber(film.filmGroup.isView)} lượt xem
                                    </span>
                                </a>

                            </Link>
                        </h3>
                    </div>
                </div>;
            }
        });
    }
}

export  default  FilmSlideForSearch;