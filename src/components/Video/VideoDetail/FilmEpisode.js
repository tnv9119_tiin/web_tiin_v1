import React from 'react';
import Helper from "../../../utils/helpers/Helper";
//import {Link} from "../../../routes";
import Link from 'next/link';

class FilmEpisode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMore: false
        };
    }

    showMore() {
        this.setState({
            showMore: true
        });
    }

    showEpisode(listFilm) {
        var result = null;
        if (listFilm.length > 0) {
            let chapter = this.props.chapter;
            result = listFilm.map((film, index) => {
                return (
                    <Link  href="/video" as={Helper.replaceUrl(film.link) + ".html?src=suggest"} >
                        <a  title={film.name} className={'episode-detal' + (chapter === film.chapter ? ' active' : '')} key={index} >
                            {film.chapter}
                        </a>

                    </Link>
                );
            });
        }
        return result;
    }

    render() {
        let showMore = this.state.showMore;
        let films = this.props.films;

        if (films.length > 0) {
            return (
                <div>
                    <div className="lst-episode" style={showMore ? {height: 'auto'} : {}} >
                        <ul className="ul-list-episode">
                            <li className="episode">
                                {this.showEpisode(films)}
                            </li>
                        </ul>
                    </div>
                    {films.length > 30 ?
                        <p className="p-more-episode" style={showMore ? {display: 'none'} : {display: 'block'}}
                           onClick={() => this.showMore()}>
                            Xem thêm tập khác
                        </p>
                        : ''
                    }
                </div>


            );
        } else {
            return null;
        }

    }

}

export default FilmEpisode;