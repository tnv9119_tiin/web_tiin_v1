import React, {Component} from 'react';
import VideoSlide from "../partials/VideoSlide";

 class VideoSameChannel extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let videoSameChannel = this.props.videos;

        if (videoSameChannel.length > 0) {
            return (
                <div>
                    <div className="playnow-auto line-videoofcn">
                        <b>VIDEO CÙNG KÊNH</b>
                    </div>

                    <VideoSlide videos={videoSameChannel} type="collapse" numberItem={4}/>
                </div>
            );
        } else {
            return null;
        }

    }

}
export  default  VideoSameChannel;
