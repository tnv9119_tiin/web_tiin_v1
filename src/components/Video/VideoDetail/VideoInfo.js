import React, {Component} from 'react';
import Helper from "../../../utils/helpers/Helper";
import ModalSubcribe from "../../Modal/ModalSubcribe";
import ModalLogin from "../../Modal/ModalLogin";
import VideoApi from "../../../services/api/Video/VideoApi";
//import {Link} from "../../../routes";
import Link from 'next/link';
import ChannelApi from "../../../services/api/Channel/ChannelApi";

import {DEFAULT_IMG, DOMAIN_IMAGE_STATIC} from "../../../config/Config";
const playnow_video_17 = DOMAIN_IMAGE_STATIC + "playnow_video_17.png";
const playnow_video_18 = DOMAIN_IMAGE_STATIC + "playnow_video_18.png";
const playnow_video_19 = DOMAIN_IMAGE_STATIC + "playnow_video_19.png";
const playnow_video_20 = DOMAIN_IMAGE_STATIC + "playnow_video_20.png";
const playnow_video_21 = DOMAIN_IMAGE_STATIC + "playnow_video_21.png";

export class VideoInfo extends Component {
    constructor(props) {
        super(props);
        let data = this.props.data;
        let channel = (Helper.checkArrNotEmpty(data, 'channels') && data.channels[0]) ? data.channels[0] : null;

        this.state = {
            isShowLogin: false,
            isShowSubcribe: false,
            isShowShare: false,
            isLoggedIn: false,

            id: data.id ? data.id : 0,
            idChannel: (channel && channel.id) ? channel.id : 0,

            isLike: data.is_like ? data.is_like : 0,
            isFollow: (channel && channel.is_follow) ? channel.is_follow : 0,

            numberLike: data.total_like ? data.total_like : 0,
            numberShare: data.total_share ? data.total_share : 0,
            numberComment: data.total_comment ? data.total_comment : 0,
            numberFollow: (channel && channel.numfollow) ? channel.numfollow : 0
        };
        this.handleFollow = this.handleFollow.bind(this);
        this.handleUnfollow = this.handleUnfollow.bind(this);
        this.callbackUnfollow = this.callbackUnfollow.bind(this);
        this.toggle = this.toggle.bind(this);

        this.handleLike = this.handleLike.bind(this);
        this.handleShare = this.handleShare.bind(this);
        this.closeShare = this.closeShare.bind(this);

        this.switchLight = this.switchLight.bind(this);
        this.handleFocusComment = this.handleFocusComment.bind(this);
    }

    componentDidMount() {
        let isLoggedIn = Helper.checkUserLoggedIn();
        if (isLoggedIn) {
            this.setState({
                isLoggedIn: true
            });
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.data.id !== prevState.id) {
            let isLoggedIn = Helper.checkUserLoggedIn();
            let channel = (Helper.checkArrNotEmpty(nextProps, 'data.channels') && nextProps.data.channels[0]) ? nextProps.data.channels[0] : null;

            return {
                isLoggedIn: isLoggedIn,
                isShowLogin: false,
                isShowSubcribe: false,

                id: nextProps.data.id,
                idChannel: (channel && channel.id) ? channel.id : 0,

                isLike: nextProps.data.is_like,
                isFollow: (channel && channel.is_follow) ?  channel.is_follow : 0,

                numberLike: nextProps.data.total_like,
                numberShare: nextProps.data.total_share,
                numberComment: nextProps.data.total_comment,
                numberFollow: (channel && channel.numfollow) ?  channel.numfollow : 0
            };
        } else return null;
    }

    handleLike() {
        if (this.state.isLoggedIn) {
            if (this.state.isLike) {
                VideoApi.unlikeVideo(this.props.data).then(
                    ({data}) => {
                        Helper.checkTokenExpired(data);
                        Helper.renewToken(data);

                        if (data.code === 200) {
                            this.setState({
                                isLike: false,
                                numberLike: this.state.numberLike > 0 ? this.state.numberLike - 1 : 0
                            });
                        }
                    },
                    (error) => {
                        this.setState({
                            error
                        });
                    });
            } else {
                VideoApi.likeVideo(this.props.data).then(
                    ({data}) => {
                        Helper.checkTokenExpired(data);
                        Helper.renewToken(data);

                        if (data.code === 200) {
                            this.setState({
                                isLike: true,
                                numberLike: this.state.numberLike + 1
                            });
                        }
                    },
                    (error) => {
                        this.setState({
                            error
                        });
                    });
            }
        } else {
            this.setState({
                isShowLogin: true
            });
        }

    }

    handleShare() {
        VideoApi.shareVideo(this.props.data).then(
            ({data}) => {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);

                this.setState({
                    isShowShare: true,
                    numberShare: this.state.numberShare + 1
                });
            },
            (error) => {
                this.setState({
                    error
                });
            });
    }

    handleFocusComment() {
        const txtComment = document.getElementById("txtComment");
        if (txtComment) {
            txtComment.focus();
        }
    }

    toggle() {
        this.setState({
            isShowLogin: false,
            isShowSubcribe: false
        });
    }

    handleFollow() {
        if (this.state.isLoggedIn) {
            ChannelApi.followChannel(this.state.idChannel).then(
                ({data}) => {
                    Helper.checkTokenExpired(data);
                    Helper.renewToken(data);

                    this.setState({
                        isShowLogin: false,
                        isShowSubcribe: false,
                        isFollow: 1,
                        numberFollow: this.state.numberFollow + 1
                    });
                }).catch(error => {
            });
        } else {
            this.setState({
                isShowLogin: true
            });
        }
    }

    handleUnfollow() {
        if (this.state.isLoggedIn) {
            this.setState({
                isShowLogin: false,
                isShowSubcribe: true
            });
        } else {
            this.setState({
                isShowLogin: true,
                isShowSubcribe: false
            });
        }
    }

    callbackUnfollow() {
        if (this.state.isLoggedIn) {

            ChannelApi.unfollowChannel(this.state.idChannel).then(
                ({data}) => {
                    Helper.checkTokenExpired(data);
                    Helper.renewToken(data);

                    this.setState({
                        isShowSubcribe: false,
                        isFollow: 0,
                        numberFollow: this.state.numberFollow > 0 ? this.state.numberFollow - 1 : 0
                    });
                }).catch(error => {
            });
        } else {
            this.setState({
                isShowLogin: true,
                isShowSubcribe: false
            });
        }

    }

    switchLight() {
        this.setState({
            turnOffLight: !this.state.turnOffLight
        });
    }

    closeShare() {
        this.setState({
            isShowShare: false
        });
    }

    share = (type) => {
        Helper.shareAll(type);
    }

    render() {

        const {turnOffLight, isLike, isFollow, numberLike, numberShare, numberComment, numberFollow, isShowShare} = this.state;
        let data = this.props.data;

        let styleLight = {};
        if (!turnOffLight) {
            styleLight = {
                height: '1727px',
                display: 'none'
            };
        } else {
            styleLight = {
                height: '1727px',
                display: 'block'
            };
        }

        if (data) {

            return (
                <div className="info-video">
                    <div className="info-video-right">
                        <h1 className="info-video-name">
                            {data.name}
                        </h1>
                        <p className="cvt-p" style={{fontSize: '16px'}}>{Helper.formatWithCommas(data.isView)} lượt
                            xem &nbsp;&nbsp;•&nbsp;&nbsp;
                            {Helper.getTimeAgo(data.DatePublish)}
                        </p>
                    </div>

                    {(Helper.checkArrNotEmpty(data, 'channels') && data.channels[0]) ?
                        <div className="playnow-home-chanel-new">
                            <Link href='/channel'
                                  as={Helper.replaceUrl(data.channels[0].link) + '.html'}>
                                <a className="playnow-chanel-link-new">
                                    <img title={data.channels[0].name}
                                         src={data.channels[0].url_avatar ? data.channels[0].url_avatar : DEFAULT_IMG}
                                         onError={(e) => {
                                             e.target.onerror = null;
                                             e.target.src = DEFAULT_IMG
                                         }}
                                    />
                                </a>

                            </Link>
                            <div className="play-chanel-info-new">
                                <h3 className="play-channel-h3-new h3-new-dv">
                                    <Link href="/channel"
                                          as={Helper.replaceUrl(data.channels[0].link) + '.html'}
                                    >
                                        <a id="linkChannel">
                                            {data.channels[0].name}
                                        </a>

                                    </Link>
                                </h3>
                            </div>
                            {!isFollow ?
                                <a className="chanel-btn-dk" onClick={this.handleFollow}>
                                    {"Theo dõi " + Helper.formatNumber(numberFollow)}</a>
                                :
                                <a className="chanel-btn-followed" style={{display: 'inline-block'}}
                                   onClick={this.handleUnfollow}>
                                    {"Đã theo dõi " + Helper.formatNumber(numberFollow)}</a>
                            }
                            <ModalLogin isOpen={this.state.isShowLogin}
                                        body={'Quý khách chưa đăng nhập! Để hoàn thành thao tác, mời quý khách bấm OK để đăng nhập.'}
                                        toggle={this.toggle}/>

                            <ModalSubcribe isOpen={this.state.isShowSubcribe}
                                           body={'Xác nhận bỏ theo dõi kênh?'}
                                           callbackUnfollow={this.callbackUnfollow}
                                           toggle={this.toggle}/>

                        </div>
                        :
                        ''
                    }


                    <div className="player-social line-more">
                        <p className="mocha-social">
                            <a className={"ms-like" + (isLike ? " active" : "")}
                               title={Helper.formatNumber(numberLike) + " Likes"} onClick={this.handleLike}>
                                {Helper.formatNumber(numberLike)}
                            </a>
                            <a onClick={this.handleFocusComment} id="ms-comment" className="ms-comment"
                               title={Helper.formatNumber(numberComment) + " Comments"}>
                                {Helper.formatNumber(numberComment)}
                            </a>
                            <a className="ms-share" title={Helper.formatNumber(numberShare) + " Shares"}
                               id="a_share_all" onClick={this.handleShare}>&nbsp;
                                {Helper.formatNumber(numberShare)}
                            </a>
                            <span className="mocha-social-save">
                                <a className="ms-save">Lưu video</a>
                                <a id="a_turnoff_light" className="ms-turnofflight"
                                   onClick={this.switchLight}>Tắt đèn</a>
                            </span>
                        </p>
                    </div>

                    <div className="social-fb">
                        <div id="fb-root"></div>
                        {/*{*/}
                        {/*    this.props.isServer? <>*/}
                        {/*        <script>*/}
                        {/*            {*/}
                        {/*                (function(d, s, id) {*/}
                        {/*                    var js, fjs = d.getElementsByTagName(s)[0];*/}
                        {/*                    if (d.getElementById(id)) return;*/}
                        {/*                    js = d.createElement(s); js.id = id;*/}
                        {/*                    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1';*/}
                        {/*                    fjs.parentNode.insertBefore(js, fjs);*/}
                        {/*                }(document, 'script', 'facebook-jssdk'))*/}
                        {/*            }*/}
                        {/*        </script>*/}
                        {/*    </>:''*/}
                        {/*}*/}


                        <div className="fb-like" data-href={data.link} data-layout="standard" data-action="like"
                             data-size="small" data-show-faces="true" data-share="true"></div>
                    </div>

                    <div id="persoff" style={styleLight} onClick={this.switchLight}/>
                    {data.description ?
                        <p className="ivc-wrap line-desc" dangerouslySetInnerHTML={{__html: data.description}}>
                        </p>
                        : ''
                    }

                    <div className="popup-notice" id="popup_turnon"
                         style={turnOffLight ? {display: 'block'} : {display: 'none'}}>
                        Click ra ngoài video để bật đèn
                    </div>

                    <div id="popshareAll" className="popup-share-fb"
                         style={isShowShare ? {display: 'block'} : {display: 'none'}}>
                        <div className="wrap-popup-share">
                            <a className="close-share" onClick={this.closeShare}>X</a>
                            <h3 className="popup-share-title">
                                CHIA SẺ
                            </h3>

                            <p className="popup-share-p">
                                <a onClick={() => this.share('facebook')} title="Facebook">
                                    <img src={playnow_video_17} alt="Facebook"/>
                                </a>
                                <a onClick={() => this.share('google')} title="Google+">
                                    <img src={playnow_video_18} alt="Google+"/>
                                </a>
                                <a onClick={() => this.share('twitter')} title="Twitter">
                                    <img src={playnow_video_19} alt="Twitter"/>
                                </a>
                                <a onClick={() => this.share('linkedin')} title="LinkedIn">
                                    <img src={playnow_video_20} alt="LinkedIn"/>
                                </a>
                                <a onClick={() => this.share('mail')} title="Email">
                                    <img src={playnow_video_21} alt="Email"/>
                                </a>
                            </p>
                            <p className="popup-share-input">
                                <input type="text" id="txtUrlShare" defaultValue={data.link}/>
                            </p>
                            <p className="popup-share-btn" onClick={(event) => {
                                Helper.copyUrlShare();
                                this.closeShare();
                            }}>
                                <a style={{color: 'white'}}>SAO CHÉP LINK</a>
                            </p>
                        </div>
                    </div>


                </div>

            );
        }

    }

}

export default VideoInfo;