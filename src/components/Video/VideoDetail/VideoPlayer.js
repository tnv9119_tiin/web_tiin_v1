import React from 'react';
import ReactPlayer from 'react-player';
import Helper from "../../../utils/helpers/Helper";
import LogApi from "../../../services/api/Log/LogApi";
import Plyr from 'plyr';
import {ARR_TIME_LOG, DEFAULT_IMG, DOMAIN_IMAGE_STATIC} from "../../../config/Config";
const iconMocha = DOMAIN_IMAGE_STATIC + "logomochavideo_1.png";

class VideoPlayer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isError: false,
            countRetry: 0,

            url: null,
            pip: false,
            playing: true,
            controls: true,
            light: false,
            volume: 0.8,
            muted: false,
            played: 0,
            loaded: 0,
            duration: 0,
            playbackRate: 1.0,
            loop: false,

            isLogStart: false,
            isLogEnd: false,
            timesStart: 0,
            timePlayed: 0,
            isSeek: "0"
        };
    }


    load = url => {
        this.setState({
            url,
            played: 0,
            loaded: 0,
            pip: false
        })
    }
    playPause = () => {
        this.setState({playing: !this.state.playing})
    }
    stop = () => {
        this.setState({url: null, playing: false})
    }

    toggleLoop = () => {
        this.setState({loop: !this.state.loop})
    }
    setVolume = e => {
        this.setState({volume: parseFloat(e.target.value)})
    }
    toggleMuted = () => {
        this.setState({muted: !this.state.muted})
    }
    setPlaybackRate = e => {
        this.setState({playbackRate: parseFloat(e.target.value)})
    }
    togglePIP = () => {
        this.setState({pip: !this.state.pip})
    }
    onPlay = () => {
        console.log('onPlay')
        if (this.state.isLogEnd) {
            let timesStart = new Date().getTime();
            this.setState({
                playing: true,

                timesStart: timesStart,
                timePlayed: 0,
                isSeek: "0",
                isLogStart: true,
                isLogEnd: false
            }, () => {
                this.logStart();
            });
        } else {
            this.setState({playing: true})
        }
    }
    onEnablePIP = () => {
        console.log('onEnablePIP')
        this.setState({pip: true})
    }
    onDisablePIP = () => {
        console.log('onDisablePIP')
        this.setState({pip: false})
    }
    onPause = () => {
        console.log('onPause')
        this.setState({playing: false})
    }
    onSeekMouseDown = e => {
        console.log("onSeekMouseDown");
        this.setState({seeking: true})
    }
    onSeekChange = e => {
        console.log("onSeekChange");
        this.setState({played: parseFloat(e.target.value)})
    }
    onSeekMouseUp = e => {
        console.log("onSeekMouseUp");

        this.setState({seeking: false})
        this.player.seekTo(parseFloat(e.target.value))
    }
    onProgress = state => {
        //console.log('onProgress', state)
        // We only want to update time slider if we are not currently seeking
        if (!this.state.seeking) {
            //let playedSeconds = parseInt(state.playedSeconds);
            if (parseInt(this.state.playedSeconds) != parseInt(state.playedSeconds)) {
                state.timePlayed = this.state.timePlayed + 1;
            }

            this.setState(state, () => {
                if (Helper.inArr(ARR_TIME_LOG, this.state.timePlayed)) {
                    let propsData = this.props.data;
                    let data = {
                        "timesstart": String(this.state.timesStart),
                        "videoId": String(propsData.id),
                        "mediaUrl": propsData.original_path,
                        "url": propsData.link,
                        "trackTime": String(parseInt(state.playedSeconds * 1000)),
                        "timePlay": String(this.state.timePlayed * 1000),
                        "duration": String(parseInt(this.state.duration * 1000)),
                        "isSeek": this.state.isSeek,
                        "flagLog": String(this.state.timePlayed + 's')
                    };
                    LogApi.pushLog(data);
                }
            });
        }
    }
    onEnded = () => {
        console.log("onEnded");
        if (!this.state.isLogEnd) {
            this.logEnded();
        }

        let isLive = this.props.data.isLive;
        if (isLive) {
            this.setState({
                isLogEnd: true,
                url: null,
                playing: false,
                loop: false
            });

        } else {
            this.setState({
                isLogEnd: true,
                playing: this.state.loop
            });

            if (this.props.autoPlay) {
                this.props.callbackNext()
            }
        }

    }
    onDuration = (duration) => {
        console.log('onDuration', duration)
        this.setState({duration})
    }
    onClickFullscreen = () => {
        screenfull.request(findDOMNode(this.player))
    }
    renderLoadButton = (url, label) => {
        return (
            <button onClick={() => this.load(url)}>
                {label}
            </button>
        )
    }
    ref = player => {
        this.player = player
    }

    onReady = () => {
        console.log("onReady");
    }

    onStart = () => {
        console.log('onStart');

        let timesStart = new Date().getTime();
        let isLogStart = this.state.isLogStart;
        this.setState({
            timesStart: timesStart,
            timePlayed: 0,
            isLogStart: true
        }, () => {
            if (!isLogStart) {
                this.logStart();
            }
        });
    }

    onSeek = () => {
        console.log('onSeek');
        if (this.state.isSeek === "0") {
            this.setState({
                isSeek: "1"
            });
        }
    }

    onError = () => {
        let {url, countRetry} = this.state;
        if (countRetry === 0 && url.includes('http://mcvideo.media1vip.keeng.net/playnow/')) {
            url = url.replace("http://mcvideo.media1vip.keeng.net/playnow/", "http://mcvideo.media1.keeng.net/uservideo/playnow/");
            this.setState({
                url: url,
                countRetry: 1
            });
        }
    }

    componentDidMount() {
        if (this.props.data) {
            let data = this.props.data;

            this.setState({
                id: data.id,
                url: data.original_path ? data.original_path : null,
                link: data.link ? data.link : ''
            }, () => {
                this.initPlayer();
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.data && Helper.checkObjExist(this.props.data, 'id')) {
            if (this.state.id && this.props.data.id !== this.state.id) {
                console.log("componentDidUpdate...", this.state);
                if (!this.state.isLogEnd) {
                    this.logEnded();
                }

                let data = this.props.data;
                let timesStart = new Date().getTime();
                this.setState({
                    id: data.id,
                    url: data.original_path ? data.original_path : null,
                    link: data.link ? data.link : '',
                    countRetry: 0,

                    isSeek: "0",
                    isLogStart: false,
                    isLogEnd: false,
                    timesStart: timesStart,
                    timePlayed: 0
                }, () => {
                    this.initPlayer();
                });
            }
        } else if (!this.state.isError) {
            this.setState({
                isError: true
            }, () => {
                this.props.callbackError();
            });
        }
    }

    componentWillUnmount() {
        if (!this.state.isLogEnd && !this.state.isError) {
            this.logEnded();
        }

        if (this.playerPlyr) {
            this.playerPlyr.destroy();
        }
    }

    logStart() {
        let data = {
            "timesstart": String(this.state.timesStart),
            "videoId": String(this.state.id),
            "mediaUrl": this.state.url,
            "url": this.state.link,
            "trackTime": "0",
            "timePlay": "0",
            "duration": String(parseInt(this.state.duration * 1000)),
            "isSeek": this.state.isSeek,
            "flagLog": "start"
        };
        LogApi.pushLog(data);
    }

    logEnded() {
        let data = {
            "timesstart": String(this.state.timesStart),
            "videoId": String(this.state.id),
            "mediaUrl": this.state.url,
            "url": this.state.link,
            "trackTime": String(parseInt(this.state.playedSeconds * 1000)),
            "timePlay": String(this.state.timePlayed * 1000),
            "duration": String(parseInt(this.state.duration * 1000)),
            "isSeek": this.state.isSeek,
            "flagLog": "end"
        };
        LogApi.pushLog(data);
    }

    initPlayer() {
        if (!this.playerPlyr) {
            let content = document.getElementById('content_video');
            if (content) {
                let videoContent = content.firstChild;
                if (videoContent) {
                    let options = {
                        settings: [],
                        tooltips: {controls: true, seek: true},
                        i18n: {
                            "play": "Phát",
                            "pause": "Tạm dừng",
                            "seek": "Seek",
                            "played": "Played",
                            "buffered": "Buffered",
                            "currentTime": "Current time",
                            "duration": "Duration",
                            "volume": "Âm lượng",
                            "mute": "Tắt tiếng",
                            "unmute": "Bật âm thanh",
                            "enterFullscreen": "Toàn màn hình",
                            "exitFullscreen": "Thoát khỏi chế độ toàn màn hình",
                            "start": "Start",
                            "end": "End",
                            "pip": "Trình phát mini"
                        },
                        volume: 0.8
                    };

                    this.playerPlyr = new Plyr(videoContent, options);
                }
            }
        }
        this.checkLive();
    }

    checkLive() {
        if (this.playerPlyr) {
            if (this.props.data) {
                if (this.props.data.isLive) {
                    let plyrProgress = document.getElementsByClassName("plyr__progress__container");
                    if (plyrProgress.length > 0) {
                        plyrProgress[0].style.visibility = 'hidden';
                    }
                    let plyrTime = document.getElementsByClassName("plyr__time");
                    if (plyrTime.length > 0) {
                        plyrTime[0].style.visibility = 'hidden';
                    }

                    var list = document.getElementsByClassName("plyr__controls");
                    if (list.length > 0) {
                        var newItem = document.createElement("div");
                        newItem.id = "div_live_id";
                        //newItem.style.marginLeft = "5px";
                        let spanElement = document.createElement("span");
                        spanElement.style.display = "inline-flex";
                        spanElement.style.marginTop = "10px";

                        let pElement = document.createElement("p");
                        pElement.className = "roundLive";
                        let textnode = document.createTextNode("LIVE");
                        spanElement.appendChild(pElement);
                        spanElement.appendChild(textnode);
                        newItem.appendChild(spanElement);

                        list[0].insertBefore(newItem, list[0].childNodes[1]);
                    }
                } else {
                    let plyrProgress = document.getElementsByClassName("plyr__progress__container");
                    if (plyrProgress.length > 0) {
                        plyrProgress[0].style.visibility = 'visible';
                    }
                    let plyrTime = document.getElementsByClassName("plyr__time");
                    if (plyrTime.length > 0) {
                        plyrTime[0].style.visibility = 'visible';
                    }
                    let divLive = document.getElementById("div_live_id");
                    if (divLive) {
                        divLive.remove();
                    }
                }

            }
        }
    }

    render() {
        const data = this.props.data;
        const url = this.state.url;
        var userLogo = Helper.checkObjExist(data, 'useLogo') ? data.useLogo : 0;

        if (data) {
            let showImage = false;
            if (data.hasLive && ((!data.isLive && !data.original_path.includes('.mp4')) || (data.isLive && data.original_path && !url) )) {
                showImage = true;
            }

            return (
                <div className="player-detail">
                    <div className="wrap-bpk-dv">
                        <div className="player-video">
                            {showImage ?
                                <img src={data.image_path ? data.image_path : data.image_path_thumb}
                                     style={{width: '100%', maxHeight: '450px'}}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}/>
                                :
                                <ReactPlayer
                                    ref={this.ref}
                                    id='content_video'
                                    width='100%'
                                    height='450px'
                                    style={{position: 'relative', zIndex: 8}}
                                    url={url}
                                    //pip={pip}
                                    // playing={playing}
                                    playing={true}
                                    controls={false}
                                    config={{
                                        file: {
                                            attributes: {
                                                controlsList: 'nodownload',
                                                poster: data.image_path ? data.image_path : null
                                            }
                                        }
                                    }}
                                    // light={light}
                                    // loop={loop}
                                    // playbackRate={playbackRate}
                                    // volume={volume}
                                    // muted={muted}
                                    onReady={this.onReady}
                                    onStart={this.onStart}
                                    onPlay={this.onPlay}
                                    onEnablePIP={this.onEnablePIP}
                                    onDisablePIP={this.onDisablePIP}
                                    onPause={this.onPause}
                                    onBuffer={() => console.log('onBuffer')}
                                    // onSeek={e => console.log('onSeek', e)}
                                    onSeek={this.onSeek}
                                    onEnded={this.onEnded}
                                    //onError={e => console.log('onError', e)}
                                    onError={this.onError}
                                    onProgress={this.onProgress}
                                    onDuration={this.onDuration}
                                />
                            }
                            {userLogo ?
                                <div className="vjs-watermark"
                                     style={{zIndex: '8', top: '15px', right: '0px', opacity: 1}}>
                                    <img src={iconMocha}/></div>
                                : ''
                            }

                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }

    }

}

export  default  VideoPlayer;