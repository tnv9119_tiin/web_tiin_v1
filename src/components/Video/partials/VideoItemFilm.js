import React from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
//import {Link} from '../../../routes';
import Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";

class VideoItemFilm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var video = this.props.video;
        if (video) {
            return (
                <div className="av-top-small swiper-slide">
                    <Link href='/video'
                          as = {Helper.replaceUrl(video.link) + ".html?src=phim"}
                    >
                        <a className="ats-lnk-img" title={video.filmGroup.groupName}>
                            <img className="ats-img" alt={video.filmGroup.groupName} title={video.filmGroup.groupName}
                                 src={video.filmGroup.groupImage ? video.filmGroup.groupImage : DEFAULT_IMG}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                            />
                            {video.filmGroup.currentVideo ?
                                <span className="sp-number-film subSp">{video.filmGroup.currentVideo}</span>
                                : ''
                            }
                        </a>

                    </Link>
                    <div className="ats-info">
                        <h3 className="ats-info-h3">
                            <Link href="/video"
                                  as = {Helper.replaceUrl(video.link) + ".html?src=phim"}
                            >
                                <a title={video.filmGroup.groupName} className="ats-info-lnk">
                                    {video.filmGroup.groupName}
                                    <span className="sp-chanel-follow">{Helper.formatNumber(video.isView)} lượt xem</span>
                                </a>

                            </Link>
                        </h3>
                    </div>
                </div>
            );
        } else {
            return <div>Loading...</div>;
        }

    }
}


export  default  VideoItemFilm;