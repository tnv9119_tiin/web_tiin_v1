import React from "react";
import Slider from "react-slick";

import Helper from "../../../utils/helpers/Helper";
//import {Link} from "../../../routes";
import Link from 'next/link';
import {DEFAULT_IMG} from "../../../config/Config";

var dragging = false;

class FilmSlide extends React.Component {
    constructor(props) {
        super(props);

        var numberItem = this.props.numberItem;
        var numberSlide = numberItem ? numberItem : 4;
        this.state = {
            numberSlide: numberSlide
        };
    }


    render() {
        var {film} = this.props;
        if (film && film.length > 0) {
            let setLength = film.length;
            if (film.length >= 10) {
                setLength = 10;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <div>
                    <button onClick={onClick}
                            {...props}
                            className="owl-slider-back"
                            aria-hidden="true"
                            aria-disabled={currentSlide === 0}
                            type="button"
                            style={{
                                display: currentSlide === 0 ? 'none' : 'block',
                                border: 'none',
                                outline: 'none'
                            }}>
                    </button>
                </div>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <div>
                    <button onClick={onClick}
                            {...props}
                            className="owl-slider-next"
                            aria-hidden="true"
                            aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                            type="button"
                            style={{
                                display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                                right: 0,
                                border: 'none',
                                outline: 'none'
                            }}>
                    </button>
                </div>

            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: this.state.numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (

                <div className="owl-slider-av slide_phim">
                    <Slider {...settings}>
                        {this.showFilms(film)}
                    </Slider>
                </div>

            );
        } else {
            return null;
        }

    }

    showFilms(films) {
        var result = null;

        if (films && films.length > 0) {
            result = films.map((film, index) => {
                if (index < 10) {
                    if (index < films.length) {
                        if (Helper.checkObjExist(film, 'filmGroup')) {
                            return <div className="av-top-small-dV swiper-slide swiper-slide" key={index}>
                                <Link href="/video"
                                      as={Helper.replaceUrl(film.link) + '.html'}>
                                    <a className="ats-lnk-img"
                                       title={film.filmGroup.groupName}
                                       onClick={(e) => {
                                           dragging && e.preventDefault();
                                       }}
                                       draggable={false}
                                    >
                                        <img alt={film.filmGroup.groupName}
                                             title={film.filmGroup.groupName} className="ats-img"
                                             src={film.filmGroup.groupImage ? film.filmGroup.groupImage : DEFAULT_IMG}
                                             onError={(e) => {
                                                 e.target.onerror = null;
                                                 e.target.src = DEFAULT_IMG
                                             }}/>
                                        {film.filmGroup.currentVideo ?
                                            <span className="sp-number-film">
                                        {film.filmGroup.currentVideo}
                                    </span>
                                            : ''
                                        }
                                    </a>

                                </Link>
                                <div className="ats-info">
                                    <h3 className="ats-info-h3-dV">
                                        <Link href="/video"
                                              as={Helper.replaceUrl(film.link) + '.html'}
                                        >
                                            <a className="ats-info-lnk"
                                               onClick={(e) => {
                                                   dragging && e.preventDefault();
                                               }}
                                               draggable={false}
                                            >
                                                {film.filmGroup.groupName}
                                                <span className="sp-chanel-follow">
                                                    {Helper.formatNumber(film.isView)} lượt xem
                                                </span>
                                            </a>

                                        </Link>
                                    </h3>
                                </div>
                            </div>;
                        }
                    }
                }
            });
        }
        return result;
    }
}

export default FilmSlide;