import React from 'react';
//import {Link} from '../../../routes';
import Link from 'next/link';
import Helper from "../../../utils/helpers/Helper";
import {DEFAULT_IMG} from "../../../config/Config";

class VideoFeatured extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        var videoHot = this.props.videoHot.topFiveAll;
        return (
            <div className="bpr-video bpr-video-tabVideoCn box-shadow-cm">
                <h3 className="bpr-video-h3">
                    VIDEO NỔI BẬT
                </h3>
                {this.showVideo(videoHot)}
            </div>
        );
    }
    showVideo(videoHot) {
        if (videoHot && videoHot.length > 0) {
            return videoHot.map((video, index) => {

                if (video && index < 4 ) {
                    return <div className="av-top-small swiper-slide" key={index}>
                        <div className="playnow-home-item-new playnow-home-item-new-tabVideoCn">
                            <Link href="/video"
                                  as={Helper.replaceUrl(video.link) + ((Helper.checkArrNotEmpty(video, 'channels') && video.channels[0]) ? ".html?src=cn" + video.channels[0].id : ".html")}
                            >
                                <a className="playnow-home-link-new" >
                                    <img className="animated"
                                         src={video.image_path ? video.image_path : DEFAULT_IMG}
                                         onError={(e) => {
                                             e.target.onerror = null;
                                             e.target.src = DEFAULT_IMG
                                         }}
                                    />
                                    <span className="icon-play-video"></span>
                                    <h3 className="playnow-home-h3-new">
                                        {video.name}
                                    </h3>
                                </a>

                            </Link>
                            <div className="playnow-home-chanel-new">
                                <div className="play-chanel-info-new">
                                    <p className="play-chanel-view-new">
                                        {Helper.formatNumber(video.isView)} lượt xem • {Helper.getTimeAgo(video.DatePublish)}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            });
        }
        return null;
    }
}
export default VideoFeatured;
