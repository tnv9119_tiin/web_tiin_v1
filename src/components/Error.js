import React from 'react';
import {Router} from '../routes';
import Footer from "./Global/Footer";


class Error extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.redirect = setInterval(function () {
            Router.pushRoute('/', {shallow: true});
        }.bind(this), 5000);
    }


    componentWillUnmount() {
        if (this.redirect) {
            clearInterval(this.redirect);
            this.redirect = null;
        }
    }

    render() {
        return (
            <>
                <div id="body">
                    <style dangerouslySetInnerHTML={{__html: "\n    #body{background-color: #f5f5f5!important;}\n"}}/>
                    <div style={{borderBottom: '1px solid #dcdcdc', paddingBottom: '35px'}}>
                        <div style={{padding: '100px 0 15px 10px'}}>
                            <span><b>Xin lỗi quý khách, không tìm được nội dung yêu cầu</b></span>
                        </div>
                    </div>
                </div>
                <Footer/>
            </>


        );
    }
}

export default Error;
