import React, {Component} from 'react';
import VideoItemFilm from "../../Video/partials/VideoItemFilm";

class ListPhim extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var videoFilm = this.props.videoFilm;
        return(
            <div className="bpr-new box-shadow-cm">
                <h3 className="bpr-new-h3">
                    PHIM MỚI CẬP NHẬT
                </h3>
                <div className="bpr-new-wrap">
                    {this.showVideos(videoFilm)}
                </div>
                <a onClick={this.props.showAllOnclick} className="p-more-video morevideophim" style={{color:'white'}}>Xem tất cả </a>
            </div>
        );
    }

    showVideos(videos){
        var result = null;
        if(videos) {
            if (videos.length > 0) {
                result = videos.map((video, index) => {
                    if (index < 18) {
                        return <VideoItemFilm key={index} video={video}/>
                    }
                });
            }
        }
        return result;
    }
}

export default ListPhim;
