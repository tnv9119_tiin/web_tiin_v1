import React, {Component} from 'react';
import FilmHot from '../Partial/FilmHot';

class CatePhimHot extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var lstFilmHot = this.props.filmHot;
        return(
            <div className="bpl-noibat box-shadow-cm">
                <h3 className="bp-noibat-h3">
                    PHIM NỔI BẬT
                </h3>
                {this.showVideos(lstFilmHot)}
            </div>
        );
    }

    showVideos(lstFilmHot){
        var result = null;
        if(lstFilmHot) {
            if (lstFilmHot.length > 0) {
                result = lstFilmHot.map((filmHot, index) => {
                    return <FilmHot key={index} filmHot={filmHot}/>
                });
            }
        }
        return result;
    }
}

export default CatePhimHot;
