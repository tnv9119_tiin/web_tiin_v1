import React, {Component} from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
import {ImageSSR} from "../../Global/ImageSSR/ImageSSR";

export class CateInfo extends Component {


    render() {
        var cateInfo = this.props.cateInfo;
        if (cateInfo) {
            return (
                <div className="chanel-top chanel-top2 box-shadow-cm">
                    <img className="chanel-top-img"
                         src={cateInfo.url_images}
                         onError={(e) => {
                             e.target.onerror = null;
                             e.target.src = "http://live84.keeng.net/playnow/images/static/web/AvatarError.jpg"
                         }}
                    />
                    <div className="chanel-top-info">
                        <a className="cti-lnk-img cate-img img-catenew">
                            <ImageSSR src={cateInfo.url_avatar}
                                      fallbackSrc={"http://live84.keeng.net/playnow/images/static/web/MochalogoError.png"}
                            />

                            {cateInfo.categoryname}
                        </a>
                    </div>
                </div>
            );
        } else {
            return null;
        }

    }
}

export default CateInfo;
