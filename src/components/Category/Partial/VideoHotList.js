import React, {Component} from 'react';
//import {Tabs, Tab, TabPanel, TabList} from "react-web-tabs";
import VideoItem from '../../Video/partials/VideoItem';

import {Tabs, Tab, TabPanel, TabList} from 'react-tabs';
import Helper from "../../../utils/helpers/Helper";
//import "react-tabs/style/react-tabs.css";

 class VideoHotList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0
        };
    }

    handleSelect = (index) => {
        this.setState({
            selectedIndex : index
        });
    }

    render() {
        const videoHot = this.props.videoHot;
        const isChannel = this.props.isChannel;
        if(videoHot){

            return (
                <div className={isChannel == 1 ? "bpl-trailer bpl-video box-shadow-cm" : "bpl-trailer box-shadow-cm"} >
                    <h3 className="bpl-trailer-h3">
                        VIDEO NỔI BẬT
                    </h3>
                    <Tabs
                        selectedIndex={this.state.selectedIndex}
                        onSelect={this.handleSelect}
                    >
                        <div className="mocha-video-tab tab-cn-videohot">
                            <TabList
                            >
                                <Tab>
                                    <a>Tuần</a>
                                </Tab>
                                <Tab>
                                    <a>Tháng</a>
                                </Tab>
                                <Tab>
                                    <a>Tất cả</a>
                                </Tab>
                            </TabList>
                        </div>
                        <TabPanel>
                            <div className="av-top-small swiper-slide">
                                {this.showVideos(videoHot.topFiveWeek, isChannel)}
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="av-top-small swiper-slide">
                                {this.showVideos(videoHot.topFiveMonth, isChannel)}
                            </div>
                        </TabPanel>
                        <TabPanel >
                            <div className="av-top-small swiper-slide">
                                {this.showVideos(videoHot.topFiveAll, isChannel)}
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            );
        }else{
            return (
                <div></div>
            );
        }

    }

    showVideos(videos, isChannel){
        videos = Helper.checkIsChannelNotShowVideo(videos);
        var result = null;
        if(videos) {
            if (videos.length > 0) {
                result = videos.map((video, index) => {
                    return <VideoItem key={index} video={video} isVideoHotOfCate={true} isVideoOfChannel={isChannel}/>
                });
            }
        }
        return result;
    }
}

export default VideoHotList;
