import React, {Component} from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
//import {Link} from '../../../routes';
import Link from "next/link";
import Helper from "../../../utils/helpers/Helper";
import ModalSubcribe from "../../Modal/ModalSubcribe";
import ChannelApi from "../../../services/api/Channel/ChannelApi";
import ModalLogin from "../../Modal/ModalLogin";
import  {ImageSSR } from '../../Global/ImageSSR/ImageSSR';

export class ChannelItem extends Component {
    constructor(props) {
        super(props);
        var channelHot = this.props.channelHot;

        this.state = {
            isShowLogin: false,
            isShowSubcribe: false,

            isLoggedIn: false,
            idChannel: channelHot.id,
            isFollow: channelHot.isFollow,
            numberFollow: channelHot.numfollow
        };

        this.handleFollow = this.handleFollow.bind(this);
        this.handleUnfollow = this.handleUnfollow.bind(this);
        this.callbackUnfollow = this.callbackUnfollow.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        let user = Helper.checkUserLoggedIn();
        if (user) {
            this.setState({
                isLoggedIn: true
            });
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.channelHot.id !== prevState.idChannel) {
            return {
                isShowLogin: false,
                isShowSubcribe: false,

                isLoggedIn: false,
                idChannel: nextProps.channelHot.id,
                isFollow: nextProps.channelHot.isFollow,
                numberFollow: nextProps.channelHot.numfollow
            };
        } else return null;
    }

    toggle() {
        this.setState({
            isShowLogin: false,
            isShowSubcribe: false
        });
    }

    handleFollow() {
        if (this.state.isLoggedIn) {
            this.setState({
                isShowLogin: false,
                isShowSubcribe: false,
                isFollow: 1,
                numberFollow: this.state.numberFollow + 1
            });
            ChannelApi.followChannel(this.state.idChannel).then(function ({data}) {
                Helper.checkTokenExpired(data);
                Helper.renewToken(data);
            }).catch(error => {});
        } else {
            this.setState({
                isShowLogin: true
            });
        }
    }

    handleUnfollow() {
        if (this.state.isLoggedIn) {
            this.setState({
                isShowLogin: false,
                isShowSubcribe: true
            });
        } else {
            this.setState({
                isShowLogin: true,
                isShowSubcribe: false
            });
        }
    }

    callbackUnfollow() {
        this.setState({
            isShowSubcribe: false,
            isFollow: 0,
            numberFollow: this.state.numberFollow > 0 ? this.state.numberFollow - 1 : 0
        });
        ChannelApi.unfollowChannel(this.state.idChannel).then(function ({data}) {
            Helper.checkTokenExpired(data);
            Helper.renewToken(data);
        }).catch(error => {});
    }


    render() {
        var channelHot = this.props.channelHot;
        const {isFollow, numberFollow} = this.state;

        if (channelHot) {
            return (
                <div className="cate-hot-item swiper-slide">
                    <Link href="/channel"as={Helper.replaceUrl(channelHot.link)+'.html'} >
                        <a title={channelHot.name}>

                            <ImageSSR className="ats-img animated" alt={channelHot.name} src={channelHot.url_avatar}
                                      fallbackSrc={DEFAULT_IMG}
                            />

                        </a>

                    </Link>
                    <a title={channelHot.name}><span className="sp-chanel-name">{channelHot.name}</span>
                        <span className="sp-chanel-follow">
                        {Helper.formatNumber(numberFollow)} followers
                    </span>
                    </a>
                    {!isFollow ?
                        <a className="chanel-btn-dk cn-catenew" onClick={this.handleFollow}>Theo dõi</a>
                        :
                        <a className="chanel-btn-followed cn-catenew cn-catenew-followed" onClick={this.handleUnfollow}>Đã theo dõi</a>
                    }

                    <ModalLogin isOpen={this.state.isShowLogin}
                                body={'Quý khách chưa đăng nhập! Để hoàn thành thao tác, mời quý khách bấm OK để đăng nhập.'}
                                toggle={this.toggle}/>

                    <ModalSubcribe isOpen={this.state.isShowSubcribe}
                                   body={'Xác nhận bỏ theo dõi kênh?'}
                                   callbackUnfollow={this.callbackUnfollow}
                                   toggle={this.toggle}/>
                </div>
            );
        }

    }
}

export default ChannelItem;
