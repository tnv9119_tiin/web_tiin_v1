import React, {Component} from 'react';
import VideoNew from './Partial/VideoNews';
import ChannelHotList from './Partial/ChannelHotList';
import VideoHotList from './Partial/VideoHotList';
import CateInfo from './Partial/CateInfo';
import VideoBoxAll from "../../components/Video/partials/VideoBoxAll";
import LoadingScreen from "../Global/Loading/LoadingScreen";

class CategoryList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: true,
            isShowAll: false
        };
    }

    showAll=()=> {
        this.setState({
            isLoaded: false
        }, () => {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
            var _self = this;
            setTimeout(function () {
                _self.setState({
                    isShowAll: true,
                    isLoaded: true
                });
            }, 1000);
        });

    }

    render() {
        const {video, videoHot, channelHot, cateInfo, idSearch} = this.props;
        const {isLoaded, isShowAll} = this.state;

        if (!isLoaded) {
            return <LoadingScreen/>;
        } else {
            if (isShowAll) {
                return (
                    <div>
                        <CateInfo cateInfo={cateInfo}/>
                        <VideoBoxAll title={"VIDEO " + cateInfo.categoryname.toUpperCase()} idSearch={idSearch}
                                     isCate="1"/>
                    </div>
                );
            } else {
                return (
                    <div>
                        <CateInfo cateInfo={cateInfo}/>
                        <div id="lstMain">
                            <div className="body-phim-center" style={{overflow: 'initial !important'}}>
                                <div className="body-phim-left">
                                    <VideoHotList videoHot={videoHot}/>
                                    <ChannelHotList channelHot={channelHot}/>
                                </div>
                                <VideoNew video={video} showAllOnclick={this.showAll}/>
                            </div>
                        </div>
                    </div>
                );
            }
        }

    }
}

export default CategoryList;
