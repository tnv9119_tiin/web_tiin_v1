import React, {Component} from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
//import {Link} from "../../../routes";
import Link from 'next/link';
import Slider from "react-slick/lib";
import Helper from "../../../utils/helpers/Helper";

var dragging = false;

class Phim extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            numberSlide: 4
        };
    }

    render() {
        const {videos, title, path} = this.props;
        const {numberSlide} = this.state;

        if (videos && videos.length > 0) {
            let setLength = videos.length;
            if (videos.length >= 10) {
                setLength = 10;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                            right: -13,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="box-phim-keeng mocha-video-phim">
                    <div className="wrap-bpk">
                        <h3 className="phim-keeng-h3">
                            <Link href={path} as ={path + '.html'}>
                                {title}
                            </Link>
                        </h3>
                        {/*<SlideResponsive video={videos} type={'boxFilmHome'}/>*/}

                        <div className="owl-slider-av">
                            <Slider {...settings}>
                                {this.showVideos(videos)}
                            </Slider>
                        </div>

                    </div>
                </div>
            );
        } else {
            return '';
        }
    }

    showVideos(videos) {
        return videos.map((video, index) => {
            if (video && index < videos.length && index < 10) {
                let link = Helper.replaceUrl(video.link) + ".html?src=home";
                return (
                    <div className="av-top-small swiper-slide swiper-slide" key={index}>
                        <Link href="/video" as={link }
                        >
                            <a className="ats-lnk-img"
                               title={video.filmGroup.groupName}
                               onClick={(e) => {
                                   dragging && e.preventDefault();
                               }}
                               draggable={false}>
                                <img alt={video.filmGroup.groupName} title={video.filmGroup.groupName}
                                     className="ats-img"
                                     src={video.filmGroup.groupImage ? video.filmGroup.groupImage : DEFAULT_IMG}
                                     onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = DEFAULT_IMG
                                     }}
                                     style={{width: '222px'}}
                                />
                                {video.filmGroup.currentVideo ?
                                    <span className="sp-number-film subSp">
                                    {video.filmGroup.currentVideo}
                                </span>
                                    : ''
                                }
                            </a>

                        </Link>
                        <div className="ats-info">
                            <h3 className="ats-info-h3">
                                <Link href="/video"as={link}
                                >
                                    <a  className="ats-info-lnk"
                                        title={video.filmGroup.groupName}
                                        onClick={(e) => {
                                            dragging && e.preventDefault();
                                        }}
                                        draggable={false}>
                                        {video.filmGroup.groupName}
                                        <span className="sp-chanel-follow">
                                                {Helper.formatNumber(video.isView)} lượt xem</span>
                                    </a>

                                </Link>
                            </h3>
                        </div>
                    </div>
                );
            }
        });
    }


}

export  default  Phim;