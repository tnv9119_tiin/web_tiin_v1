import React  from 'react';
import {DEFAULT_IMG} from "../../../config/Config";
//import {Link} from "../../../routes";
import Link from 'next/link';

import Slider from "react-slick/lib";
import LoadingScreen from "../../Global/Loading/LoadingScreen";
import Helper from "../../../utils/helpers/Helper";

var dragging = false;

 class Channel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            numberSlide: 5
        };
    }

    render() {
        const {channels} = this.props;
        const {numberSlide} = this.state;

        if (channels && channels.length > 0) {
            let setLength = channels.length;
            if (channels.length >= 10) {
                setLength = 10;
            }

            const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-back"
                        aria-hidden="true"
                        aria-disabled={currentSlide === 0}
                        type="button"
                        style={{
                            display: currentSlide === 0 ? 'none' : 'block',
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
                <button onClick={onClick}
                        {...props}
                        className="owl-slider-next"
                        aria-hidden="true"
                        aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                        type="button"
                        style={{
                            display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                            right: -13,
                            border: 'none',
                            outline: 'none'
                        }}>
                </button>
            );

            //swipeToSlide
            const settings = {
                className: "center",
                arrows: true,
                infinite: false,
                centerPadding: "50px",
                slidesToShow: numberSlide,
                swipeToSlide: true,
                prevArrow: <PrevButton/>,
                nextArrow: <NextButton/>,
                beforeChange: () => dragging = true,
                afterChange: () => dragging = false
            };

            return (
                <div className="owl-slider-av">
                    <Slider {...settings}>
                        {this.showChannels(channels)}
                    </Slider>
                </div>
            );
        } else {
            return <LoadingScreen/>;
        }
    }

    showChannels(channels) {
        return channels.map((channel, index) => {
            if (channel && index < channels.length && index < 10) {
                return <div className="cate-hot-item swiper-slide swiper-slide-active" key={index}>
                    <Link href={Helper.replaceUrl(channel.link)} as={Helper.replaceUrl(channel.link) + '.html'}
                    >
                        <a title={channel.name}
                           onClick={(e) => {
                               dragging && e.preventDefault();
                           }}
                           draggable={false}>
                            <img className="ats-img animated" alt={channel.name} src={channel.url_avatar}
                                 onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = DEFAULT_IMG
                                 }}
                            />
                        </a>

                    </Link>

                    <Link href ={Helper.replaceUrl(channel.link)} as ={Helper.replaceUrl(channel.link) +'.html'}
                    >
                        <a title={channel.name}
                           onClick={(e) => {
                               dragging && e.preventDefault();
                           }}
                           draggable={false}>
                            <span className="sp-chanel-name">{channel.name} </span>
                            <span className="sp-chanel-follow">
                                {Helper.formatNumber(channel.numfollow)} followers
                            </span>
                        </a>

                    </Link>

                </div>
            }
        });
    }
}


export  default  Channel;