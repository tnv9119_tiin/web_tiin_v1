import React  from "react";
import Slider from "react-slick";
import VideoItem from "../Video/partials/VideoItem";
import FilmItem from "../Film/FilmItem";
import ChannelItem from "../Channel/Partial/ChannelItem";
import ChannelItemSub from '../Category/Partial/ChannelItem';

class SlideResponsive extends React.Component {
    constructor(props) {
        super(props);
        var numberItem = this.props.numberItem;
        var numberSlide = 4;
        if (numberItem) {
            numberSlide = numberItem;
        }
        this.state = {
            videoNew: '',
            videoHai: '',

            numberSlide: numberSlide
        };
    }

    render() {
        var videos = this.props.video;
        let setLength = videos.length;
        if (videos.length >= 10) {
            setLength = 10;
        }

        const PrevButton = ({onClick, currentSlide, slideCount, ...props}) => (
            <button onClick={onClick}
                    {...props}
                    className="owl-slider-back"
                    aria-hidden="true"
                    aria-disabled={currentSlide === 0}
                    type="button"
                    style={{
                        display: currentSlide === 0 ? 'none' : 'block',
                        border: 'none',
                        outline: 'none'
                    }}>
            </button>
        );

        const NextButton = ({onClick, currentSlide, slideCount, ...props}) => (
            <button onClick={onClick}
                    {...props}
                    className="owl-slider-next"
                    aria-hidden="true"
                    aria-disabled={currentSlide >= setLength - this.state.numberSlide}
                    type="button"
                    style={{
                        display: (currentSlide >= setLength - this.state.numberSlide) ? 'none' : 'block',
                        right: -13,
                        border: 'none',
                        outline: 'none'
                    }}>
            </button>
        );

        //swipeToSlide
        const settings = {
            className: "center",
            arrows: true,
            infinite: false,
            centerPadding: "50px",
            slidesToShow: this.state.numberSlide,
            swipeToSlide: true,
            prevArrow: <PrevButton/>,
            nextArrow: <NextButton/>
        };

        return (
            <div className="owl-slider-av">
                <Slider {...settings}>
                    {this.showVideos(videos)}
                </Slider>
            </div>
        );
    }

    showVideos(videos) {
        var result = null;
        var boxFilmSearch = this.props.type;
        if (videos) {
            if (videos.length > 0) {
                result = videos.map((video, index) => {
                    if (video && index < 10) {
                        if (index < videos.length) {
                            if (boxFilmSearch && boxFilmSearch === 'boxFilmSearch' || boxFilmSearch === 'boxFilmHome') {
                                return (
                                    <FilmItem key={index} video={video} type={boxFilmSearch}/>
                                )
                            } else if (boxFilmSearch && boxFilmSearch === 'boxChannelSearch') {
                                return (
                                    <ChannelItem key={index} video={video}/>
                                )
                            } else if (boxFilmSearch && boxFilmSearch === 'boxChannelFollow') {
                                return (
                                    <ChannelItemSub key={index} channelHot={video}/>
                                )
                            } else {
                                return (
                                    <VideoItem key={index} video={video}/>
                                )
                            }
                        }
                    }
                });
            }
        }
        return result;
    }
}

export default SlideResponsive;

