import { createStore,applyMiddleware } from 'redux';
import rootReducer from './../reducers/index';
import { composeWithDevTools } from 'redux-devtools-extension';

// initialState
const initialState = {};

// Create store
//export const store = createStore(rootReducer, initialState);

export function initializeStore(state = initialState) {
    return createStore(
        rootReducer,
        state,
        composeWithDevTools(applyMiddleware())
    )
}
