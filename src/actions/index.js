// export const SET_USER = 'SET_USER'

import {HIDE_LOADING_SCREEN, SHOW_LOADING_SCREEN} from "../constants/actionTypes";

export function showLoadingScreen(){
  return {
    type: SHOW_LOADING_SCREEN
  };
}

export function hideLoadingScreen(){
  return {
    type: HIDE_LOADING_SCREEN
  };
}
