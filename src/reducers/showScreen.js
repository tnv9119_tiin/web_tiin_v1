import {SHOW_LOADING_SCREEN, HIDE_LOADING_SCREEN} from "../constants/actionTypes";

const initialState = {
    text: 'Redux',
    isShowLoading: false
};

export default function todoShowScreen(state = initialState, action) {
    switch (action.type) {
        case SHOW_LOADING_SCREEN:
            return {
                ...state,
                text: 'Redux - SHOW_LOADING_SCREEN',
                isShowLoading: true
            };
        case HIDE_LOADING_SCREEN:
            return {
                ...state,
                text: 'Redux - HIDE_LOADING_SCREEN',
                isShowLoading: false
            };

        default:
            return state;
    }
}
