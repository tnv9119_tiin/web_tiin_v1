import { combineReducers } from 'redux';
import showScreen from './showScreen';

const rootReducer = combineReducers({
  showScreen
});

export default rootReducer;