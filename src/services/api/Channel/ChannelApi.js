import ApiCaller from "../../../utils/ApiCaller";
import * as Config from '../../../config/Config';
import Helper from "../../../utils/helpers/Helper";
import HelperServer from "../../../utils/helpers/HelperServer";

function getVideoChannel(idChannel, offset = 0, lastIdStr) {

    return ApiCaller.callApiMocha(Config.API_LIST_VIDEO_BY_CHANNEL + '?channelid=' + idChannel + '&limit=20&offset=' + offset + '&lastIdStr='+lastIdStr+'&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}
function getVideoChannelServer(idChannel, offset = 0, lastIdStr,cookie) {

    return ApiCaller.callApiMocha(Config.API_LIST_VIDEO_BY_CHANNEL + '?channelid=' + idChannel + '&limit=20&offset=' + offset + '&lastIdStr='+lastIdStr+'&token=' + HelperServer.getTokenParamForServer(cookie),
        'GET', null);
}

function getVideoMyChannel(idChannel, offset = 0, lastIdStr) {
    return ApiCaller.callApiMocha(Config.API_LIST_VIDEO_MY_CHANNEL + '?channelid=' + idChannel + '&limit=20&offset=' + offset + '&lastIdStr='+ lastIdStr+'&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getInfoMyChannel() {
    return ApiCaller.callApiMocha(Config.API_GET_MY_CHANNEL_INFO + '?token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getInfoChannel(idChannel) {
    return ApiCaller.callApiMocha(Config.API_GET_CHANNEL_INFO + '?channelid=' + idChannel + '&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getVideoHot(idChannel) {
    return ApiCaller.callApiMocha(Config.API_TOP_VIDEO_OF_CHANNEL + '?channelid=' + idChannel + '&limit=5&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getFilmGrp(idChannel, page = 0) {
    return ApiCaller.callApiMocha(Config.API_LIST_FILM_OF_CHANNEL + '?channelId=' + idChannel + '&limit=20&offset=' + page + '&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getChannelFollowOfChannel(idChannel, offset = 20) {
    return ApiCaller.callApiMocha(Config.API_LIST_CHANNEL_FOLLOW_OF_CHANNEL + '?channelId=' + idChannel + '&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getChannelRelated(offset, channelid, channelname) {
    return ApiCaller.callApiMocha(Config.API_GET_CHANNEL_RELATED + '?limit=10&offset=0&channelid=' + channelid + '&channelname=' + encodeURIComponent(channelname) + '&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}



function getChannelUserFollow() {
    return ApiCaller.callApiMocha(Config.API_LIST_CHANNEL_FOLLOWED + '?limit=20&offset=0&token=' + Helper.getTokenParamForUrl(), 'GET', null);
}
function getChannelUserFollowServer(cookie) {
    return ApiCaller.callApiMocha(Config.API_LIST_CHANNEL_FOLLOWED + '?limit=20&offset=0&token=' +HelperServer.getTokenParamForServer(cookie), 'GET', null);
}

function getVideoNewPublish(lastIdStr = '') {
    return ApiCaller.callApiMocha(Config.API_LIST_VIEO_NEWPUBLISH + '?limit=20&offset=0&lastIdStr=' + encodeURIComponent(lastIdStr) + '&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getVideoNewPublishServer(lastIdStr,cookie) {
    return ApiCaller.callApiMocha(Config.API_LIST_VIEO_NEWPUBLISH + '?limit=20&offset=0&lastIdStr=' + encodeURIComponent(lastIdStr) + '&token=' + HelperServer.getTokenParamForServer(cookie),
        'GET', null);
}

function getChannelOfUser() {
    return ApiCaller.callApiMocha(Config.API_GET_MY_CHANNEL + '?token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}

function getStatOfChannel(id) {
    return ApiCaller.callApiMocha(Config.API_GET_STAT_OF_CHANNEL + '?channelid=' + id + '&limit=20&offset=0&lastIdStr=&token=' + Helper.getTokenParamForUrl(),
        'GET', null);
}
function deleteCache(channelid) {
    return ApiCaller.callApiMocha(Config.API_DELETE_CACHE + '?msisdn=&channelid=' + channelid,
        'GET', null);
}
function deleteCacheChannel(channelid) {
    return ApiCaller.callApiMocha(Config.API_DELETE_CACHE_CHANNEL + '?msisdn=&channelid=' + channelid,
        'GET', null);
}


function followChannel(id) {
    let dataPost = {
        channelId: id,
        status: 1,
        token: Helper.getToken()
    };

    return ApiCaller.callApiMocha(Config.API_FOLLOW_CHANNEL, 'POST', dataPost);
}

function unfollowChannel(id) {
    let dataPost = {
        channelId: id,
        status: 0,
        token: Helper.getToken()
    };

    return ApiCaller.callApiMocha(Config.API_FOLLOW_CHANNEL, 'POST', dataPost);
}


export default {
    getVideoChannel,
    getVideoChannelServer,
    getVideoMyChannel,
    getInfoMyChannel,
    getInfoChannel,
    getVideoHot,
    getFilmGrp,
    getChannelFollowOfChannel,
    getChannelRelated,

    getChannelUserFollow,
    getChannelUserFollowServer,
    getChannelOfUser,
    getVideoNewPublish,
    getVideoNewPublishServer,
    getStatOfChannel,
    followChannel,
    unfollowChannel,
    deleteCache,
    deleteCacheChannel,
};
