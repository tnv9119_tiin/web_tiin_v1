import ApiCaller from "../../../utils/ApiCaller";
import {
    API_LIST_VIDEO_BY_CATE,
    API_GET_VIDEO_DETAIL,
    API_GET_VIDEO_RELATED,
    API_LIST_VOLUME_FILM,
    API_TOP_VIDEO_OF_CHANNEL, API_GET_FILM_RELATED, API_PUSH_ACTION,API_PUSH_ACTION_SHARE, DOMAIN
} from "../../../config/Config";
import Helper from "../../../utils/helpers/Helper";
import HelperServer from "../../../utils/helpers/HelperServer";

function getVideoDetail(path, strGetLocation) {
    let endpoint = API_GET_VIDEO_DETAIL + '?url=' + encodeURIComponent(DOMAIN + path + '.html') + '&token=' + Helper.getTokenParamForUrl();
    let headers = {
        'content-type': 'application/x-www-form-urlencoded',
        'geoIP': strGetLocation ? strGetLocation : ''
    };
    return ApiCaller.callApiMocha(endpoint, 'GET', null, headers);
}

function getVideoDetailServer(path, strGetLocation,cookie) {
    let endpoint = API_GET_VIDEO_DETAIL + '?url=' + encodeURIComponent(DOMAIN + path + '.html') + '&token=' + HelperServer.getTokenParamForServer(cookie);
    let headers = {
        'content-type': 'application/x-www-form-urlencoded',
        'geoIP': strGetLocation ? strGetLocation : ''
    };
    return ApiCaller.callApiMocha(endpoint, 'GET', null, headers);
}

function getVideoRelated(cateId, name, url) {
    let endpoint = API_GET_VIDEO_RELATED + '?cateId=' + cateId +'&clientType=ios&domain=hl2.mocha.com.vn&lastIdStr=&limit=20&msisdn=0376636544&networkType=3G&offset=0&q='+ encodeURI(name)+ '&revision=11030&security=QhYK/Un4DElmPYxit0awS3CvspV7%2BfdqGpAftekZ%2B4BjmCrljyjVz7D0u7YlHFirAKnnlBnb8VwM/9EBytF4H974S4v7H7amSAjMI3hQ5GyXVSVr5ZQnaF9Gvonjd7nkpg1ZCGcjwChnqmN8WGBJv6%2B9TqFw8RZd1ubslv4d6g8%3D&timestamp=1555404109880&url=' + encodeURIComponent(url) + '&video_type=&vip=NOVIP';
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function getFilmRelated(filmGroupsID, videoId) {
    let endpoint = API_GET_FILM_RELATED + '?filmId=' + filmGroupsID + '&videoId=' + videoId + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function getFilmRelatedServer(filmGroupsID, videoId,cookie) {
    let endpoint = API_GET_FILM_RELATED + '?filmId=' + filmGroupsID + '&videoId=' + videoId + '&token=' + HelperServer.getTokenParamForServer(cookie);
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function getVideoSameChannel(channelId) {
    let endpoint = API_TOP_VIDEO_OF_CHANNEL +'?channelid=' + channelId + '&limit=5&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function getVideoSameChannelServer(channelId,cookie) {
    let endpoint = API_TOP_VIDEO_OF_CHANNEL +'?channelid=' + channelId + '&limit=5&token=' + HelperServer.getTokenParamForServer(cookie);
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function getVolumeFilmGroups(filmGroupsID) {

    let endpoint = API_LIST_VOLUME_FILM + '?filmGroupID=' + filmGroupsID + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function getVolumeFilmGroupsServer(filmGroupsID,cookie) {

    let endpoint = API_LIST_VOLUME_FILM + '?filmGroupID=' + filmGroupsID + '&token=' +  HelperServer.getTokenParamForServer(cookie);
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function likeVideo(video) {
    let channel = (Helper.checkArrNotEmpty(video, 'channels') && video.channels[0]) ? video.channels[0] : null;
    let data = {
        actionType: "LIKE",
        itemId: String(video.id),
        url: video.link,
        itemName: video.name,
        imgUrl: video.image_path,
        mediaUrl: video.original_path,

        channelId: channel ? String(channel.id) : '',
        channelName: channel ? channel.name : '',
        channelAvatar: channel ? channel.url_avatar : ''
    };

    let timeStamp = new Date().getTime();
    let token = Helper.getToken();
    let strData = JSON.stringify(data);
    let dataPost = {
        secinf: video.secinf,
        data: strData,
        token: token,
        timestamp: timeStamp,
        security: Helper.encryptStr(Helper.md5Str(token + timeStamp + video.secinf + strData)),
        platform: 'WEB'
    };

    return ApiCaller.callApiMocha(API_PUSH_ACTION, 'POST', dataPost);

}

function unlikeVideo(video) {
    let channel = (Helper.checkArrNotEmpty(video, 'channels') && video.channels[0]) ? video.channels[0] : null;
    let data = {
        actionType: "UNLIKE",
        itemId: String(video.id),
        url: video.link,
        itemName: video.name,
        imgUrl: video.image_path,
        mediaUrl: video.original_path,

        channelId: channel ? String(channel.id) : '',
        channelName: channel ? channel.name : '',
        channelAvatar: channel ? channel.url_avatar : ''
    };

    let timeStamp = new Date().getTime();
    let token = Helper.getToken();
    let strData = JSON.stringify(data);
    let dataPost = {
        secinf: video.secinf,
        data: strData,
        token: token,
        timestamp: timeStamp,
        security: Helper.encryptStr(Helper.md5Str(token + timeStamp + video.secinf + strData)),
        platform: 'WEB'
    };

    return ApiCaller.callApiMocha(API_PUSH_ACTION, 'POST', dataPost);

}

function shareVideo(video) {
    let channel = (Helper.checkArrNotEmpty(video, 'channels') && video.channels[0]) ? video.channels[0] : null;
    let data = {
        actionType: "SHARE",
        itemId: String(video.id),
        url: video.link,
        itemName: video.name,
        imgUrl: video.image_path,
        mediaUrl: video.original_path,

        channelId: channel ? String(channel.id) : '',
        channelName: channel ? channel.name : '',
        channelAvatar: channel ? channel.url_avatar : ''
    };

    let timeStamp = new Date().getTime();
    let token = Helper.getToken();
    let strData = JSON.stringify(data);
    let dataPost = {
        secinf: video.secinf,
        data: strData,
        token: token,
        timestamp: timeStamp,
        security: Helper.encryptStr(Helper.md5Str(token + timeStamp + video.secinf + strData)),
        platform: 'WEB'
    };

    return ApiCaller.callApiMocha(API_PUSH_ACTION_SHARE, 'POST', dataPost);
}


export default {
    getVideoDetail,
    getVideoDetailServer,
    getVideoRelated,
    getFilmRelated,
    getFilmRelatedServer,
    getVolumeFilmGroups,
    getVolumeFilmGroupsServer,
    getVideoSameChannel,
    getVideoSameChannelServer,
    likeVideo,
    unlikeVideo,
    shareVideo
};
