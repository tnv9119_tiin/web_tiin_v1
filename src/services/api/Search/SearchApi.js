import Helper from "../../../utils/helpers/Helper";
import {API_SEARCH_CHANNEL, API_SEARCH_FILM, API_SEARCH_VIDEO, API_SEARCH_VIDEO_OFCHANNEL} from "../../../config/Config";
import ApiCaller from "../../../utils/ApiCaller";
import HelperServer from "../../../utils/helpers/HelperServer";


function searchVideo(query) {
    let endpoint = API_SEARCH_VIDEO + '?limit=20&offset=0&q=' + encodeURIComponent(query) + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

function searchVideoServer(query,cookie) {
    let endpoint = API_SEARCH_VIDEO + '?limit=20&offset=0&q=' + encodeURIComponent(query) + '&token=' + HelperServer.getTokenParamForServer(cookie);
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}
function searchFilm(query) {
    let endpoint = API_SEARCH_FILM + '?limit=20&offset=0&q=' + encodeURIComponent(query) + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}
function searchChannel(query) {
    let endpoint = API_SEARCH_CHANNEL + '?limit=20&offset=0&q=' + encodeURIComponent(query) + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}
function searchVideoOfChannel(query,channelId) {
    let endpoint = API_SEARCH_VIDEO_OFCHANNEL + '?channelId=' + channelId + '&limit=20&offset=0&q=' + encodeURIComponent(query) + '&token=' + Helper.getTokenParamForUrl();
    return ApiCaller.callApiMocha(endpoint, 'GET', null);
}

export default {
    searchVideo,
    searchVideoServer,
    searchChannel,
    searchFilm,
    searchVideoOfChannel
};
