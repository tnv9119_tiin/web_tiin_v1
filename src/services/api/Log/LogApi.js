import Helper from "../../../utils/helpers/Helper";
import ApiCaller from "../../../utils/ApiCaller";
import {API_PUSH_LOG, API_PUSH_LOG_TRAFFIC, IS_LOG_TRAFFIC} from "../../../config/Config";


function pushLog(data) {
    let timeStamp = new Date().getTime();
    let dataPost = {
        dataEnc: Helper.encryptData(data),
        videoId: data.videoId,
        url: data.url,
        psid: Helper.md5Str(String(data.videoId + timeStamp)),
        security: Helper.encryptStr(Helper.md5Str(timeStamp + JSON.stringify(data))),
        timestamp: timeStamp,
        platform: 'WEB',
        token: Helper.getToken()
    };
    let headers = {
        'content-type': 'application/x-www-form-urlencoded',
        'clientId': Helper.getClientId()
    };
    return ApiCaller.callApiMocha(API_PUSH_LOG, 'POST', dataPost, headers).then(
        (response) => {
            if(response&&response.data)
            {
                Helper.checkTokenExpired(response.data);
                Helper.renewToken(response.data);
            }

        });
}

function pushLogTraffic(data) {
    let timeStamp = new Date().getTime();
    let dataPost = {
        dataEnc: Helper.encryptData(data),
        timestamp: timeStamp,
        security: Helper.encryptStr(Helper.md5Str(timeStamp + JSON.stringify(data))),
    };

    ApiCaller.callApiMocha(API_PUSH_LOG_TRAFFIC, 'POST', dataPost).then(
        (response) => {
            if(response && response.data)
            {
                Helper.checkTokenExpired(response.data);
                Helper.renewToken(response.data);

                if (response.data.code === 200) {
                    var timeExp = new Date();
                    timeExp.setMinutes(timeExp.getMinutes() + 2);
                    Helper.setCookie(IS_LOG_TRAFFIC, 1, timeExp);
                }
            }

        });
}

export default {
    pushLog,
    pushLogTraffic
};
