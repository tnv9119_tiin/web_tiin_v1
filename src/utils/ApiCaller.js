import axios from 'axios';
import * as Config from './../config/Config';
import qs from 'qs';

function callApiMocha(endpoint, method = 'GET', body = null, headers = null) {
    if (!headers) headers = {
        'content-type': 'application/x-www-form-urlencoded'
    };

    return axios({
        method: method,
        url: `${Config.MV_SERVICE}${endpoint}`,
        data: qs.stringify(body),
        proxy: {
            host: Config.HOST_PROXY,
            port: Config.PORT_PROXY
        },
        headers: headers
    }).catch(err => {
        console.log(err);
    });
}


function callApiUploadImage(endpoint, method = 'POST', body) {
    return axios({
        method: method,
        url: `${Config.MV_SERVICE}${endpoint}`,
        data: body,
        config: {
            headers: {'Content-Type': 'multipart/form-data'}
        },
        proxy: {
            host: Config.HOST_PROXY,
            port: Config.PORT_PROXY
        }
    });
}


function callApiPostFormData(endpoint, method = 'POST', body) {
    return axios({
        method: method,
        url: `${Config.MV_SERVICE}${endpoint}`,
        data: body,
        config: {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        },
        proxy: {
            host: Config.HOST_PROXY,
            port: Config.PORT_PROXY
        }
    });
}

export default {
    callApiMocha,
    callApiUploadImage,
    callApiPostFormData
};
