import {DECRYPT_KEY} from "../../config/Config";
var xxtea = require('xxtea-node');


function getCookieServer(cookie,key) {
    var name = key + "=";
    var decodedCookie = decodeURIComponent(cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

function checkUserLoggedInServer(cookie) {
    let user = getCookieServer(cookie,'userInfo');
    return !!user;
}

function getTokenParamForServer(cookie) {
    let userInfo =getCookieServer(cookie,'userInfo');
    if (userInfo) {
        var dataDecrypted = JSON.parse(xxtea.decryptToString(userInfo, DECRYPT_KEY));
        return encodeURIComponent(dataDecrypted.token);
    }

    return '';
}



export  default  {
    getCookieServer,
    checkUserLoggedInServer,
    getTokenParamForServer
}