import React, {Component} from "react";
import VideoPlayer from "../src/components/Video/VideoDetail/VideoPlayer";
import VideoInfo from "../src/components/Video/VideoDetail/VideoInfo";
import VideoRelated from "../src/components/Video/VideoDetail/VideoRelated";
import VideoSameChannel from "../src/components/Video/VideoDetail/VideoSameChannel";
import VideoApi from "../src/services/api/Video/VideoApi";

import Helper from "../src/utils/helpers/Helper";
import {DOMAIN_IMAGE_STATIC, API_GET_LOCATION, ID_CHANNEL_NOT_SHOW, HOST_PROXY, PORT_PROXY} from "../src/config/Config";
import FilmEpisode from "../src/components/Video/VideoDetail/FilmEpisode";
import FilmRelated from "../src/components/Video/VideoDetail/FilmRelated";
import SearchApi from "../src/services/api/Search/SearchApi";
import Comment from "../src/components/Comment/Comment";

import Error from "../src/components/Error";
import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";

import Metaseo from "./metaseo";
import {Router} from '../src/routes';
import axios from 'axios';

import CircularJSON from "circular-json";
import NoSSR from 'react-no-ssr';
import LogApi from "../src/services/api/Log/LogApi";

const ver4_17 = DOMAIN_IMAGE_STATIC + "ver4_17.png";
const ver4_17_a = DOMAIN_IMAGE_STATIC + "ver4_17_a.png";


var _isMounted = false;

class Video extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isError: false,
            isLoaded: false,

            geoIp: null,
            pathname: null,
            autoPlay: true,
            items: null,
            isFilm: false,
            videoRelated: [],
            videoSameChannel: [],

            listFilm: []
        };
    }

    static async getInitialProps({req, res, asPath}) {
        var props = {
            isServer: false,
            isLoaded: false,
            items: {},
            isFilm: false,
            listFilm: [],
            isError: false,
            filmsRelated: [],
            videoRelated: [],
            videoSameChannel: [],
            pathname: asPath,
            path: asPath

        };
        if (req) {
            const cookie = req.headers.cookie;
            props.isServer = true;
            _isMounted = true;
            let location = '', filmGroupsID = 0, videoId = 0, name = '', channelId = 0;
            var pathname = Helper.replaceUrl(asPath);
            pathname = Helper.getPathname(pathname);
            props.pathname = pathname;

            await axios({
                method: "get",
                url: API_GET_LOCATION,
                // proxy: {
                //     host: HOST_PROXY,
                //     port: PORT_PROXY
                // },
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function (response) {
                    // handle success
                    location = response.data;
                })
                .catch((error) => {
                    props.error = error.response ? error.response.status : true;
                });

            await VideoApi.getVideoDetailServer(pathname, location, cookie).then(
                (res) => {
                    if (res && res.data) {
                        props.isError = false;
                        let data = res.data;
                        if (_isMounted && Helper.checkObjExist(data, 'data.videoDetail') && data.data.videoDetail.id) {
                            name = data.data.videoDetail.name;
                            channelId = data.data.videoDetail.channels[0].id;

                            videoId = data.data.videoDetail.id;
                            let link = data.data.videoDetail.link;
                            filmGroupsID = data.data.videoDetail.filmGroupsID;
                            let isFilm = !!filmGroupsID;

                            props.isLoaded = true;
                            props.items = data.data.videoDetail;
                            props.isFilm = isFilm;

                        } else {
                            props.isError = false;
                        }
                    }
                },
                (error) => {
                    props.isError = true;
                });

            if (props.isFilm) {

                await Promise.all([
                    VideoApi.getVolumeFilmGroupsServer(filmGroupsID, cookie),
                    VideoApi.getFilmRelatedServer(filmGroupsID, videoId, cookie)
                ]).then((responses) => {
                        if (responses) {
                            props.isError = false;
                            props.isLoaded = true;
                            let responseFilmGroup = JSON.parse(CircularJSON.stringify(responses))[0];
                            let responseFilmRelated = JSON.parse(CircularJSON.stringify(responses))[1];
                            props.listFilm = responseFilmGroup.data.result;
                            props.filmsRelated = responseFilmRelated.data.data.suggestFilm;
                        }
                    },
                    (error) => {
                        props.isError = true;
                    });

            } else {
                //Get video related (remove the first item)
                await SearchApi.searchVideoServer(name, cookie).then(
                    (response) => {
                        //remove the first item
                        if (response && response.data) {
                            props.isError = false;
                            if (_isMounted && response.data) {
                                let listVideo = Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : [];
                                listVideo.shift();

                                props.videoRelated = listVideo;
                            }
                        }

                    },
                    (error) => {
                        props.isError = true;
                    });

                if (channelId) {
                    //Get video same channel
                    await VideoApi.getVideoSameChannelServer(channelId, cookie).then(
                        (response) => {
                            if (response && response.data) {
                                if (_isMounted && Helper.checkObjExist(response.data, 'data.topFiveAll')) {
                                    props.videoSameChannel = response.data.data.topFiveAll;
                                }
                            }
                        },
                        (error) => {
                            props.isError = true;
                        });
                }

            }

        }

        return {props};
    }

    switchAutoPlay = () => {
        this.setState({
            autoPlay: !this.state.autoPlay
        });
    }

    componentDidMount() {

        if (!this.props.props.isServer) {
            let pathname = Helper.replaceUrl(Router.asPath);

            pathname = Helper.getPathname(pathname);
            this.fetchData(pathname);
        }

    }

    componentWillUnmount() {
        _isMounted = false;
        window.removeEventListener('scroll', this.handleScroll);
    }


    componentDidUpdate(prevProps, prevState) {
        if (!this.props.props.isServer) {
            let pathname = Helper.replaceUrl(Router.asPath);
            pathname = Helper.getPathname(pathname);

            // window.onpopstate = (e) => {
            //     if(!this.props.props.isServer){
            //
            //         if (pathname !== this.state.pathname) {
            //             this.fetchData(pathname);
            //         }
            //     }
            // };

            if (pathname !== this.state.pathname) {
                this.fetchData(pathname);
            }
        }
    }

    fetchData(pathname) {
        if (pathname) {

            let id = Helper.getIdFromPathname(pathname);
            var playedIds = sessionStorage.getItem("playedIds");
            if (playedIds) {
                playedIds = playedIds + ',' + id;
                sessionStorage.setItem("playedIds", playedIds);
            } else {
                sessionStorage.setItem("playedIds", id);
            }

            _isMounted = true;
            var self = this;

            self.setState({
                isLoaded: false,
                pathname: pathname
            }, () => {
                var xhttp = new XMLHttpRequest();
                xhttp.open("get", API_GET_LOCATION, true);
                xhttp.send();
                xhttp.timeout = 3000;
                xhttp.onreadystatechange = function (e) {
                    if (xhttp.readyState === 4 && xhttp.status === 200) {

                        VideoApi.getVideoDetail(pathname, this.response).then(
                            ({data}) => {
                                Helper.renewToken(data);

                                if (_isMounted && Helper.checkObjExist(data, 'data.videoDetail') && data.data.videoDetail && data.data.videoDetail.id) {

                                    let name = data.data.videoDetail.name;
                                    let channelId = Helper.checkArrNotEmpty(data, 'data.videoDetail.channels') && data.data.videoDetail.channels[0] ? data.data.videoDetail.channels[0].id : 0;
                                    let videoId = data.data.videoDetail.id;
                                    let filmGroupsID = data.data.videoDetail.filmGroupsID;
                                    let isFilm = !!filmGroupsID;

                                    if (channelId == ID_CHANNEL_NOT_SHOW) {
                                        self.setState({
                                            isError: true
                                        });
                                    } else {

                                        self.setState({
                                            isLoaded: true,
                                            items: data.data.videoDetail,
                                            isFilm: isFilm
                                        }, () => {
                                            if (window.pageYOffset !== 0) {
                                                window.scrollTo(0, 0);
                                            }

                                            var userInfo = Helper.getUserInfo();
                                            var msisdn = '';
                                            if (userInfo && userInfo.username) {
                                                msisdn = userInfo.username;
                                            }
                                            let dataLogTrffic = {
                                                msisdn: msisdn,
                                                url: data.data.videoDetail.link,
                                                videoUrl: data.data.videoDetail.original_path
                                            };
                                            self.pushLogTraffic(dataLogTrffic);
                                        });

                                        if (isFilm) {
                                            VideoApi.getVolumeFilmGroups(filmGroupsID).then(
                                                ({data}) => {
                                                    Helper.renewToken(data);

                                                    if (_isMounted && Helper.checkArrNotEmpty(data, 'result')) {
                                                        self.setState({
                                                            listFilm: data.result
                                                        });
                                                    }
                                                },
                                                (error) => {
                                                    self.setState({
                                                        isError: true
                                                    });
                                                });

                                            //Phim liên quan
                                            VideoApi.getFilmRelated(filmGroupsID, videoId).then(
                                                ({data}) => {
                                                    Helper.renewToken(data);

                                                    if (_isMounted && Helper.checkArrNotEmpty(data, 'data.suggestFilm')) {
                                                        self.setState({
                                                            filmsRelated: data.data.suggestFilm
                                                        });
                                                    }

                                                },
                                                (error) => {
                                                    self.setState({
                                                        isError: true
                                                    });
                                                });

                                        } else {
                                            //Get video related (remove the first item)
                                            SearchApi.searchVideo(name).then(
                                                ({data}) => {
                                                    Helper.renewToken(data);


                                                    if (_isMounted && data) {
                                                        let listVideo = Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : [];

                                                        // Lọc danh sách video liên quan
                                                        listVideo.shift();
                                                        let arrVideo = [];
                                                        if (playedIds) {
                                                            listVideo.map((video, index) => {
                                                                if (!playedIds.includes(video.id)) {
                                                                    arrVideo.push(video);
                                                                }
                                                            });
                                                        } else {
                                                            arrVideo = listVideo;
                                                        }

                                                        self.setState({
                                                            videoRelated: arrVideo
                                                        });
                                                    }

                                                },
                                                (error) => {
                                                    self.setState({
                                                        isError: true
                                                    });
                                                });

                                            if (channelId) {
                                                //Get video same channel
                                                VideoApi.getVideoSameChannel(channelId).then(
                                                    ({data}) => {
                                                        Helper.renewToken(data);

                                                        if (_isMounted && Helper.checkObjExist(data, 'data.topFiveAll')) {
                                                            self.setState({
                                                                videoSameChannel: data.data.topFiveAll
                                                            });
                                                        }
                                                    },
                                                    (error) => {
                                                        self.setState({
                                                            isError: true
                                                        });
                                                    });
                                            }

                                        }
                                    }

                                } else {
                                    self.setState({
                                        isError: true
                                    });
                                }
                            },
                            (error) => {
                                self.setState({
                                    isError: true
                                });
                            });

                    }
                };
                xhttp.ontimeout = function(e){
                    VideoApi.getVideoDetail(pathname, this.response).then(
                        ({data}) => {
                            Helper.renewToken(data);

                            if (_isMounted && Helper.checkObjExist(data, 'data.videoDetail') && data.data.videoDetail && data.data.videoDetail.id) {

                                let name = data.data.videoDetail.name;
                                let channelId = Helper.checkArrNotEmpty(data, 'data.videoDetail.channels') && data.data.videoDetail.channels[0] ? data.data.videoDetail.channels[0].id : 0;
                                let videoId = data.data.videoDetail.id;
                                let filmGroupsID = data.data.videoDetail.filmGroupsID;
                                let isFilm = !!filmGroupsID;

                                if (channelId == ID_CHANNEL_NOT_SHOW) {
                                    self.setState({
                                        isError: true
                                    });
                                } else {

                                    self.setState({
                                        isLoaded: true,
                                        items: data.data.videoDetail,
                                        isFilm: isFilm
                                    }, () => {
                                        if (window.pageYOffset !== 0) {
                                            window.scrollTo(0, 0);
                                        }

                                        var userInfo = Helper.getUserInfo();
                                        var msisdn = '';
                                        if (userInfo && userInfo.username) {
                                            msisdn = userInfo.username;
                                        }
                                        let dataLogTrffic = {
                                            msisdn: msisdn,
                                            url: data.data.videoDetail.link,
                                            videoUrl: data.data.videoDetail.original_path
                                        };
                                        self.pushLogTraffic(dataLogTrffic);
                                    });

                                    if (isFilm) {
                                        VideoApi.getVolumeFilmGroups(filmGroupsID).then(
                                            ({data}) => {
                                                Helper.renewToken(data);

                                                if (_isMounted && Helper.checkArrNotEmpty(data, 'result')) {
                                                    self.setState({
                                                        listFilm: data.result
                                                    });
                                                }
                                            },
                                            (error) => {
                                                self.setState({
                                                    isError: true
                                                });
                                            });

                                        //Phim liên quan
                                        VideoApi.getFilmRelated(filmGroupsID, videoId).then(
                                            ({data}) => {
                                                Helper.renewToken(data);

                                                if (_isMounted && Helper.checkArrNotEmpty(data, 'data.suggestFilm')) {
                                                    self.setState({
                                                        filmsRelated: data.data.suggestFilm
                                                    });
                                                }

                                            },
                                            (error) => {
                                                self.setState({
                                                    isError: true
                                                });
                                            });

                                    } else {
                                        //Get video related (remove the first item)
                                        SearchApi.searchVideo(name).then(
                                            ({data}) => {
                                                Helper.renewToken(data);


                                                if (_isMounted && data) {
                                                    let listVideo = Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : [];

                                                    // Lọc danh sách video liên quan
                                                    listVideo.shift();
                                                    let arrVideo = [];
                                                    if (playedIds) {
                                                        listVideo.map((video, index) => {
                                                            if (!playedIds.includes(video.id)) {
                                                                arrVideo.push(video);
                                                            }
                                                        });
                                                    } else {
                                                        arrVideo = listVideo;
                                                    }

                                                    self.setState({
                                                        videoRelated: arrVideo
                                                    });
                                                }

                                            },
                                            (error) => {
                                                self.setState({
                                                    isError: true
                                                });
                                            });

                                        if (channelId) {
                                            //Get video same channel
                                            VideoApi.getVideoSameChannel(channelId).then(
                                                ({data}) => {
                                                    Helper.renewToken(data);

                                                    if (_isMounted && Helper.checkObjExist(data, 'data.topFiveAll')) {
                                                        self.setState({
                                                            videoSameChannel: data.data.topFiveAll
                                                        });
                                                    }
                                                },
                                                (error) => {
                                                    self.setState({
                                                        isError: true
                                                    });
                                                });
                                        }

                                    }
                                }

                            } else {
                                self.setState({
                                    isError: true
                                });
                            }
                        },
                        (error) => {
                            self.setState({
                                isError: true
                            });
                        });
                };
                xhttp.onerror = function (e) {
                };

            });
        }
    }

    getLinkNext = () => {
        let pathname;
        if (this.state.isFilm) {
            let videos = this.state.listFilm;

            let idx = -1;
            for (var i = 0; i < videos.length; i++) {
                if (videos[i].chapter == this.state.items.chapter) {
                    idx = i;
                    break;
                }
            }

            if (idx > -1 && videos.length > (idx + 1)) {
                pathname = Helper.replaceUrl(videos[idx + 1].link);
            }
        } else {
            let videos = this.state.videoRelated;

            if (videos.length > 0) {
                pathname = Helper.replaceUrl(videos[0].link);
            }
        }
        return pathname;
    }

    callbackNext = () => {
        let pathname = this.getLinkNext();
        if (pathname && pathname !== this.state.pathname) {
            //this.fetchData(pathname);
            Router.pushRoute(pathname);
        }
    }

    callbackError = () => {
        if (!this.state.isError) {
            this.setState({
                isError: true
            });
        }
    }

    pushLogTraffic(data) {
        let dataPost = {
            "msisdn": data.msisdn,
            "platform": "WEB",
            "clientId": Helper.getClientId(),
            "currentTime": Helper.getDateTime(),
            "action": this.state.action,
            "ip": "",
            "url": data.url,
            "videoUrl": data.videoUrl,
            "useragent": Helper.getUserAgent(),
            "sessionId": ""
        };
        LogApi.pushLogTraffic(dataPost);
    }

    render() {

        let {props} = this.props;

        let isServer = props.isServer;
        const isError = isServer ? props.isError : this.state.isError;
        const isLoaded = isServer ? props.isLoaded : this.state.isLoaded;
        const {autoPlay} = this.state;

        const items = isServer ? props.items : this.state.items;
        const isFilm = isServer ? props.isFilm : this.state.isFilm;
        const videoRelated = isServer ? props.videoRelated : this.state.videoRelated;
        const videoSameChannel = isServer ? props.videoSameChannel : this.state.videoSameChannel;
        const listFilm = isServer ? props.listFilm : this.state.listFilm;
        const filmsRelated = isServer ? props.filmsRelated : this.state.filmsRelated;

        //const {isError, isLoaded, autoPlay, items, isFilm, videoRelated, videoSameChannel, listFilm, filmsRelated} = this.state;


        if (isError) {
            return <Error/>;
        } else if (!isLoaded) {
            return <LoadingScreen/>;
        } else {
            let title = items && items.name ? items.name : 'Mocha video - Mạng xã hội chia sẻ video';
            if (items && Helper.checkArrNotEmpty(items, 'channels') && items.channels[0] && items.channels[0].name)
                title += ' - ' + items.channels[0].name;
            let meta = {
                title: title,
                image: items ? items.image_path : '',
                description: items ? items.description : '',
                url: items ? items.link : '',
                video: items ? items.original_path : '',
                type: 'video',
                path: props.path
            };
            return (
                <>
                    <Metaseo type="video" data={meta}/>
                    <div id="body">
                        <NoSSR onSSR={<LoadingScreen/>}>
                            <section>
                            </section>
                        </NoSSR>
                        {items ?
                            <VideoPlayer data={items}
                                         callbackNext={this.callbackNext}
                                         callbackError={this.callbackError}
                                         autoPlay={autoPlay}
                            />
                            : ''
                        }

                        <div className="mocha-box-login-new">

                            {items ?
                                <VideoInfo data={items}/>
                                : ''
                            }

                            {isFilm ?
                                <div>
                                    {listFilm.length > 0 ?
                                        <div>
                                            <div className="playnow-auto">
                                                <b>DANH SÁCH TẬP</b>
                                                <p className="playnow-auto-p">
                                                    Tự động phát
                                                    <a className="playnow-auto-lnk" onClick={this.switchAutoPlay}>
                                                        <img src={autoPlay ? ver4_17_a : ver4_17} alt=""/>
                                                    </a>
                                                </p>
                                            </div>

                                            <FilmEpisode films={listFilm} chapter={items.chapter}/>
                                        </div>
                                        : ''
                                    }

                                    <FilmRelated films={filmsRelated}/>
                                </div>
                                :
                                <div>
                                    {videoRelated.length > 0 ?
                                        <div>
                                            <div className="playnow-auto" id="playnow-auto-video">
                                                <b>VIDEO LIÊN QUAN</b>
                                                <p className="playnow-auto-p">
                                                    Tự động phát
                                                    <a className="playnow-auto-lnk" onClick={this.switchAutoPlay}>
                                                        <img src={autoPlay ? ver4_17_a : ver4_17} alt=""/>
                                                    </a>
                                                </p>
                                            </div>
                                            <VideoRelated videos={videoRelated}/>
                                        </div>
                                        : ''
                                    }

                                    <VideoSameChannel videos={videoSameChannel}/>
                                </div>
                            }

                            {items ?
                                <Comment data={items}/>
                                : ''
                            }

                        </div>

                    </div>

                    {(items && Helper.checkArrNotEmpty(items, 'categories') && items.categories[0] && items.categories[0].id) ?
                        <a id="internalLink" style={{display: 'none'}}
                           href={'/-cg' + items.categories[0].id + '.html'}/>
                        : ''
                    }
                </>

            );
        }

    }
}

export default Video;
