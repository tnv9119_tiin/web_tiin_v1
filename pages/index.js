//import App from '../src/main/App';
import React from "react";
//Home

import ErrorBoundary from "../src/components/ErrorBoundary";
import Helper from "../src/utils/helpers/Helper";
import CateApi from "../src/services/api/Category/CategoryApi";
import ChannelApi from "../src/services/api/Channel/ChannelApi";
import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";
import Slide from "../src/components/Slides/Slide";
import VideoNew from "../src/components/Slides/SlideVideoHome/VideoNew";
import VideoBox4Video from "../src/components/Video/partials/VideoBox4Video";
import ChannelSubscribe from "../src/components/Home/ChannelSubscribe";
import Phim from "../src/components/Slides/SlideFilmHome/Phim";
//End Home
import CircularJSON from 'circular-json';
import NoSSR from 'react-no-ssr';
import HelperServer from "../src/utils/helpers/HelperServer";
import Error from "../src/components/Error";
import Metaseo from "./metaseo";

class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
            isLoaded: false,
            videoBanner: [],
            videoNew: [],
            videoHot: [],
            videoFilm: [],
            videoHai: [],
            videoChannelSubcribe: [],
            channelSubcribe: [],
            videoSports: []
        };
    }

    // static async getInitialProps({req}) {
    //     let props = {
    //         error: false,
    //         isServer: false,
    //         isLoaded: false,
    //         videoBanner: [],
    //         videoNew: [],
    //         videoHot: [],
    //         videoChannelSubcribe: [],
    //         channelSubcribe: [],
    //         videoFilm: [],
    //         videoHai: [],
    //         videoSports: []
    //
    //     };
    //     let lastIdStr = '';
    //
    //     if (req) {
    //         const cookie = req.headers.cookie;
    //         props.isServer = true;
    //
    //         await Promise.all([
    //             CateApi.getVideoBannerServer(cookie),
    //             CateApi.getVideoAllServer(1002, 0, lastIdStr, cookie),
    //             CateApi.getVideoAllServer(1001, 0, lastIdStr, cookie),
    //             CateApi.getVideoAllFilmServer(10, 0, cookie),
    //             CateApi.getVideoAllServer(7, 0, lastIdStr, cookie),
    //             CateApi.getVideoAllServer(3, 0, lastIdStr, cookie) //1s
    //             //2s
    //         ])
    //
    //             .then((responses) => {
    //                 if (responses) {
    //                     props.isLoaded = true;
    //                     let responseBanner = JSON.parse(CircularJSON.stringify(responses))[0];
    //                     let responseNew = JSON.parse(CircularJSON.stringify(responses))[1];
    //                     let responseHot = JSON.parse(CircularJSON.stringify(responses))[2];
    //                     let responseFilm = JSON.parse(CircularJSON.stringify(responses))[3];
    //                     let responseHai = JSON.parse(CircularJSON.stringify(responses))[4];
    //                     let responseSport = JSON.parse(CircularJSON.stringify(responses))[5];
    //
    //                     props.videoBanner = Helper.checkArrNotEmpty(responseBanner.data, 'data.listVideo') ?
    //                         responseBanner.data.data.listVideo : [];
    //                     props.videoNew = Helper.checkArrNotEmpty(responseNew.data, 'data.listVideo') ?
    //                         responseNew.data.data.listVideo : [];
    //                     props.videoHot = Helper.checkArrNotEmpty(responseHot.data, 'data.listVideo') ?
    //                         responseHot.data.data.listVideo : [];
    //                     props.videoFilm = Helper.checkArrNotEmpty(responseFilm.data, 'data.listVideo') ?
    //                         responseFilm.data.data.listVideo : [];
    //                     props.videoHai = Helper.checkArrNotEmpty(responseHai.data, 'data.listVideo') ?
    //                         responseHai.data.data.listVideo : [];
    //                     props.videoSports = Helper.checkArrNotEmpty(responseSport.data, 'data.listVideo') ?
    //                         responseSport.data.data.listVideo : [];
    //                 }
    //             })
    //             .catch((error) => {
    //                 props.error = error.response ? error.response.status : true;
    //             });
    //
    //
    //         if (HelperServer.checkUserLoggedInServer(cookie)) {
    //             await Promise.all([
    //                 ChannelApi.getVideoNewPublishServer(lastIdStr, cookie),
    //                 ChannelApi.getChannelUserFollowServer(cookie)
    //             ]).then((responses) => {
    //                 if (responses) {
    //                     props.isLoaded = true;
    //                     let responseChannelSubcribe = JSON.parse(CircularJSON.stringify(responses))[0];
    //                     let responseNew = JSON.parse(CircularJSON.stringify(responses))[1];
    //                     props.videoChannelSubcribe = Helper.checkArrNotEmpty(responseChannelSubcribe.data, 'data.listVideo') ?
    //                         responseChannelSubcribe.data.data.listVideo : [];
    //                     props.channelSubcribe = Helper.checkArrNotEmpty(responseNew.data, 'data.listChannel') ?
    //                         responseNew.data.data.listChannel : [];
    //                 }
    //             })
    //                 .catch((error) => {
    //                     props.error = error.response ? error.response.status : true;
    //                 });
    //
    //         }
    //
    //     }
    //
    //     return {props}
    // }

    componentDidMount() {
         let lastIdStr = '';

            try {

                CateApi.getVideoAll(1002, 0, lastIdStr)
                    .then((response) => {
                            if (response && response.data) {
                                this.setState({
                                    error: false,
                                    isLoaded: true,
                                    videoNew: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                                })
                            }
                        }
                    )

                CateApi.getVideoAll(1001, 0, lastIdStr).then((response) => {
                        if (response && response.data) {
                            this.setState({
                                error: false,
                                isLoaded: true,
                                videoHot: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                            });
                        }
                    }
                );

                CateApi.getVideoAll(7, 0, lastIdStr).then((response) => {
                        if (response && response.data) {
                            this.setState({
                                error: false,
                                isLoaded: true,
                                videoHai: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                            });
                        }
                    }
                );

                CateApi.getVideoAllFilm(10).then((response) => {
                        if (response && response.data) {
                            this.setState({
                                error: false,
                                isLoaded: true,
                                videoFilm: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                            });
                        }
                    }
                );

                CateApi.getVideoBanner(1001).then((response) => {
                        if (response && response.data) {
                            this.setState({
                                error: false,
                                isLoaded: true,
                                videoBanner: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                            });
                        }
                    }
                );

                if (Helper.checkUserLoggedIn()) {
                    ChannelApi.getVideoNewPublish(lastIdStr)
                        .then((response) => {
                                if (response && response.data) {

                                    Helper.checkTokenExpired(response.data);
                                    if (Helper.checkArrNotEmpty(response.data, 'data.listVideo')) {
                                        this.setState({
                                            error: false,
                                            isLoaded: true,
                                            videoChannelSubcribe: response.data.data.listVideo
                                        });
                                    }
                                }

                            }
                        );


                    ChannelApi.getChannelUserFollow()
                        .then((response) => {
                                if (response && response.data) {
                                    Helper.checkTokenExpired(response.data);
                                    if (Helper.checkArrNotEmpty(response.data, 'data.listChannel')) {
                                        this.setState({
                                            error: false,
                                            isLoaded: true,
                                            channelSubcribe: response.data.data.listChannel
                                        });
                                    }
                                }
                            }
                        );

                }


                //Other
                CateApi.getVideoAll(3, 0, lastIdStr)
                    .then((response) => {
                            if (response && response.data) {
                                this.setState({
                                    error: false,
                                    videoSports: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                                });
                            }
                        }
                    )
                    .catch((error) => {
                        this.setState({
                            error: error.response ? error.response.status : true
                        });
                    });
            } catch (err) {
                this.setState({
                    error: err.response ? err.response.status : true
                });
            }
    }

    render() {
        let {isLoaded, videoBanner, videoNew, videoHot, videoChannelSubcribe, channelSubcribe, videoFilm, videoHai, videoSports} =  this.state;
        let error =  this.state.error;
        if (error) {
            return <Error/>
        } else {
            if (!isLoaded) {
                return <LoadingScreen/>;
            } else {
                // let videoBanner = props.videoBanner.length > 0 ? props.videoBanner : this.state.videoBanner;
                // let videoNew = props.videoNew.length > 0 ? props.videoNew : this.state.videoNew;
                // let videoHot = props.videoHot.length > 0 ? props.videoHot : this.state.videoHot;
                // let videoChannelSubcribe = props.videoChannelSubcribe.length > 0 ? props.videoChannelSubcribe : this.state.videoChannelSubcribe;
                // let channelSubcribe = props.channelSubcribe.length > 0 ? props.channelSubcribe : this.state.channelSubcribe;
                // let videoFilm = props.videoFilm.length > 0 ? props.videoFilm : this.state.videoFilm;
                // let videoHai = props.videoHai.length > 0 ? props.videoHai : this.state.videoHai;
                // let videoSports = props.videoSports.length > 0 ? props.videoSports : [];

                var arrayTop = [];
                for (let i = 0; i < videoBanner.length; i++) {
                    arrayTop[i] = videoBanner[i].id;
                }

                return (
                    <>
                        <Metaseo type="home"/>
                        <ErrorBoundary>
                            <div id="body">
                                <NoSSR onSSR={<LoadingScreen/>}>
                                    <section>
                                    </section>
                                </NoSSR>
                                <Slide
                                    slides={videoBanner}
                                    isServer="false"
                                />
                                <VideoNew title="MỚI CẬP NHẬT"
                                          videos={videoNew}
                                          path="/moi-cap-nhat-cg1002"/>
                                <VideoBox4Video title="VIDEO HOT"
                                                type={'hot'} array={arrayTop}
                                                videos={videoHot}
                                                path="/hot-cg1001"/>
                                {this.showChannelSubscribe(videoChannelSubcribe, channelSubcribe)}
                                <Phim title="PHIM"
                                      videos={videoFilm}
                                      path="/phim"/>
                                <VideoBox4Video title="HÀI"
                                                videos={videoHai}
                                                path="/hai-cg7"/>
                                <VideoBox4Video title="KHÁC"
                                                videos={videoSports}
                                                others={true}
                                                isServer="false"
                                                path="/more"/>
                                <div className="clear"/>
                            </div>
                        </ErrorBoundary>

                    </>
                );
            }
        }

    }


    showChannelSubscribe = (videoChannelSubcribe, channelSubcribe, isServer) => {
        return <ChannelSubscribe title="KÊNH THEO DÕI"
                                 videos={videoChannelSubcribe}
                                 channels={channelSubcribe}
        />;
    }
}

export default Index;



