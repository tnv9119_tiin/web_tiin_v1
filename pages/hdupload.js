import React, {Component} from 'react';

 class Hdupload extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
                        <meta name="keywords" content="thể lệ, chương trình, Up, video, tiền, Mocha"/>
                        <meta property="og:site_name" content="Thể lệ chương trình Up video được tiền trên Mocha"/>
                        <meta name="revisit" content="1 days"/>
                        <meta name="language" content="vi-VN"/>
                        <meta name="language" content="vi-VN"/>
                        <meta name="robots" content="index,follow,noodp"/>
                        <meta name="Googlebot" content="index,follow"/>
                        <title>Thể lệ chương trình Up video được tiền trên Mocha</title>
                    </head>

                    <body style={{fontSize: '19px',
                        lineHeight: '30px',
                        width: '100%',
                        margin: '9px 0px'}}>
                    <div className="box-rule">
                        <h3 style={{fontSize: '20px',
                            textAlign: 'center'}}>Thể lệ chương trình Up video được tiền trên Mocha</h3>
                        <p><b>1. Thời gian triển khai:</b>từ 01/7/2018 đến 31/12/2018.<br/>
                            <b>2. Đối tượng tham gia:</b> Tất cả người dùng Mocha Việt Nam<br/>
                            <b>3. Nội dung và Thể lệ chi tiết chương trình:</b><br/>
                            - Người dùng upload video lên Mocha, với mỗi lượt xem phát sinh từ video, người upload nhận
                            được 8 đồng/lượt xem, tương đương với 8 điểm tích lũy trên Mocha.<br/>
                            - Mỗi khách hàng chỉ được up 3 video/ngày, nhiều hơn số lượng này hệ thống sẽ tự động từ
                            chối.<br/>
                            - Cách upload: gửi link hoặc upload video từ máy lên.<br/>
                            <b>4. Quy định chung</b><br/>
                            - Lượt xem được tính điểm là lượt xem từ 30 giây trở lên/video.<br/>
                            - Trường hợp 01 người dùng xem nhiều lần cùng 1 nội dung video trong ngày chỉ được tính là 1
                            lần xem.<br/>
                            - 1 video chỉ được cộng tối đa 100.000 điểm/ngày.<br/>
                            - Lượt xem để cộng điểm được tính trong vòng 30 ngày kể từ ngày video được duyệt lên trang.
                            Sau 30 ngày, nếu có phát sinh thêm lượt xem, các video này sẽ không được tính điểm tích lũy.<br/>
                            - Việc cộng điểm và quy đổi điểm được thực hiện trên ứng dụng Mocha<br/>
                            - Điểm cộng chỉ có giá trị đổi thẻ nạp trong vòng 6 tháng kể từ ngày cộng điểm. Sau 6 tháng,
                            nếu khách hàng không thực hiện đổi thẻ, số điểm tích lũy 6 tháng trước đó sẽ bị xóa.<br/>
                            - Các quy định khác tuân theo tính năng điểm tích lũy trên ứng dụng Mocha<br/>
                            <b>5. Quy định về việc từ chối duyệt video</b><br/>
                            Các video sau sẽ bị từ chối :<br/>
                            - Video dài hơn 30 phút.<br/>
                            - Video trùng với nội dung đã có trên trang .<br/>
                            - Video có nội dung châm biếm về các lãnh tụ, các vấn đề chính trị, tôn giáo, đồi trụy, phân
                            biệt vùng miền, phỉ báng, lăng mạ, phạm pháp, khuyến khích hành vi phạm pháp hoặc trái với
                            thuần phong mỹ tục Việt Nam.<br/>
                            - Video có chứa thông tin cá nhân của người khác hoặc mang cá nhân ra để châm biếm.<br/>
                            - Video có nội dung quảng cáo, tuyên truyền hàng hóa, dịch vụ thuộc danh mục cấm theo quy
                            định Pháp luật.<br/>
                        </p>
                        <h3>Hướng dẫn sử dụng Up video trên Mocha</h3>
                        <p><b>1. Điện thoại</b><br/>
                            - Mở ứng dụng Mocha phiên bản Android hoặc iOS<br/>
                            - Chuyển sang mục Video và click vào nút “upload” như trên ảnh <br/>
                            <img  width="70%" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload1.jpg"/><br/>
                            - Có 2 hình thức để upload video lên Mocha, đó là:<br/>
                            &#9679;    Thông qua link youtube<br/>
                            <img  width="70%" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload2.jpg"/><br/>
                            &#9632;    Nhập link youtube có độ dài dưới 10 phút<br/>
                            &#9632;    Lựa chọn 1 trong các chuyên mục: Phim, Hài, Sao, Sống, Đẹp, Dân chơi, Nhạc, Tips,
                            Thể thao<br/>
                            <img  width="70%" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload3.jpg"/><br/>
                            &#9632;    Người dùng có thể tùy chọn bật hoặc tắt tính năng “Đồng thời đăng lên tài khoản
                            cá nhân của tôi”. Nếu người dùng bật tính năng này thì video đồng thời được cập nhật lên
                            trang cá nhân của người dùng trên mạng xã hội Mocha<br/>
                            &#9632;    Ấn nút Upload<br/>
                            &#9632;    Hệ thống thông báo việc upload thành công là hoàn tất<br/>
                            &#9679;    Thông qua file vật lý từ thiết bị<br/>
                            <img  width="70%" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload4.jpg"/><br/>
                            &#9632;    Click chọn file, tìm file trong máy và chọn<br/>
                            &#9632;    Nhập tiêu đề cho video<br/>
                            &#9632;    Nhập mô tả cho video<br/>
                            &#9632;    Lựa chọn 1 trong các chuyên mục: Phim, Hài, Sao, Sống, Đẹp, Dân chơi, Nhạc, Tips,
                            Thể thao<br/>
                            &#9632;    Người dùng có thể tùy chọn bật hoặc tắt tính năng “Đồng thời đăng lên tài khoản
                            cá nhân của tôi”<br/>
                            &#9632;    Ấn nút Upload<br/>
                            &#9632;    Hệ thống thông báo tiến trình upload và kế quả upload thành công là hoàn tất<br/>
                            <img  width="70%" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload5.jpg"/><br/>
                            <b>2. Website</b><br/>
                            - Truy cập website và tiến hành đăng nhập<br/>
                            - Click vào nút “upload” như trên ảnh<br/>
                            <img  width="70%" className="imgweb" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload6.jpg"/><br/>
                            - Có 2 hình thức để upload video lên Mocha, đó là:<br/>
                            &#9679;    Thông qua link youtube<br/>
                            <img  width="70%" className="imgweb" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload7.jpg"/><br/>
                            &#9632;    Nhập link youtube có độ dài dưới 10 phút<br/>
                            &#9632;    Lựa chọn 1 trong các chuyên mục: Phim, Hài, Sao, Sống, Đẹp, Dân chơi, Nhạc, Tips,
                            Thể thao<br/>
                            &#9632;    Ấn nút Upload<br/>
                            &#9632;    Hệ thống thông báo việc upload thành công là hoàn tất<br/>
                            &#9679;    Thông qua file vật lý từ thiết bị<br/>
                            <img  width="70%" className="imgweb" alt="hướng dẫn sử dụng up video trên mocha"
                                 src="http://cdn2.keeng.net/playnow/images/20181002/hdupload8.jpg"/><br/>
                            &#9632;    Click chọn file, tìm file trong máy và chọn<br/>
                            &#9632;    Nhập tiêu đề cho video<br/>
                            &#9632;    Nhập mô tả cho video<br/>
                            &#9632;    Lựa chọn 1 trong các chuyên mục: Phim, Hài, Sao, Sống, Đẹp, Dân chơi, Nhạc, Tips,
                            Thể thao<br/>
                            &#9632;    Ấn nút Upload<br/>
                            &#9632;    Hệ thống thông báo việc upload thành công là hoàn tất<br/>
                        </p>
                    </div>
                    </body>
                </div>
            );
    }
}

export default Hdupload;
