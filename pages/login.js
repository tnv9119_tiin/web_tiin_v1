import React, {Component} from "react";
import AuthService from "../src/services/auth/AuthService";
import Helper from "../src/utils/helpers/Helper";

import { Router } from '../src/routes';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            submitted: false,
            qr_image: null,
            error: ''
        };
    }


    handleChange =(e)=>{
        const {name, value} = e.target;
        this.setState({
            [name]: value,
            error: '',
            submitted: false
        });
    }


    handleSubmit=(e)=> {
        e.preventDefault();

        this.setState({submitted: true});
        var {username, password} = this.state;

        var user_remove = Helper.remove084(username);
        user_remove = user_remove.trim();
        var valid = Helper.validatePhoneFormat(user_remove);
        if (!valid) {
            this.setState({
                error: 'Số điện thoại hoặc mật khẩu không đúng. Quý khách vui lòng kiểm tra lại!'
            });
            return;
        }

        var valid016 = Helper.validatePhoneStartWith16(user_remove);
        if (!valid016) {
            this.setState({
                // error: 'Số điện thoại của bạn đã được chuyển sang đầu số mới 03, xin vui lòng nhập lại.'
                error: 'Số điện thoại hoặc mật khẩu không đúng. Quý khách vui lòng kiểm tra lại!'
            });
            return;
        }

        // stop here if form is invalid
        if (!(username && password )) {
            return;
        }

        AuthService.login(user_remove, password).then(
            user => {
                if (user) {
                    var to ='/';
                    Router.pushRoute(to);
                }
            },
            error => {
                this.setState({
                    error: error
                });
            }
        );

    }

    render() {

        const {username, password, submitted, error} = this.state;

        return (
            <div id="body">
                    <div className="body-login">
                        <div className="mocha-box-login mocha-box-login1  login-old">
                            <h3 className="mbl-h3">ĐĂNG NHẬP</h3>

                            {/*<form id="loginForm" action="j_spring_security_check" method="POST">*/}
                            <form onSubmit={this.handleSubmit}>
                                <div className="mbl-form">

                                    <p className="mbl-form-p">
                                        <span className="mbl-sp-left">Số điện thoại</span>
                                        <span className="mbl-sp-right">
                                        <input type="text" id="txtUserID" name="username" className="mbl-input-txt"
                                               value={username} onChange={this.handleChange} />
                                    </span>
                                    </p>
                                    <p className="mbl-form-p">
                                        <span className="mbl-sp-left">Mật khẩu</span><span className="mbl-sp-right">
                                        <input type="password" id="txtPass" name="password" className="mbl-input-txt"
                                               value={password} onChange={this.handleChange}/> <br/>
                                    </span>
                                    </p>
                                    <p className="mbl-form-p mbl-form-remember">
                                        <span className="mbl-sp-left">&nbsp;</span>
                                        <span className="mbl-sp-right"/>
                                    </p>

                                    <p className="mbl-form-p">
                                        <span className="mbl-sp-left">&nbsp;</span>
                                        <span className="mbl-sp-right">
                                        <button className="mbl-btn-login" style={{color:'white'}}> ĐĂNG NHẬP </button>
                                    </span>
                                    </p>
                                </div>
                            </form>

                            <div style={{
                                width: '96%',
                                float: 'left',
                                textAlign: 'justify',
                                paddingLeft: '30px',
                                color: 'Red',
                                paddingTop: '5px'
                            }}>
                                <label id="lbError" style={{color: 'Red'}}>
                                    {(function () {
                                        if (submitted) {
                                            if (username.trim().length > 0 && password.trim().length === 0) {
                                                return "Mật khẩu chưa được nhập.";
                                            } else if (username.trim().length === 0 || password.trim().length === 0) {
                                                return "Số điện thoại chưa được nhập.";
                                            } else if (error) {
                                                //return "Thông tin đăng nhập không hợp lệ.";
                                                return error;
                                            }
                                        }
                                    })()}
                                </label>
                            </div>
                            <div className="mbl-info">
                                <p className="mbl-info-p">
                                    Bạn chưa có mật khẩu? Soạn tin nhắn để lấy mật khẩu</p>
                                <ul className="mbl-info-ul">
                                    <li>Với thuê bao Viettel, soạn: <strong>P MK</strong> gửi <strong>5005</strong> (miễn
                                        phí)
                                    </li>
                                    <li>Với thuê bao mạng khác, soạn: <strong>P
                                        MK</strong> gửi <strong>8062</strong> (1000đ/sms)
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
        );
    }
}

export default Login;
