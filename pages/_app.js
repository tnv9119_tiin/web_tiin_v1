import App from 'next/app';
import {AppContainer} from 'react-hot-loader';
import React from 'react';
import withReduxStore from '../lib/withReduxStore';
import {Provider} from 'react-redux';
import {Router} from '../src/routes';
import {initGA, logPageView} from '../lib/gooleAnalytics';
import Helper from "../src/utils/helpers/Helper";

import '../styles/VideoItem.css'
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/GioiThieuApp.scss';
import '../styles/upload.css';
import 'react-toastify/dist/ReactToastify.css';
import "react-tabs/style/react-tabs.css";
import '../styles/plyr.css';
import '../styles/slide_3.css';

import Template from "../src/Template";
import ScrollToTop from "../src/components/Global/Scroll/ScrollToTop";
import ShowButtonToTop from "../src/components/Global/Scroll/ShowButtonToTop";
import Header from "../src/components/Global/Header";
import Footer from "../src/components/Global/Footer";

class VideoMochaApp extends App {
    static async getInitialProps(allProps) {
        const {Component, ctx} = allProps;
        let pageProps = {};

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx)
        }

        return {
            pageProps
        }
    }

    componentDidMount() {
        initGA();
        logPageView();
        Router.router.events.on('routeChangeComplete', logPageView);
    }

    checkHideHeader(location) {
        let arrHideHeader = ['/upload', '/gioithieuapp', '/faq', '/contact', '/download', '/provision', '/guide',  '/quayso19', '/thelegametet', '/theletopdiem8-3', '/game8-3', '/gametet', '/hdchuyenngu', '/hdupload', '/ipage','/thelesharevideo10k', '/theletopdiem8-3','/theleupload', '/thuthachdata'];
        return Helper.inArr(arrHideHeader, location);
    }

    checkHideFooter(location) {
        let arrHideFooter = ['/profile', '/phim', '/login', '/uploadvideo', '/gioithieuapp', '/faq', '/contact', '/download', '/provision', '/guide',  '/quayso19', '/thelegametet', '/theletopdiem8-3', '/game8-3', '/gametet', '/hdchuyenngu', '/hdupload', '/ipage','/thelesharevideo10k', '/theletopdiem8-3','/theleupload', '/thuthachdata'];
        return !!(Helper.inArr(arrHideFooter, location) || location.includes('-cn') || location.includes('-cg') || location.includes('-v'));
    }

    render() {
        const {Component, pageProps, reduxStore} = this.props;
        let location = this.props.router.asPath;
        return (

            <Provider store={reduxStore}>
                <AppContainer>
                    <Template>
                        <ScrollToTop>
                            {this.checkHideHeader(Helper.replaceUrl(location)) ? '' :
                                <Header/>
                            }

                            <Component {...pageProps} />
                            {this.checkHideFooter(Helper.replaceUrl(location)) ? '' :
                                <Footer/>
                            }
                            <ShowButtonToTop/>
                        </ScrollToTop>
                    </Template>
                </AppContainer>
            </Provider>
        )
    }
}

export default withReduxStore(VideoMochaApp)
