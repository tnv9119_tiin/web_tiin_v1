import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class TheLeTopDiem83 extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta charset="utf-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                            <title>Thể lệ game</title>

                            <link href="http://live84.keeng.net/playnow/images/static/web_static/tet/css/Style.css" rel="stylesheet" type="text/css" />
                    </head>
                    <body class="content-thele-game" style={{padding:'unset'}}>
                    <div class="header">
                        <img src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/the-le-top-diem.png" title="" alt="" />
                    </div>
                    <div class="WordSection1">
                        <p class="content1">
                            <i>
                <span>
                    Khi tham gia chơi Game “Trao yêu thương- Trúng kim cương”trên các ứng dụng dịch vụ của Viettel, bạn có cơ hội trúng rất nhiều phần quà giá trị bao gồm bộ trang sức kim cương vàng trắng 14k, bộ trang sức ngọc trai vàng trắng 14k, vé xem phim CGV và hàng triệu phần quà hấp dẫn khác.
                </span>
                            </i>
                        </p>
                        <p class="header-content">
                            <b>1. Thời gian diễn ra chương trình</b>
                        </p>
                        <p class="content2">
                            <ul>
                                <li><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Thời gian dự kiến chương trình từ ngày 06/03/2019 đến 31/03/2019.</li>
                            </ul>
                        </p>
                        <p class="header-content">
                            <b>2. Đối tượng tham gia</b>
                        </p>
                        <p class="content2">
                            <ul>
                                <li><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Tất cả khách hàng là thuê bao di động trả trước, trả sau Viettel đang hoạt động 02 chiều tại thời điểm tham gia chơi và thời điểm nhận quà.</li>
                                <li><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Chương trình không áp dụng cho thuê bao Dcom, Homephone.</li>
                            </ul>
                        </p>
                        <p class="header-content">
                            <b>3. Nội dung chương trình</b>
                        </p>
                        <p class="content2">
                            <ul>
                                <li><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Trên các ứng dụng/Wap/Web của các trang, dịch vụ của Viettel bao gồm My Viettel, Viettel Portal, Shop Viettel, Mocha, Keeng, MyClip, 5Dmax, bạn sẽ thấy banner của game hoặc khi vào menu của từng ứng dụng/wap/ web này bạn sẽ thấy mục game “Trao yêu thương- Trúng kim cương” và có thể click vào mục này để tham gia chơi.</li>
                                <li>
                    <span>
                        <b><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Các bước tham gia chơi Game như sau:</b>
                    </span>
                                    <ul class="content3">
                                        <li><span class="list-style">-</span>Bước 1: Truy cập App/Wap/web của các trang, dịch vụ sau đều có thể tham gia: My Viettel, Viettel Portal, Shop Viettel, Mocha, Keeng, MyClip, 5Dmax.</li>
                                        <li><span class="list-style">-</span>Bước 2: Đăng nhập vào game.</li>
                                        <li><span class="list-style">-</span>Bước 3: Bắt đầu chơi: Khi bạn nhấn chơi, giao diện sẽ cho phép người chơi bắt các hộp quà, các chướng ngại vật hoặc không bắt được gì.</li>
                                        <li><span class="list-style">-</span>Khi bắt được hộp quà, bạn sẽ đập hộp quà để trúng các phần quà. Thông báo quà tặng sẽ hiện ở popup trên giao diện game và SMS gửi về điện thoại.</li>
                                    </ul>
                                </li>
                                <li>
                    <span>
                        <b><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Quy định về lượt chơi:</b>
                    </span>
                                    <ul class="content3">
                                        <li><span class="list-style">-</span>Viettel tặng bạn 3 lượt chơi miễn phí sau mỗi một giờ đồng hồ, không cộng dồn vào giờ kế tiếp nếu bạn không sử dụng.</li>
                                        <li><span class="list-style">-</span>Để thêm lượt chơi, bạn có thể hoàn thành các nhiệm vụ ở mục “thêm lượt”. Mỗi nhiệm vụ tương ứng với số lượt chơi được cộng thêm được quy định rõ. Lượt chơi được thêm khi hoàn thành nhiệm vụ được sử dụng đến hết thời gian diễn ra chương trình.</li>
                                    </ul>
                                </li>
                                <li>
                                    <span><b><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Quy định về thời hạn sử dụng quà tặng:</b></span>
                                    <ul class="content3">
                                        <li><span class="list-style">-</span>Quà tặng là SMS nội mạng: Thời hạn sử dụng 7 ngày kể từ thời điểm trúng quà.</li>
                                        <li><span class="list-style">-</span>Quà tặng là phút gọi nội mạng: Thời hạn sử dụng 7 ngày kể từ thời điểm trúng quà.</li>
                                        <li><span class="list-style">-</span>Quà tặng là Data tốc độ cao: Thời hạn sử dụng 7 ngày kể từ thời điểm trúng quà.</li>
                                        <li><span class="list-style">-</span>Quà tặng là các dịch vụ Mocha, Keeng, MyClip, 5Dmax (các dịch vụ VAS): Một tuần sử dụng miễn phí dịch vụ Mocha/Keeng/MyClip/ 5Dmax.</li>
                                        <li><span class="list-style">-</span>Quà tặng là voucher vé xem phim CGV: Sử dụng đến 24h ngày 31/07/2019.</li>
                                    </ul>
                                </li>
                                <li>
                                    <span><b><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Thông báo trúng quà:</b></span>
                                    <ul class="content3">
                                        <li><span class="list-style">-</span>Bạn sẽ nhận được thông báo nhận được quà tặng trên game và tin nhắn thông báo, hướng dẫn cách thức nhận quà trong vòng 24 giờ, kể từ thời điểm bạn trúng quà khi chơi Game.</li>
                                        <li><span class="list-style">-</span>Quà tặng là SMS, phút gọi nội mạng, data, voucher vé xem phim: Viettel thông báo qua popup trên Game và qua tin nhắn SMS.</li>
                                        <li><span class="list-style">-</span>Quà tặng hiện vật bộ trang sức kim cương vàng trắng 14k, bộ trang sức ngọc trai vàng trắng 14k: Viettel thông báo qua popup trên Game và qua tin nhắn SMS. Với quà tặng chỉ vàng cho 5 người chơi đạt nhiều điểm may mắn nhất: Đến ngày 31/03/2019, Viettel sẽ tiến hành thống kê điểm may mắn của người chơi để xác định người trúng quà. Nhân viên của Viettel sẽ liên hệ với người chơi trúng quà để làm các thủ tục nhận quà. Người chơi có trách nhiệm cung cấp các thông tin, giấy tờ cá nhân (chứng minh thư, hộ khẩu), nộp thuế theo quy định của Nhà nước để nhận quà. Danh sách khách hàng nhận được quà là bộ trang sức kim cương vàng trắng 14k, bộ trang sức ngọc trai vàng trắng 14k, chỉ vàng sẽ được công bố trên website vietteltelecom.vn.</li>
                                    </ul>
                                </li>
                                <li>
                                    <span><b><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Thời gian địa điểm, cách thức và thủ tục trao quà</b></span>
                                    <ul class="content3">
                                        <li><span class="list-style">-</span>Quà tặng là các dịch vụ VAS, tin nhắn nội mạng, phút gọi nội mạng, data: Hệ thống sẽ tự động cộng vào tài khoản cho thuê bao trúng quà.</li>
                                        <li><span class="list-style">-</span>Quà tặng là voucher vé xem phim: Viettel gửi mã voucher qua SMS đến số thuê bao trúng quà.</li>
                                        <li><span class="list-style">-</span>Quà tặng là bộ trang sức kim cương vàng trắng 14k, bộ trang sức ngọc trai vàng trắng 14k, chỉ vàng: Sẽ được trao cho người chơi trúng quà trong vòng 45 ngày kể từ khi kết thúc chương trình.</li>
                                        <li>
                                            <span>Tổng công ty Viễn thông Viettel liên hệ với người chơi nhận được quà, hướng dẫn khách hàng các thủ tục nhận quà và tổ chức trao quà.</span>
                                            <ul class="content4">
                                                <li><span class="list-style">+</span>Thủ tục cần có để nhận quà: Người nhận được quà là hiện vật phải là chính chủ thuê bao: Khi nhận quà chủ thuê bao phải xuất trình CMND/Hộ chiếu còn hiệu lực theo quy định (CMND không quá 15 năm kể từ ngày được cấp, hộ chiếu phải còn hiệu lực trong vòng 06 tháng tính đến thời điểm nhận quà hoặc các giấy tờ khác thay thế nhưng phải có ảnh và có xác nhận của chính quyền địa phương trong vòng 06 tháng tính đến thời điểm nhận quà). Nếu ủy quyền cho người khác nhận quà thì người đi nhận phải có CMND bản photo và bản gốc của chính chủ trúng quà, CMND bản photo và bản gốc của người được ủy quyền, giấy ủy quyền đi nhận giải quà của người trúng quà. Trường hợp người trúng quà đặc biệt hiện vật không phải là chính chủ thuê bao: Khi nhận, người nhận được quà phải xuất trình CMND, cung cấp ít nhất 05 số điện thoại thường xuyên liên lạc trong vòng 06 tháng gần nhất.</li>
                                                <li><span class="list-style">+</span>Quà tặng sẽ được trao tại địa chỉ của khách hàng hoặc tại các cửa hàng Viettel tỉnh/TP.</li>
                                                <li><span class="list-style">+</span>Trong trường hợp quá 30 ngày kể từ ngày kết thúc chương trình mà Tổng Công ty Viễn thông Viettel không thể liên lạc được với khách hàng hoặc khách hàng không liên hệ với Tổng Công ty Viễn thông Viettel để làm thủ tục nhận quà thì quà tặng sẽ không được trao.</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <span><b><span class="list-style"   style={{fontFamily:'Wingdings'}}>v<span style={{font:"7.0pt Times New Roman"}}></span></span> Các quy định khác:</b></span>
                                    <ul class="content3">
                                        <li><span class="list-style">-</span>Khách hàng chơi game “Trao yêu thương- Trúng kim cương” vẫn được hưởng quyền lợi từ các chương trình khuyến mại khác của Tổng Công ty Viễn thông Viettel (nếu đủ điều kiện).</li>
                                        <li><span class="list-style">-</span>Tổng Công ty Viễn thông Viettel hoàn toàn chịu trách nhiệm trong việc quản lý tính chính xác của bằng chứng xác định trúng quà và đưa bằng chứng xác định trúng quà vào lưu thông trong chương trình khuyến mại. Trường hợp bằng chứng xác định trúng quà của Tổng Công ty phát hành có sai sót gây hiểu nhầm cho khách hàng trong việc trúng quà, Tổng Công ty Viễn thông Viettel có trách nhiệm trao các quà này cho khách hàng trúng quà. Việc tổ chức chương trình khuyến mại phải đảm bảo tính khách quan và công khai.</li>
                                        <li><span class="list-style">-</span>Tổng Công ty Viễn thông Viettel được sử dụng tên và hình ảnh của khách hàng trúng quà cho mục đích quảng cáo thương mại.</li>
                                        <li><span class="list-style">-</span>Trong trường hợp xảy ra tranh chấp liên quan đến chương trình khuyến mại này, Tổng Công ty Viễn thông Viettel có trách nhiệm trực tiếp giải quyết, nếu không thỏa thuận được tranh chấp sẽ được xử lý theo quy định của pháp luật.</li>
                                        <li><span class="list-style">-</span>Nếu người chơi có dấu hiệu gian lận hoặc quấy rối, Ban Tổ chức có thể đưa vào danh sách đen không được tiếp tục chơi game “Trao yêu thương- Trúng kim cương” trên các ứng dụng dịch vụ của Viettel.</li>
                                        <li><span class="list-style">-</span>Quyết định của Ban Tổ chức (Viettel) là quyết định cuối cùng. Chi tiết liên hệ 198 (0đ).</li>
                                    </ul>
                                </li>
                            </ul>
                        </p>
                    </div>
                    </body>
                </div>
            );
    }
}

export default TheLeTopDiem83;
