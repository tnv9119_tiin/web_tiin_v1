import React, {Component} from "react";
//import '../../../styles/GioiThieuApp.scss';
//import {Link} from "../src/routes";
import Link from 'next/link';
import $ from 'jquery';

import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
const logo_mocha = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_mocha.png";
const logo_viettel = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_viettel.png";
const logo_congthuong = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_congthuong.png";


export class Faq extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        $(document).ready(function () {
            $('a.faq-click').click(function () {
                $(this).parent().find('.faq-content').toggleClass('hide_faq');
                if ($(this).hasClass('faq_show')) {
                    $(this).removeClass('faq_show');
                } else {
                    $(this).addClass('faq_show');
                }
            });

        });
    }

    render() {
        return (
            <div id="wrapper">
                <style dangerouslySetInnerHTML={{__html: "\n  #footer { margin-top: 0;}"}}/>
                <div className="header">
                    <div className="header-top">
                        <Link href="/" >
                            <a style={{float: 'left', marginTop: '5px'}}>
                                <img height="35" src={logo_mocha}/>
                            </a>

                        </Link>
                        <Link  href="/contact" as="/contact.html">
                            <a className="top-menu">LIÊN HỆ</a>
                        </Link>
                        <Link  href="/faq" as="/faq.html" >
                            <a className="top-menu active">HỎI & ĐÁP</a>
                        </Link>
                        <Link  href="/gioithieuapp" as="/gioithieuapp.html" >
                            <a className="top-menu">TÍNH NĂNG</a>
                        </Link>
                    </div>
                </div>
                <div style={{background: '#e0d2f9 none repeat scroll 0 0', paddingBottom: '20px'}}>
                    {/*<div className="mocha-guide mocha-faq" style={{paddingTop:'7px'}}>*/}

                    <p className="notice-mocha"
                       style={{
                           width: '100%',
                           color: '#FFF',
                           textAlign: 'center',
                           zIndex: 9999,
                           fontSize: '14px',
                           fontFamily: 'arial',
                           padding: '7px 0px',
                           top: '45px'
                       }}>
                        <a style={{color: 'rgb(254, 0, 0)'}}>THÔNG BÁO: Hiện nay,
                            khách hàng dùng các gói cước Sinh viên của Viettel được ưu đãi MIỄN PHÍ GỌI NỘI MẠNG khi
                            dùng Mocha. Bấm để xem thêm! </a></p>
                    {/*<div className="mocha-guide mocha-faq" style={{paddingTop:'7px'}}>*/}
                    {/*<p className="notice-mocha"*/}
                    {/*style="width: 100%; color: rgb(255, 255, 255); text-align: center; z-index: 9999; font-size: 14px; font-family: arial; padding: 7px 0px; top: 45px;">*/}
                    {/*<a href="http://mocha.com.vn/callout.html">THÔNG BÁO: Hiện nay, khách hàng dùng các gói cước*/}
                    {/*Sinh viên của Viettel được ưu đãi MIỄN PHÍ GỌI NỘI MẠNG khi dùng Mocha. Bấm để xem*/}
                    {/*thêm! </a></p>*/}

                    <div className="box-faq">
                        <p className="mocha-guide-p">Hỏi và đáp</p>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p"> 1, Mocha là gì?</p>
                            </a>
                            <div className="faq-content hide_faq">
                                Mocha là ứng dụng nhắn tin, trò chuyện miễn phí thuần Việt đa phương tiện trên điện
                                thoại di động, được phát triển bởi Tập đoàn Viễn thông Quân đội Viettel. Với Mocha,
                                bạn có thể nhắn tin đến tất cả các thuê bao Viettel, kể cả thuê bao đó không cài
                                Mocha. Giúp bạn kết nối bạn bè, người thân ở mọi lúc mọi nơi.
                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">2, Đăng ký tài khoản và tạo mật khẩu Mocha bằng cách nào?</p>
                            </a>
                            <div className="faq-content hide_faq">
                                Người dùng sử dụng chính số điện thoại cá nhân của mình để đăng ký và kích hoạt tài
                                khoản Mocha theo các bước:<br/>
                                - Nhập số điện thoại để lấy mã kích hoạt từ tổng đài Mocha (từ SMS)<br/>
                                - Dùng mã kích hoạt để kích hoạt tài khoản Mocha <br/>

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">3, Đăng nhập Mocha bằng cách nào?</p>
                            </a>
                            <div className="faq-content hide_faq">
                                Bạn có thể dùng số điện thoại cá nhân để đăng nhập Mocha.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">4, Tôi muốn thay đổi mật khẩu đăng nhập trên Mocha thì phải làm
                                    thế nào?</p>
                            </a>
                            <div className="faq-content hide_faq">
                                Với Mocha, bạn không cần mật khẩu do hệ thống có khả năng tự động nhận diện số điện
                                thoại để đăng nhập. Đảm bảo tính tiện dụng và bảo mật cho khách hàng.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">5, Cho hỏi Mocha có thu phí người dùng?</p>
                            </a>
                            <div className="faq-content hide_faq">Trả lời: Mocha là ứng dụng nhắn tin hoàn toàn miễn
                                phí giữa những người dùng đã cài ứng dụng.<br/>
                                Về việc mất phí khi chuyển tiền qua Mocha, điều này là do tính năng chuyển tiền hiện
                                đang được tích hợp từ một hệ thống khác và việc thu phí là quy định của hệ thống
                                cung cấp dịch vụ chuyển tiền. <br/>
                                Riêng với gói cước Mocha, người dùng có thể nhắn tin từ Mocha đến tất cả thuê bao
                                Viettel, kể cả thuê bao chưa cài ứng dụng và được miễn phí data khi chat và nghe
                                nhạc trong Mocha. Cước phí: 2.500đ/tuần đầu tiên, sau 7 ngày 5.000đ/tuần.<br/>

                            </div>
                        </div>

                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">6, Mocha có chức năng gì nổi bật?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">
                                Người dùng Mocha được hưởng nhiều ưu đãi cả về tính năng lẫn khuyến mãi khi sử dụng
                                dịch vụ:<br/>
                                - Tính năng Mocha SMS: Người dùng Mocha sẽ được gửi miễn phí tin nhắn cho tất cả
                                thuê bao Viettel, không giới hạn số lượng tin nhắn sau khi đăng ký gói cước Mocha
                                Free (2.500đ/tuần đầu tiên, sau 7 ngày 5.000đ/tuần).<br/>
                                - Tính năng cùng nghe cho phép người dùng Mocha chia sẻ và cùng nghe một bản nhạc
                                với bạn chat của mình.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">7, Mocha có thể cài đặt trên các hệ điều hành nào của điện
                                    thoại?</p>
                            </a>
                            <div className="faq-content hide_faq">
                                Hiện tại, bạn có thể tải Mocha trên Google Play (Android), Apple Store (iOS),
                                Windows Store (Windows Phone).

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">8, Mocha có cho thấy số điện thoại của người dùng không?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">
                                Mocha hiển thị số điện thoại của người dùng trong danh bạ, giúp bạn dễ dàng kiểm tra
                                thông tin bạn bè đang chat với mình.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">9, Danh bạ trên điện thoại có đồng bộ với Mocha?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">
                                Mocha sẽ đồng bộ danh bạ của bạn với danh sách bạn bè đang sử dụng Mocha giúp bạn dễ
                                dàng, thuận tiện hơn khi sử dụng.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">10, Làm cách nào để tìm bạn mới trên Mocha?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">
                                Bạn hãy vào tab Kết Nối để nghe nhạc và kết bạn với những người có cùng sở thích âm
                                nhạc nhé.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">11, Làm sao để theo dõi và cập nhật các phiên bản mới từ Mocha?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">Người dùng có thể theo dõi thông tin về các phiên
                                bản mới của Mocha trên website http://mocha.com.vn hoặc cập nhật trực tiếp tại App
                                Store, Google Play Store, Windows Store.
                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">12, Làm sao để chặn người lạ xem thông tin cá nhân và các hoạt
                                    động khác của mình trên Mocha?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">
                                Thông tin cá nhân của bạn hoàn toàn được bảo mật trên Mocha, chỉ có bạn bè trong
                                danh bạ của bạn mới có thể thấy thông tin này.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">13, Tại sao tôi không thể cập nhật phiên bản mới trên kho ứng
                                    dụng?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">
                                Để cập nhật Mocha phiên bản mới trên các dòng máy, người dùng cần có tài khoản cá
                                nhân: tài khoản Apple Store (iOS), tài khoản Google Play (Android), tài khoản
                                Windows Store (Windows Phone).

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">14, Mocha hỗ trợ cho các nhà mạng VN nào?
                                </p>
                            </a>
                            <div className="faq-content hide_faq">Mocha hiện tại đang hỗ trợ cho các nhà mạng:
                                Mobifone, Viettel, Vinaphone, G-mobile, Vietnammobile.

                            </div>
                        </div>
                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">15, Máy của tôi tại sao lại mất kết nối với Mocha?</p>
                            </a>
                            <div className="faq-content hide_faq"> Người dùng có thể kiểm tra lại tình trạng kết nối
                                theo cách sau:<br/>
                                1. Kiểm tra lại dung lượng bộ nhớ của máy<br/>
                                2. Kiểm tra lại dung lượng và dịch vụ gói cước 3G<br/>
                                3. Di chuyển đến vùng phủ sóng tốt hơn và thử lại<br/>
                                Trường hợp tình trạng mạng bình thường nhưng vẫn không kết nối được, người dùng
                                có thể liên hệ trực tiếp đến bộ phận CSKH của Mocha để được hỗ trợ tốt nhất<br/>
                                Gửi mail đến địa chỉ: <a
                                    href="mailto:mocha@viettel.com.vn">mocha@viettel.com.vn</a> hoặc LH 18008098
                                (Miễn phí) để được hỗ trợ chi tiết<br/>
                            </div>
                        </div>

                        <div className="box-faq-row">
                            <a className="faq-click faq_show">
                                <span className="faq-sp"></span>
                                <p className="faq-p">16, Để xác nhận mọi thông tin và được hỗ trợ từ Mocha, tôi phải
                                    liên hệ ai? Bằng cách nào?
                                </p>
                            </a>
                            <div className="faq-content hide_faq"> Người dùng có thể liên hệ bộ phận chăm sóc khách
                                hàng của Mocha qua:<br/>
                                Email:<a href="mailto:mocha@viettel.com.vn">mocha@viettel.com.vn</a> <br/>
                                Tổng đài CSKH Viettel: 18008098 (Miễn phí)

                            </div>
                        </div>


                    </div>
                    {/*</div>*/}
                </div>
                <div id="footer">
                    <div className="mocha-footer">
                        <a className="logo-mocha" href="http://mocha.com.vn/"></a>
                        <p className="provision-intro">
                            Cơ quan chủ quản: Viettel Telecom - Chăm sóc khách hàng 18008098 (Miễn phí)
                            <br/>
                            <Link  href="/provision" as="/provision.html">
                                <a className="provision">Điều khoản sử dụng</a>
                            </Link>
                            |
                            <Link  href="/guide" as="/guide.html">
                                <a className="provision">Hướng dẫn sử dụng</a>
                            </Link>
                        </p>
                        <a href="#" className="logo-viettel">
                            <img src={logo_viettel}/>
                        </a>
                        <a href="http://online.gov.vn/HomePage.aspx" className="logo-viettel logo-congthuong">
                            <img src={logo_congthuong}/>
                        </a>
                    </div>

                </div>

            </div>

        );
    }

}

export default Faq;
