
import React, {Component} from 'react';
import "../styles/thuthachdata.css";

class ThuThachData extends Component {
    constructor(props) {
    super(props);
}
    render() {
    return (
        <div>
            <html lang="en" xmlns="http://www.w3.org/1999/xhtml"/>
            <head>
                <meta charset="utf-8" />
                <title>Thử thách data - Kiếm quà cực đã</title>
            </head>
            <div class="calibre" id="calibre_link-0">
                <p class="block_">THỬ THÁCH DATA - KIẾM QUÀ CỰC ĐÃ</p>
                <p class="block_1">Chào mừng Quý khách tham gia chương trình “<span class="bold italic">Thử thách data - Kiếm quà cực đã</span>” của Viettel để có cơ hội nhận ngay những phần thưởng giá trị như iPhone 11 Pro Max và cước viễn thông với tổng giá trị lên tới hàng tỉ đồng.</p>
                <p class="block_2">&nbsp;</p>
                <p class="block_1">Các bước tham gia chương trình: </p>
                <p class="block_1"><span lang="pt">Chọn “</span><i lang="pt" class="calibre1">Tham gia ngay</i><span lang="pt">” </span><span class="bold">&#8680;</span><span lang="pt" class="text_"> Chọn thuê bao muốn mời gói </span><span class="bold">&#8680;</span><span lang="pt"></span><span lang="pt" class="text_"> Chọn gói data để mời  Xác nhận. </span><span lang="pt" class="text_1"> </span></p>
                <div class="calibre2">
                    <div class="block_3"><span class="bullet_">-&nbsp;</span><span class="calibre3"><span lang="pt"><span class="calibre4">Quý khách nhận ngay 25% giá trị gói data cộng vào tài khoản gốc (với thuê bao trả trước) hoặc giảm trừ cước (với thuê bao trả sau) khi thuê bao được mời thực hiện đăng kí gói theo lời mời của Quý khách.  </span></span>
                </span>
                    </div>
                    <div class="block_3"><span class="bullet_">-&nbsp;</span><span class="calibre3"><span lang="pt"><span class="calibre4">15 thuê bao có số lượt mời thành công nhiều nhất trong tuần sẽ nhận ngay thẻ cào trị giá 500,000đ.  </span></span>
                </span>
                    </div>
                    <div class="block_3"><span class="bullet_">-&nbsp;</span><span class="calibre3"><span lang="pt">Khi thuê bao được mời đăng kí gói theo lời mời, thuê bao mời và thuê bao được mời đều sẽ nhận được mã số may mắn để tham gia chương trình quay số trúng 15 chiếc iPhone 11 Pro Max 512GB dự kiến quay vào các ngày 07/11/2019, 07/12/2019 và 07/01/2020 (Mỗi đợt quay số có giải thưởng là 5 chiếc iPhone). Viettel sẽ tiến hành liên hệ trao thưởng đối với các thuê bao may mắn trúng giải. </span></span>
                    </div>
                </div>
                <p class="block_2">&nbsp;</p>
                <p class="block_4" lang="pt">Chi tiết về thể lệ chương trình vui lòng xem tại <a href="https://viettel.vn/thuthachdata">https://viettel.vn/thuthachdata</a>. </p>
                <p class="block_5">&nbsp;</p>
                <p class="block_6">Lưu ý:</p>
                <div class="calibre2">
                    <div class="block_3"><span class="bullet_">-&nbsp;</span><span class="calibre3"><i lang="pt" class="calibre1">Mỗi thuê bao có 100 lượt mời/ngày (áp dụng theo ngày, không cộng dồn vào ngày kế tiếp nếu không sử dụng hết).</i></span></div>
                    <div class="block_3"><span class="bullet_">-&nbsp;</span><span class="calibre3"><i lang="pt" class="calibre1">Thuê bao lan tỏa sẽ không được phép tiếp tục tham gia chương trình đến hết tháng nếu có 15 lượt lan tỏa không thành công tính từ ngày đầu tiên của tháng đó.</i></span></div>
                </div>
                <p class="block_7">&nbsp;</p>

            </div>
        </div>
    );
}
}

export default ThuThachData;
