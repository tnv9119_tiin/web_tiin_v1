import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class TheLeShare10k extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
                        <meta name="keywords" content="Thể lệ, chia sẻ, thẻ cào" />
                        <meta property="og:site_name" content="THỂ LỆ CHƯƠNG TRÌNH CHIA SẺ VIDEO NHẬN THẺ CÀO 10K" />
                        <meta name="revisit" content="1 days" />
                        <meta name="language" content="vi-VN" />
                        <meta name="language" content="vi-VN" />
                        <meta name="robots" content="index,follow,noodp" />
                        <meta name="Googlebot" content="index,follow" />
                        <title>THỂ LỆ CHƯƠNG TRÌNH CHIA SẺ VIDEO NHẬN THẺ CÀO 10K</title>
                    </head>

                    <body>
                    {/*<style>*/}
                    {/*    .box-rule*/}
                    {/*    {*/}
                    {/*        max-width: 1000px;*/}
                    {/*        margin: auto;*/}
                    {/*    }*/}
                    {/*    .box-rule h3*/}
                    {/*    {*/}
                    {/*        font-size: 15px;*/}
                    {/*        padding: 5px;*/}
                    {/*        line-height: 25px;*/}
                    {/*    }*/}
                    {/*    .box-chuyenso p*/}
                    {/*    {*/}
                    {/*        padding: 5px 15px 5px;*/}
                    {/*        line-height: 20px;*/}
                    {/*        font-size: 14px;*/}
                    {/*        color: #333333;*/}
                    {/*    }*/}
                    {/*    .p_bold*/}
                    {/*    {*/}
                    {/*        font-weight: bold;*/}
                    {/*    }*/}
                    {/*    .p_tab*/}
                    {/*    {*/}
                    {/*        padding-left: 15px;*/}
                    {/*    }*/}
                    {/*</style>*/}
                    <div class="box-rule">
                        <h3>
                            THỂ LỆ CHƯƠNG TRÌNH CHIA SẺ VIDEO NHẬN THẺ CÀO 10K</h3>
                        <p class="p_bold">
                            1. Thời gian triển khai:</p>
                        <p class="p_tab">Từ ngày 26/09/2018 đến ngày 26/10/2018</p>
                        <p class="p_bold">
                            2. Đối tượng tham gia:</p>
                        <p class="p_tab">Tất cả người dùng Việt Nam</p>
                        <p class="p_bold">
                            3. Nội dung chương trình:</p>
                        <p class="p_tab">
                            -	Người dùng tham gia xem tập phim mới nhất trong bộ phim “Có nắng có gió, ấy là ngày tươi đẹp” hoặc “Chị đứng đấy chờ em đến tán”</p>
                        <p class="p_tab">
                            - Hệ thống tự động gửi tin nhắn về mời người dùng tham gia chương trình</p>
                        <p class="p_tab">
                            - Người dùng tiến hành chia sẻ tập phim đó lên Facebook theo hướng dẫn trong tin nhắn để nhận 10k Spoint</p>
                        <p class="p_bold">
                            4. Tiêu chí chấm giải:</p>
                        <p class="p_tab">
                            -	Người chơi có số lượt chia sẻ thành công là số chia hết cho 10 (ví dụ: 10, 20, 30,…, 10000) là người trúng thưởng</p>
                        <p class="p_tab">
                            -	Số lượt chia sẻ cao nhất có thể trúng giải là 10.000</p>
                        <p class="p_tab">
                            - Mỗi người dùng chỉ có thể nhận thưởng tối đa 1 lần/1 tập phim</p>
                        <p class="p_bold">
                            5. Cơ cấu giải thưởng</p>
                        <p class="p_tab">
                            - Mỗi người chơi trúng giải được nhận 10.000 Spoint/lượt chơi (tương đương 10.000đ)</p>
                        <p class="p_tab">
                            - Thời gian và cách thức nhận giải thưởng, Mocha sẽ cung cấp cụ thể cho người dùng
                            qua tin nhắn trong ứng dụng Mocha</p>
                        <p class="p_bold">
                            6. Quy định chung</p>
                        <p class="p_tab">
                            - Quy định về sử dụng phần thưởng Spoint của chương trình này tuân theo thể lệ chung về điểm tích lũy Spoint Mocha</p>
                        <p class="p_tab">
                            - BTC được quyền sử dụng hình ảnh người chơi trúng giải để quảng cáo, truyền thông</p>
                        <p class="p_tab">
                            - Trường hợp hậu kiểm và phát hiện người chơi không tuân thủ quy định của chương
                            trình thì BTC có quyền thu hồi giải thưởng ngay cả khi đã công bố trúng thưởng</p>
                        <p class="p_tab">
                            - Nếu có tranh chấp xảy ra thì quyết định của Ban tổ chức (Mocha) là quyết định
                            cuối cùng.</p>
                    </div>
                    </body>
                </div>
            );
    }
}

export default TheLeShare10k;
