import React, {Component} from 'react';
import EditChannel from '../src/components/Channel/ManagementChannel/EditChannel';
import { Router } from '../src/routes';
import Helper from "../src/utils/helpers/Helper";
import HelperServer from "../src/utils/helpers/HelperServer";

class Createchannel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            canCreate:false
        }
        var userInfo = Helper.getUserInfo();
        var channelInfo = null;
        if (userInfo) {
            channelInfo = userInfo.channelInfo ? userInfo.channelInfo : null;
            if (channelInfo) {
                Router.replaceRoute(Helper.replaceUrl(channelInfo.link));
            } else {
                this.state = {
                    canCreate: true
                };
            }
        }
    }

    static async getInitialProps({ req,res  }) {
        if(req)
        {
            const  cookie  = req.headers.cookie;
            if (!HelperServer.checkUserLoggedInServer(cookie)) {
                res.redirect('/login')
            }
        }
    }

    render() {
        const {canCreate} = this.state;
        if (canCreate) {
            return (
                <div className="box-create-channel">
                    <EditChannel type="create"/>
                </div>
            );
        } else {
            return null;
        }
    }
}

export default Createchannel;
