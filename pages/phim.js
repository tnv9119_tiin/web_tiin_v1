import React, {Component} from 'react';
import Helper from "../src/utils/helpers/Helper";
import CateApi from "../src/services/api/Category/CategoryApi";
//import '../styles/VideoMocha/VideoItem.css';
import ChannelApi from "../src/services/api/Channel/ChannelApi";
import CateInfo from "../src/components/Category/Partial/CateInfo";
import VideoBoxAll from "../src/components/Video/partials/VideoBoxAll";
import CatePhimHot from "../src/components/Category/Phim/CatePhimHot";
import CateChannelHot from "../src/components/Category/Phim/CateChannelHot";
import ListPhim from "../src/components/Category/Phim/ListPhim";
import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";
import CircularJSON from "circular-json";
import Error from "../src/components/Error";
import NoSSR from 'react-no-ssr';
import Metaseo from "./metaseo";

class Phim extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            isFirstOnTop: false,

            videoFilm: [],
            filmHot: [],
            isShowAll: false,
            cateInfo: [],
            videoTrailerPhim: [],
            videoMotPhim: [],
            lastIdStr: ''
        };

    }

    static async getInitialProps({req, asPath}) {
        var props = {
            error: false,
            isServer: false,
            isLoaded: false,

            videoFilm: [],
            filmHot: [],
            cateInfo: [],
            videoTrailerPhim: [],
            videoMotPhim: [],
            pathName: asPath

        }

        if (req) {
            const cookie = req.headers.cookie;
            props.isServer = true;
            var lastIdStr = '';

            await Promise.all([
                CateApi.getFilmHotServer(cookie),
                CateApi.getVideoAllFilmServer(10, cookie),
                ChannelApi.getVideoChannelServer(1476, 0, lastIdStr, cookie),
                ChannelApi.getVideoChannelServer(1478, 0, lastIdStr, cookie)
            ]).then((responses) => {
                if (responses) {
                    props.isLoaded = true;
                    let resFilmHot = JSON.parse(CircularJSON.stringify(responses))[0];
                    let resVideoAllFilm = JSON.parse(CircularJSON.stringify(responses))[1];
                    let resTrailerPhim = JSON.parse(CircularJSON.stringify(responses))[2];
                    let resMotPhim = JSON.parse(CircularJSON.stringify(responses))[3];
                    props.filmHot = Helper.checkObjExist(resFilmHot, 'data.data.listVideo') ? resFilmHot.data.data.listVideo : [];
                    props.videoFilm = Helper.checkObjExist(resVideoAllFilm, 'data.data.listVideo') ? resVideoAllFilm.data.data.listVideo : [];
                    props.cateInfo = Helper.checkObjExist(resVideoAllFilm.data, 'data') ? resVideoAllFilm.data.data : [];
                    props.videoTrailerPhim = Helper.checkObjExist(resTrailerPhim, 'data.data.listVideo') ? resTrailerPhim.data.data.listVideo : [];
                    props.videoMotPhim = Helper.checkObjExist(resMotPhim, 'data.data.listVideo') ? resMotPhim.data.data.listVideo : [];
                }
            })
                .catch((error) => {
                    props.error = error.response ? error.response.status : true;
                });
        }

        return {props};
    }

    showAll = () => {
        this.setState({
            isShowAll: true
        });
    }

    componentDidMount() {
        if (!this.props.props.isServer) {
            window.addEventListener('scroll', this.handleScroll);

            if (window.pageYOffset === 0 && !this.state.isFirstOnTop) {
                this.setState({
                    isFirstOnTop: true
                });
            }
            this.fetchData();
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {
        if (window.pageYOffset === 0 && !this.state.isFirstOnTop) {
            this.setState({
                isFirstOnTop: true
            });
        }
    }

    fetchData() {
        let lastIdStr = this.state.lastIdStr;
        CateApi.getFilmHot().then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    filmHot: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );

        CateApi.getVideoAllFilm(10).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    isLoaded: true,
                    videoFilm: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : [],
                    cateInfo: Helper.checkObjExist(data, 'data.cateInfo') ? data.data.cateInfo : null
                });
            }
        );
        ChannelApi.getVideoChannel(1476, 0, lastIdStr).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    videoTrailerPhim: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );
        ChannelApi.getVideoChannel(1478, 0, lastIdStr).then(
            ({data}) => {
                Helper.renewToken(data);

                this.setState({
                    videoMotPhim: Helper.checkArrNotEmpty(data, 'data.listVideo') ? data.data.listVideo : []
                });
            }
        );

    }

    render() {

        var {props} = this.props;
        var {isServer} = props;

        const isFirstOnTop = isServer ? true : this.state.isFirstOnTop;
        const isLoaded = isServer ? props.isLoaded : this.state.isLoaded;
        const videoFilm = isServer ? props.videoFilm : this.state.videoFilm;
        const filmHot = isServer ? props.filmHot : this.state.filmHot;
        const cateInfo = isServer ? props.cateInfo.cateInfo : this.state.cateInfo;
        const videoTrailerPhim = isServer ? props.videoTrailerPhim : this.state.videoTrailerPhim;
        const videoMotPhim = isServer ? props.videoMotPhim : this.state.videoMotPhim;
        const isShowAll = isServer ? false : this.state.isShowAll;

        if (this.props.error) {
            return <Error/>
        }
        if (!isFirstOnTop || !isLoaded) {
            return <LoadingScreen/>;
        } else {
            var meta = {
                title: cateInfo.categoryname,
                image: cateInfo.url_avatar,
                description: cateInfo.description,
                url: '',
                video: '',
            };
            if (isShowAll) {
                return (
                    <div>
                        <Metaseo type="phim" data={meta}/>
                        <CateInfo cateInfo={cateInfo}/>
                        <VideoBoxAll title="PHIM MỚI CẬP NHẬT" idSearch={this.props.idSearch} isFilm="1"/>
                    </div>
                );
            } else {
                return (
                    <div>
                        <Metaseo type="phim" data={meta}/>
                        <NoSSR onSSR={<LoadingScreen/>}>
                            <section>
                            </section>
                        </NoSSR>
                        <CateInfo cateInfo={cateInfo}/>
                        <div id="lstMainFilm">
                            <div className="body-phim-center">
                                <div className="body-phim-left">
                                    <CatePhimHot filmHot={filmHot}/>
                                    <CateChannelHot video={videoTrailerPhim} type={'trailer'}/>
                                    <CateChannelHot video={videoMotPhim}/>
                                </div>
                                <div className="body-phim-right">
                                    <ListPhim videoFilm={videoFilm} showAllOnclick={this.showAll}/>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }
    }
}

export default Phim;
