import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class TheLeUpload extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <title>Thể lệ upload</title>

                        <meta property="al:ios:url" content="mocha://mochavideo?ref=http%3A%2F%2Fm.video.mocha.com.vn%2Flpage.html"/>
                        <meta property="al:ios:app_store_id" content="946275483"/>
                        <meta property="al:ios:app_name" content="Mocha"/>

                        <meta property="al:android:url" content="mocha://mochavideo?ref=http%3A%2F%2Fm.video.mocha.com.vn%2Flpage.html" />
                            <meta property="al:android:package" content="com.viettel.mocha.app" />
                                <meta property="al:android:app_name" content="Mocha" />

                                    <meta property="og:title" content="Quy định upload nội dung lên Mocha Video"/>
                                    <meta property="og:type" content="website"/>

                                    <meta property="og:url" content="http://video.mocha.com.vn/lpage.html"/>
                                    <meta property="fb:app_id" content="482452638561234"/>

                                    <meta property="og:image" content="http://video.mocha.com.vn/images/clip_logo1.png" />
                                        <meta property="og:description" content="Quy định upload nội dung lên Mocha Video" />
                                            <meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" name="viewport" />

                    </head>
                    <body>
                    <div class="content">
                        <p><b>Quy định upload nội dung lên Mocha Video</b></p>
                        <p>Mocha Video là mạng xã hội video dành cho giới trẻ. Trở thành người dùng Mocha có nghĩa là bạn đã gia nhập một
                            cộng đồng người xem và cung cấp nội dung trên Mocha Video. Trên nền tảng này, bạn có thể chia sẻ, thưởng thức,
                            cung cấp nội dung và mang về những thu nhập hấp dẫn. Những đóng góp của bạn giúp tạo nên môi trường mạng xã hội
                            video thú vị, lành mạnh.</p>

                        <p>Dưới đây là các quy định về upload video lên mạng xã hội Mocha Video:</p>

                        <p><b>1. Quy định về kênh (Channel)</b></p>
                        <p>- Kênh là nơi chứa các video của bạn, được đặt tên, đăng tải video.</p>
                        <p>- Từ tháng 7/2019, một tài khoản có thể tạo 1 kênh để upload video.</p>
                        <p>- Lượt xem (view) của kênh là lượt xem của các video trong kênh đó.</p>
                        <p>- Lượt xem của user là lượt xem của kênh hoặc tổng các kênh (nếu có).</p>
                        <p>- Chứng nhận kênh: Với các kênh đạt được các mốc: 100.000, 500.000 và 1.000.000 người follow sẽ được nhận cúp
                            chứng nhận của Mocha theo các mốc tương ứng là Bạc, Vàng và Kim cương.</p>
                        <p>- Quy định xác thực kênh (Verify):</p>
                        <p>• Kênh hạng Basic: Là kênh của người dùng upload video thông thường. Video của người dùng hạng basic sẽ được lên
                            trang sau khi ban quản trị (BQT) phê duyệt. Những nội dung được đánh giá tốt sẽ được set lên top trang chủ để có
                            cơ hội tăng lượt xem.</p>
                        <p>• Kênh hạng Premium: Là những kênh đạt được số người follow từ 5.000 trở lên, có nhiều nội dung độc quyền, đặc
                            sắc và lượt xem trung bình cao. Video thuộc kênh hạng này được xuất bản lên trang (BQT hậu kiểm) và sẽ được ưu
                            tiên set ở các vị trí hot.</p>
                        <p>- Quy định tạo kênh:</p>
                        <p>• Tên kênh và ảnh đại diện của kênh (avatar) phải phù hợp với quy định về nội dung của Mocha Video. Bất cứ vi
                            phạm nào về tên kênh, ảnh đại diện hoặc nội dung sẽ bị xóa kênh theo quy định kiểm duyệt của chúng tôi.</p>
                        <p>• Sau khi hoàn thành việc tạo kênh, bạn có thể bắt đầu tải video lên kênh.</p>
                        <p><b>2. Quy định về tính tiền</b></p>
                        <p>- Đơn giá lượt xem: 8 đồng/lượt xem hợp lệ.</p>
                        <p>- Lượt xem hợp lệ được tính như sau:</p>
                        <p>• Lượt xem gồm toàn bộ lượt xem trên app và wap, web Mocha có thời gian tối thiểu 10s tính từ lúc bắt đầu video.
                            Đối với video có thời lượng dưới 10s, lượt xem được tính khi xem toàn bộ video.</p>
                        <p>• Lượt xem được tính trong vòng 90 ngày kể từ khi video được upload lên hệ thống, tính cho các video được up load
                            kể từ ngày 01/06/2019 trở về sau.</p>
                        <p>• Trường hợp 1 thuê bao 3G/4G xem nhiều lần cùng 1 nội dung trong ngày chỉ được tính là 1 lần xem. Nếu không nhận
                            diện được thuê bao thì chỉ tính 1 lượt xem cho 1 IP duy nhất trong vòng 1 giờ.</p>
                        <p><b>3. Quy định rút tiền</b></p>
                        <p>- Người dùng upload video có thể thực hiện rút tiền bằng một trong hai hình thức: Qua thẻ cào Viettel hoặc rút
                            tiền qua dịch vụ Viettel Pay.</p>
                        <p>• Với hình thức rút tiền qua thẻ cào (thực hiện với mức tiền dưới 500.000 đồng): Người dùng có thể rút vào ngày
                            25 – 30 hàng tháng.</p>
                        <p>- Với hình thức rút tiền qua Viettel Pay (Số tiền đạt trên 500.000 đồng): </p>
                        <p>• Khách hàng cài đặt và đăng ký dịch vụ Viettel Pay (trên kho ứng dụng iOS hoặc Android) để rút tiền.</p>
                        <p>• Thời gian rút tiền: Từ 25 – 30 hàng tháng. </p>

                        <p><b>4. Quy định về kiểm duyệt nội dung</b></p>
                        <p> Để đảm bảo nội dung video được upload phù hợp với chính sách và không gây tổn hại đến cộng đồng, chúng tôi yêu cầu
                            người dùng tuân thủ chặt chẽ những chính sách nội dung được đưa ra. Bên cạnh đó, chúng tôi hoàn toàn có quyền gỡ/xóa
                            bỏ những nội dung không phù hợp theo tiêu chí đánh giá và ngưng cấp quyền sử dụng cho kênh/tài khoản vi phạm trong
                            những trường hợp được đánh giá nghiêm trọng. Trong mọi trường hợp, người dùng chịu trách nhiệm về bản quyền nội dung
                            mà mình đưa lên.</p>
                        <p><b>4.1. Quy định về bản quyền</b></p>
                        <p>- User đảm bảo và chịu trách nhiệm về bản quyền video upload lên kênh.</p>
                        <p>- Ban Quản trị (BQT) sẽ không duyệt và xóa bỏ nội dung không đảm bảo bản quyền.</p>
                        <p><b>4.2. Quy định nội dung được đưa</b></p>
                        <p>- Không được sử dụng nội dung kích động nhân dân chống Nhà nước Cộng hoà xã hội chủ nghĩa Việt Nam; gây phương
                            hại đến an ninh quốc gia, trật tự an toàn xã hội; phá hoại khối đoàn kết toàn dân; </p>
                        <p>- Không được sử dụng nội dung tuyên truyền chiến tranh xâm lược, gây hận thù giữa các dân tộc; tuyên truyền, kích
                            động bạo lực, dâm ô, tệ nạn xã hội, mê tín dị đoan, phá hoại thuần phong mỹ tục của dân tộc, cụ thể:</p>
                        <p>• Nội dung liên quan đến vấn đề tôn giáo, sắc tộc, kích động thù hằn dân tộc và sắc tộc;</p>
                        <p>• Nội dung miêu tả tỷ mỉ những hành động dâm ô, chém giết rùng rợn, phi nhân tính trong các tin, bài viết, hình
                            ảnh;</p>
                        <p>• Nội dung, tranh, ảnh kích dâm, khỏa thân, hở thân thiếu thẩm mỹ, không phù hợp với thuần phong mỹ tục Việt
                            Nam;</p>
                        <p>• Nội dung truyền bá hủ tục, mê tín, dị đoan.</p>
                        <p>- Không được sử dụng nội dung tiết lộ bí mật Nhà nước, bí mật quân sự, an ninh, kinh tế, đối ngoại và những bí
                            mật khác do pháp luật quy định;</p>
                        <p>- Không được sử dụng nội dung đưa tin sai sự thật, xuyên tạc, vu khống nhằm xúc phạm danh dự của tổ chức, danh
                            dự, nhân phẩm của công dân hoặc vi phạm quyền riêng tư cá nhân;</p>
                        <p>- Không đưa các thông tin chưa được kiểm định, xác thực tác động đến số đông.</p>
                        <p>- Không được quảng cáo, tuyên truyền, mua bán hàng hoá, dịch vụ thuộc danh mục cấm theo quy định pháp luật;</p>
                        <p><b>4.3. Quy định xử lý vi phạm nội dung</b></p>
                        <p> User vi phạm chính sách nội dung sẽ bị cảnh báo (những lỗi nội dung liên quan đến chính trị, tôn giáo sẽ khóa kênh
                            lập tức).</p>
                        <p>- Vi phạm lần 1 cảnh cáo mức vàng</p>
                        <p>- Vi phạm lần 2 cảnh cáo ở mức đỏ</p>
                        <p>- Vi phạm lần 3 khóa kênh.</p>

                        <p>Chúng tôi hy vọng người dùng tích cực tạo một cộng đồng trong sạch và mang lại những điều tốt đẹp hơn cho nền
                            tảng chung.</p>
                    </div>
                    </body>
                </div>
            );
    }
}

export default TheLeUpload;
