import React, {Component} from "react";
import ComponentMore from "../src/components/ComponentMore";
import CategoryApi from "../src/services/api/Category/CategoryApi";
import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";
import Helper from "../src/utils/helpers/Helper";

class More extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            isOnTop: false,
            items: [],
            hasMoreItems: false,
            offset: 0,
        };
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
        if (window.pageYOffset === 0 && !this.state.isOnTop) {
            this.setState({
                isOnTop: true
            });
        }


        var self = this;
        let isTop = (window.pageYOffset === 0);
        self.setState({
            isLoaded: false,
            isOnTop: isTop
        }, () => {
            CategoryApi.getListHiddenCategory(20, this.state.offset).then(
                (result) => {
                    Helper.renewToken(result.data);
                    if (result.data.data) {
                        self.setState({
                            isLoaded: true,
                            items: result.data.data.listCate
                        });
                    }
                },
                (error) => {
                    self.setState({
                        error
                    });
                });
        });

    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {
        if (window.pageYOffset === 0 && !this.state.isOnTop) {
            this.setState({
                isOnTop: true
            });
        }
    }

    render() {
        const {isLoaded, isOnTop, items} = this.state;

        if (!isLoaded || !isOnTop) {
            return <LoadingScreen/>;
        } else {
            return (
                <div className='container' style={{paddingTop: '100px', minHeight: '700px'}}>
                    {this.show(items)}
                </div>
            );
        }
    }


    show(items) {
        var result = "";
        if (items.length > 0) {
            result = items.map((item, index) => {
                return <div key={index}>
                    <ComponentMore item={item.id} categoryName={item.categoryname}/>
                </div>
            });
        }
        return result;
    }
}

export default More;
