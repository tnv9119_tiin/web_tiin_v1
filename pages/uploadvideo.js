import React, {Component} from "react";
import  Link from 'next/link';
import Dropzone from 'react-dropzone';
import axios, {post} from 'axios';

//import '../styles/upload.css';
//import '../styles/VideoMocha/VideoItem.css';
import {DOMAIN_IMAGE_STATIC, MOCHA_GOOGLE_PLAY_URL} from "../src/config/Config";
import CateApi from "../src/services/api/Category/CategoryApi";
import Helper from "../src/utils/helpers/Helper";
import {ToastContainer, toast} from 'react-toastify';
//import 'react-toastify/dist/ReactToastify.css';
import UploadApi from "../src/services/api/Upload/UploadApi";
import * as Config from "../src/config/Config";
import {  Modal,  ModalBody, ModalFooter } from 'reactstrap';

import { withRouter } from 'next/router';
import HelperServer from "../src/utils/helpers/HelperServer";


const icon_upload = DOMAIN_IMAGE_STATIC + "upload/icon_upload.png";
const icon_upload_a = DOMAIN_IMAGE_STATIC + "upload/icon_upload_a.png";


class Uploadvideo extends React.Component {

    constructor(props) {
        super(props);

        let channelUrl = null;
        let user = Helper.getUserInfo();
        if (user && Helper.checkObjExist(user, 'channelInfo') && user.channelInfo.link) {
            channelUrl = Helper.replaceUrl(user.channelInfo.link);
        }

        this.state = {
            channelUrl: channelUrl,
            listCategory: [],
            linkYoutube: '',
            categoryYoutube: 0,

            nameVideo: '',
            descriptionVideo: '',
            categoryVideo: 0,


            submitted: false,
            file: null,
            pathFile: '',
            uploadPercentage: 0,

            isStepTwo: false,
            showModalSuccess: false

        };
    }

    static async getInitialProps({ req,res  }) {
        if(req)
        {
            const  cookie  = req.headers.cookie;
            if (!HelperServer.checkUserLoggedInServer(cookie)) {
                res.redirect('/login')
            }
        }
    }

    componentDidMount() {
        window.addEventListener("beforeunload", this.onUnload);
        CateApi.getCategoryUpload().then(
            (response) => {
                if(response && response.data) {
                    if (Helper.checkArrNotEmpty(response.data, 'data.listCate')) {
                        this.setState({
                            listCategory: response.data.data.listCate
                        });
                    }
                }
            }
        );
    }

    onUnload=(event)=> { // the method that will be used for both add and remove event
        console.log("Changes you made may not be saved.");
        if (this.state.uploadPercentage) {
            event.returnValue = "Changes you made may not be saved.";
        }
    }

    handleChange =(e)=> {
        const {name, value} = e.target;
        this.setState({
            [name]: value
        });
    }

    cancelUpload=()=> {
        if (this.axiosCancelSource) {
            this.axiosCancelSource.cancel('Component unmounted.');
            this.setState({
                showModalCancel: false,
                isStepTwo: false,
                file: null,
                pathFile: '',
                nameVideo: '',
                descriptionVideo: '',
                categoryVideo: 0,
                uploadPercentage: 0
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener("beforeunload", this.onUnload);

        if (this.axiosCancelSource) {
            this.axiosCancelSource.cancel('Component unmounted.');
        }
    }

    handleCancel=()=> {
        this.setState({
            showModalCancel: true
        });
    }

    handleSubmit=(e) =>{
        e.preventDefault();

        let submitted = this.state.submitted;
        if (!submitted) {
            this.setState({submitted: true});
            var {nameVideo, descriptionVideo, categoryVideo, pathFile} = this.state;

            let checkUpload = Helper.checkInputUploadByUser(nameVideo, descriptionVideo, categoryVideo);
            if (checkUpload.error) {
                this.toastMessage('error', checkUpload.message);
            } else {
                // Call Api createVideoByLinkYoutube
                UploadApi.createVideoUploadByUser(nameVideo, descriptionVideo, pathFile, categoryVideo).then(
                    ({data}) => {
                        Helper.checkTokenExpired(data);
                        if (data.code === 200) {
                            this.setState({
                                showModalSuccess: true,
                                submitted: false
                            });
                        } else if (data.code === 400) {
                            this.setState({
                                submitted: false
                            });
                            this.toastMessage('error', 'Upload video thất bại.');
                        }else {
                            this.setState({
                                submitted: false
                            });
                            this.toastMessage('error', data.desc);
                        }
                    },
                    (error) => {
                        this.setState({
                            error,
                            submitted: false
                        });
                    });
            }
        }

    }

    handleSubmitWithLinkYoutube =(e) =>{
        e.preventDefault();

        let submitted = this.state.submitted;
        if (!submitted) {
            this.setState({submitted: true});
            var {linkYoutube, categoryYoutube} = this.state;

            let checkLinkYt = Helper.checkInputUploadYoutube(linkYoutube, categoryYoutube);
            if (checkLinkYt.error) {
                this.toastMessage('error', checkLinkYt.message);
            } else {
                // Call Api createVideoByLinkYoutube
                UploadApi.createVideoByLinkYoutube(linkYoutube, categoryYoutube).then(
                    ({data}) => {
                        Helper.checkTokenExpired(data);
                        if (data.code === 200) {
                            this.setState({
                                showModalSuccess: true,
                                submitted: false
                            });
                        } else if (data.desc) {
                            this.setState({
                                submitted: false
                            });
                            this.toastMessage('error', data.desc);
                        }
                    },
                    (error) => {
                        this.setState({
                            error,
                            submitted: false
                        });
                    });
            }
        }
    }

    toastMessage(type = 'warning', message) {
        switch (type) {
            case 'warning' :
                toast.warn(message, {
                    position: "bottom-left",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
                break;

            case 'error':
                toast.error(message, {
                    position: "bottom-left",
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
                break;
        }

    }

    onDrop = (file) => {
        if (file && file.length > 0) {
            var maxSizeUp = 2147483648;
            var fileName = file[0].name;
            var fileSize = file[0].size;
            var typeAfterCheck = fileName.slice(fileName.lastIndexOf('.') + 1);
            var nameFile = fileName.slice(0, fileName.lastIndexOf('.'));
            var nameUpload = nameFile.split('.').join('');
            console.log(nameUpload);

            var fleTypeAllowed = ['mp4', 'flv', 'mov', 'm4v', 'avi', 'mkv'];

            if (!Helper.inArr(fleTypeAllowed, typeAfterCheck)) {
                this.toastMessage('warning', fileName + ' không đúng định dạng. Chọn file có định dạng (mp4, flv, mov, m4v, avi, mkv)');
                return;
            } else if (fileSize > maxSizeUp) {
                this.toastMessage('warning', 'Bạn vui lòng chọn file nhỏ hơn 2048MB');
                return;
            }

            const bodyFormData = new FormData();
            bodyFormData.append('fileName', nameUpload+'.'+typeAfterCheck);
            bodyFormData.append('uploadFile', file[0]);

            this.axiosCancelSource = axios.CancelToken.source();
            this.setState({
                isStepTwo: true
            }, () => {
                return axios.post(`${Config.MV_SERVICE}${Config.API_UPLOAD_FILE}`,
                    bodyFormData,
                    {
                        headers: {'Content-Type': 'multipart/form-data'},
                        cancelToken: this.axiosCancelSource.token,
                        onUploadProgress: function (progressEvent) {
                            let uploadPercentage = (progressEvent.total !== 0) ? parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total)) : 0;
                            this.setState({
                                uploadPercentage: uploadPercentage
                            });

                        }.bind(this)
                    }).then(
                    ({data}) => {
                        if (data.code === 200) {
                            this.setState({
                                isStepTwo: true,
                                pathFile: data.path
                            });
                        } else if (data.desc) {
                            this.setState({
                                isStepTwo: false,
                            });
                            this.toastMessage('error', 'Bạn vui lòng gọi file khác.');
                        }
                    },
                    (error) => {
                        this.setState({
                            error
                        });
                    });
            });
        }
    }

    onPreviewDrop = (files) => {
        this.setState({
            files: this.state.files.concat(files),
        });
    }

    toggle=() =>{
        this.setState({
            showModalCancel: false,
            showModalSuccess: false,
        });
    }

    render() {
        const {channelUrl, listCategory, linkYoutube, categoryYoutube, nameVideo, descriptionVideo, categoryVideo, showModalCancel, showModalSuccess, isStepTwo, uploadPercentage} = this.state;

        var options = null;
        options = listCategory.map((category, index) => {
            return <option key={index} value={category.categoryid}>{category.categoryname}</option>
        });

        return (
            <>

                <div id="body">
                    <div className="content_upload">
                        <div className="body-phim-center upload-center"
                             style={isStepTwo ? {display: 'none'} : {display: 'block'}}>
                            <div className="body-phim-left" id="youtube">

                                <form onSubmit={this.handleSubmitWithLinkYoutube.bind(this)}>

                                    <div className="bpl-info box-shadow-cm">
                                        <p className="mbl-form-p pl-hoder-cl">
                                            <span className="mbl-sp-left lb-youtube" style={{width: 'unset'}}>UPLOAD TỪ YOUTUBE</span>
                                            <span className="mbl-sp-right">
                                        <input type="text" className="mbl-input-txt upload-select" name="linkYoutube"
                                               placeholder="Nhập link Youtube" value={linkYoutube} maxLength={100}
                                               onChange={this.handleChange}/>
                                    </span>
                                        </p>
                                        <p className="mbl-form-p">
                                    <span className="mbl-sp-right">
                                        <select name="categoryYoutube"
                                                className="mbl-input-select upload-select" value={categoryYoutube}
                                                onChange={this.handleChange}>
                                            <option value={0} disabled>Chọn chuyên mục</option>
                                            {options}
                                        </select>
                                    </span>
                                        </p>
                                        <p>
                                    <span id="Content_lblNoti" className="cpd-result" style={{color: 'Black'}}>
                                        Lưu ý: Bạn vui lòng chọn video độ dài dưới 10 phút
                                    </span>
                                        </p>
                                        <p className="mbl-form-p mbl-form-btnup">
                                    <span className="mbl-sp-right">
                                        <button className="mbl-btn-login youtube"
                                                style={{background: 'rgb(204, 204, 204)'}}>
                                            UPLOAD
                                        </button>
                                    </span>
                                        </p>
                                    </div>

                                </form>

                                <div/>
                            </div>
                            <div className="body-phim-right">
                                <span className="mbl-sp-left lb-browser" style={{width: 'unset'}}>UPLOAD TỪ MÁY</span>

                                <Dropzone onDrop={this.onDrop}>
                                    {({getRootProps, getInputProps}) => (
                                        <div id="dropUp" className="box-shadow-cm" {...getRootProps()}>
                                            <input {...getInputProps()} />

                                            <img src={icon_upload}
                                                 onMouseOver={e => (e.currentTarget.src = icon_upload_a)}
                                                 onMouseOut={e => (e.currentTarget.src = icon_upload)}
                                            />
                                            <p className="text-up-p">CLICK ĐỂ CHỌN VIDEO TẢI LÊN</p>
                                            <p>Hoặc kéo thả video tại đây</p>

                                        </div>
                                    )}
                                </Dropzone>

                            </div>
                        </div>

                        <div className="body-phim-center ul-box" style={isStepTwo ? {display: 'block'} : {display: 'none'}}>
                            <div className="upload-process box-shadow-cm">
                                <div className="row-form title-video-info">
                                    <div className="title">THÔNG TIN VIDEO</div>
                                </div>
                                <div className="row-form" id="progressBarBox">
                                    <div className="progress">
                                        <div className="progress-bar" role="progressbar" id="showProgressBar" aria-valuenow={0}
                                             style={{width: uploadPercentage + '%'}}
                                             aria-valuemin={0} aria-valuemax={100}/>
                                        <div
                                            className="progress-text">{uploadPercentage === 100 ? "Hoàn thành" : uploadPercentage + "/100"}</div>
                                        <a className="close-progress" onClick={this.handleCancel} id="cancelUploadId"
                                           title="Hủy tải lên">x</a>
                                    </div>
                                    <p className="text-1" id="textOnProgress"
                                       style={uploadPercentage !== 100 ? {display: 'block'} : {display: 'none'}}>Video của bạn
                                        đang tải
                                        lên, hãy giữ trang này cho đến khi quá trình này hoàn tất.</p>
                                </div>
                                <div className="accordion-wrap pl-hoder-cl">
                                    <div className="accordion-content">

                                        <form onSubmit={this.handleSubmit.bind(this)}>
                                            <div className="row-form row-form-sub">
                                                <div className="form-group">
                                                    <input type="text" className="form-control" name="nameVideo"
                                                           value={nameVideo}
                                                           placeholder="Nhập tiêu đề video"
                                                           maxLength={255} onChange={this.handleChange}/>
                                                    <span id="showChar" className="numInput">{nameVideo.length}/255</span>
                                                </div>
                                            </div>


                                            <div className="row-form row-form-sub">
                                                <div className="form-group">
                                        <textarea className="form-control" rows={5}
                                                  name="descriptionVideo"
                                                  value={descriptionVideo}
                                                  onChange={this.handleChange}
                                                  placeholder="Nhập mô tả video"
                                                  maxLength={700}/>
                                                    <span id="showCharArea"
                                                          className="numInput">{descriptionVideo.length}/700</span>
                                                </div>
                                            </div>
                                            <div className="row-form">
                                                <div className="form-group">
                                                    <p className="mbl-form-p">
                                            <span className="mbl-sp-right">
                                                <select name="categoryVideo"
                                                        className="mbl-input-select upload-select"
                                                        value={categoryVideo}
                                                        onChange={this.handleChange}>

                                                    <option value={0} disabled>Chọn chuyên mục</option>
                                                    {options}
                                                </select>
                                            </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <p className="mbl-form-p">
                                    <span className="btn-upload-success">
                                         <span className="btn-upload-success">
                                             <button className="mbl-btn-login browserbtn"
                                                     style={{background: "rgb(94, 72, 205)", color:'white'}}>
                                                 HOÀN THÀNH
                                             </button>
                                         </span>
                                    </span>
                                            </p>
                                        </form>
                                        <div className="clearfix"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/*<div className="popup-create-chanel"*/}
                        {/*     style={showModalSuccess ? {display: 'block'} : {display: 'none'}}>*/}
                        {/*    <div className="popup-create-content">*/}
                        {/*        <p className="popup-create-p" id="messPopup">*/}
                        {/*            Upload video thành công. Chúng tôi sẽ phê duyệt và thông báo lại kết quả cho bạn qua ứng*/}
                        {/*            dụng Mocha*/}
                        {/*        </p>*/}
                        {/*        <p className="popup-create-btn">*/}
                        {/*            <a onClick={this.toggle} href={MOCHA_GOOGLE_PLAY_URL} target="_blank"*/}
                        {/*               className="pcb-continue mocahIns" id="a-installMc">*/}
                        {/*                Cài Mocha*/}
                        {/*            </a>*/}
                        {/*            {channelUrl ?*/}
                        {/*                <Link className="pcb-continue btnOk" to={channelUrl}>Ok</Link>*/}
                        {/*                :*/}
                        {/*                <a className="pcb-continue btnOk" onClick={this.toggle}>Ok</a>*/}
                        {/*            }*/}
                        {/*        </p>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        <Modal isOpen={showModalSuccess} id="ul-modal" toggle={this.toggle} style={{width: '340px', top: '15%'}}>
                            <ModalBody className="popup-create-p">Upload video thành công. Chúng tôi sẽ phê duyệt và thông báo lại kết quả cho bạn qua ứng
                                dụng Mocha.</ModalBody>
                            <ModalFooter className="popup-create-btn">
                                <a  href={MOCHA_GOOGLE_PLAY_URL} target="_blank" className="pcb-continue" style={{cursor: 'pointer'}} color="primary" onClick={this.toggle}>Cài Mocha</a>
                                {channelUrl ?
                                    <Link href="/channel" as={channelUrl +'.html'}>
                                        <a className="pcb-continue" style={{cursor: 'pointer'}} color="secondary">Ok</a>
                                    </Link>
                                    :
                                    <p className="pcb-continue" style={{cursor: 'pointer'}} color="secondary" onClick={this.toggle}>Ok</p>
                                }
                            </ModalFooter>
                        </Modal>

                        <Modal isOpen={showModalCancel} id="ul-modal" toggle={this.toggle} style={{width: '340px', top: '15%'}}>
                            <ModalBody className="popup-create-p">Xác nhận hủy tải video lên.</ModalBody>
                            <ModalFooter className="popup-create-btn">
                                <p className="pcb-continue" style={{cursor: 'pointer'}} color="primary" onClick={this.cancelUpload}>Đồng ý</p>
                                <p className="pcb-continue" style={{cursor: 'pointer'}} color="secondary" onClick={this.toggle}>Không</p>
                            </ModalFooter>
                        </Modal>

                        <div id="toast-container" className="toast-top-right">
                            <div className="toast toast-success" aria-live="polite"
                                 style={uploadPercentage === 100 ? {display: 'block'} : {display: 'none'}}>
                                <div className="toast-title"/>
                                <div className="toast-message">Đăng tải thành công</div>
                            </div>
                        </div>

                        <ToastContainer
                            position="bottom-left"
                            autoClose={5000}
                            hideProgressBar
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnVisibilityChange
                            draggable
                            pauseOnHover
                        />
                    </div>
                </div>

            </>



        );
    }
}

export default withRouter(Uploadvideo);