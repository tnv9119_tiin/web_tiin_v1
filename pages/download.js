import React, {Component} from "react";
//import '../../../styles/GioiThieuApp.scss';
import Link from "next/link";

import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
const logo_mocha = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_mocha.png";
const img1 = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/img1.png";
const img2 = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/img2.png";
const img3 = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/img3.png";
const logo_viettel = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_viettel.png";
const logo_congthuong = DOMAIN_IMAGE_STATIC + "gioi_thieu_app/logo_congthuong.png";


export class Download extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="wrapper">
                <style dangerouslySetInnerHTML={{__html: "\n #header { z-index: 9; width: 100%; background: #f7f7f7; position: static;}\n #body {min-height: 0; height: 100%; overflow: hidden; width: 100%; background: #e0d2f9 !important ; } \n .mocha-guide {padding: 15px 0px; background: none;} \n #footer { background: #f5f5f5 none repeat scroll 0 0; padding: 25px 0px 25px 0px; margin-top: 0;}"}}/>
                <div id="header">
                    <div className="header-top">
                        <Link href="/" >
                            <a style={{float: 'left', marginTop: '5px'}}>
                                <img height={35} src={logo_mocha}/>
                            </a>
                        </Link>
                        <Link  href="/contact"
                               as="/contact.html"
                        >
                            <a className="top-menu"> LIÊN HỆ</a>
                        </Link>
                        <Link  href="/faq"
                               href="/faq.html"
                        >
                            <a className="top-menu">HỎI &amp; ĐÁP</a>
                        </Link>
                        <Link href="/gioithieuapp"
                              as="/gioithieuapp.html"
                        >
                            <a className="top-menu">TÍNH NĂNG</a>
                        </Link>
                    </div>
                </div>
                <div id="body">
                    <div className="mocha-guide" style={{paddingTop: '5px'}}>
                        <p style={{
                            width: '100%',
                            color: 'rgb(254, 0, 0)',
                            zIndex: 9999,
                            fontSize: '14px',
                            fontFamily: 'arial',
                            top: '45px',
                            textAlign: 'center',
                            padding: '0px 0px 8px'
                        }}><a style={{color: 'rgb(254, 0, 0)'}} href="http://mocha.com.vn/mochafree.html">THÔNG BÁO:
                            Hiện nay, khách hàng dùng các gói cước Sinh viên của Viettel được ưu đãi MIỄN PHÍ GỌI NỘI
                            MẠNG khi dùng Mocha. Bấm để xem thêm! </a></p>
                        <div className="box-faq box-download">
                            <p className="mocha-guide-p">Tải về</p>
                            <div className="mocha-download-list">
                                <div className="mocha-download-item">
                                    <img className="mocha-download-item-img" src={img1}/>
                                    <a className="mocha-download-a1"
                                       href="https://itunes.apple.com/us/app/mocha-messenger/id946275483?mt=8">Tải
                                        ngay</a>
                                </div>
                                <div className="mocha-download-item">
                                    <img className="mocha-download-item-img" src={img2}/>
                                    <a className="mocha-download-a1" href="http://mocha.com.vn/app?ref=clientID">Tải ngay</a>
                                </div>
                                <div className="mocha-download-item">
                                    <img className="mocha-download-item-img" src={img3}/>
                                    <a className="mocha-download-a1"
                                       href="https://www.microsoft.com/en-us/store/apps/mocha/9nblggh1f5tm">Tải ngay</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <div className="mocha-footer">
                        <Link href="/">
                            <a  className="logo-mocha"></a>
                        </Link>
                        <p className="provision-intro">
                            Cơ quan chủ quản: Viettel Telecom - Chăm sóc khách hàng 18008098 (Miễn phí)
                            <br/>
                            <Link  href="/provision"
                                   as="/provision.html"
                            >
                                <a className="provision">Điều khoản sử dụng</a>
                            </Link>
                            |
                            <Link  href="/guide"
                                   as="/guide.html">
                                <a className="provision">Hướng dẫn sử dụng</a>
                            </Link>
                        </p>
                        <a href="#" className="logo-viettel">
                            <img src={logo_viettel}/>
                        </a>
                        <a href="http://online.gov.vn/HomePage.aspx" className="logo-viettel logo-congthuong">
                            <img src={logo_congthuong}/>
                        </a>
                    </div>
                </div>
            </div>

        );
    }

}

export default Download;
