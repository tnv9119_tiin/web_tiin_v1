import React from 'react';
import Helper from "../src/utils/helpers/Helper";
import ChannelApi from "../src/services/api/Channel/ChannelApi";

//import {Tabs, Tab, TabPanel, TabList, TabProvider} from 'react-web-tabs';
import ChannelInfo from '../src/components/Channel/ChannelInfo';
import ChannelTabHome from '../src/components/Channel/ChannelTabHome';
//import {ChannelTabVideo} from '../../components/Channel/ChannelTabVideo';
import ChannelTabPhim from '../src/components/Channel/ChannelTabPhim';
import ChannelTabReport from '../src/components/Channel/ChannelTabReport';
import ChannelTabInfo from '../src/components/Channel/ChannelTabInfo';
import VideoBoxAll from "../src/components/Video/partials/VideoBoxAll";
import {Tabs, Tab, TabPanel, TabList} from 'react-tabs';
//import "react-tabs/style/react-tabs.css";
import SearchInChannel from "../src/pages/Channel/SearchInChannel";
import SearchApi from "../src/services/api/Search/SearchApi";
import VideoFeatured from "../src/components/Video/partials/VideoFeatured";
import {withRouter} from 'next/router';

import LoadingScreen from "../src/components/Global/Loading/LoadingScreen";
import Error from "../src/components/Error";
import {ID_CHANNEL_NOT_SHOW} from "../src/config/Config";
import Header from "../src/components/Global/Header";
import NoSSR from 'react-no-ssr';
import Metaseo from "./metaseo";

class Channel extends React.Component {
    constructor(props) {
        super(props);

        let identify = this.props.router.asPath ? Helper.getIdByLink(this.props.router.asPath, '-cn') : '';
        this.state = {
            isError: false,
            isLoaded: false,
            isFirstOnTop: false,

            identify: identify,
            tab: "home",
            videoNew: [],
            infoChannel: null,
            channelUserFollow: [],
            videoHot: [],
            videoFilm: [],
            isTabVideo: false,
            selectedIndex: 0,
            isToggleOn: false,
            display: 'block',
            videoSearch: [],
            videoMyChannel: [],
            lastIdStr: '',
            query: '',
            queryEntered: '',
        };

        this.showAllVideo = this.showAllVideo.bind(this);
        this.handleClickShow = this.handleClickShow.bind(this);
        this.handleInputKeyPress = this.handleInputKeyPress.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

        this.handleScroll = this.handleScroll.bind(this);
        this.myRef = React.createRef();
    }

    showSearch() {
        this.setState(state => ({
            isToggleOn: true
        }));
    }

    handleClickShow() {
        this.search.focus();
        this.setState(state => ({
            isToggleOn: !state.isToggleOn
        }));
    }

    disabledSearch() {
        this.setState(state => ({
            isToggleOn: false,
            query: ''
        }));
    }

    showAllVideo() {
        window.scrollTo(0, this.myRef.current.offsetTop);
        var _self = this;
        this.setSelectedIndex = setTimeout(function () {
            _self.setState({
                selectedIndex: 1
            });
        }, 100);
    }

    handleInputKeyPress(e) {
        let valueStr = this.search.value.trim();
        if (e.key === 'Enter' && valueStr.length > 0) {
            this.setState({
                display: 'none',
                query: valueStr,
                queryEntered: valueStr
            });
            //call api search in channel
            SearchApi.searchVideoOfChannel(valueStr, this.state.identify).then(
                (response) => {
                    if (response && response.data) {
                        Helper.renewToken(response.data);
                        this.setState({
                            videoSearch: Helper.checkArrNotEmpty(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                        });
                    }
                },
                (error) => {
                    this.setState({
                        isError: true
                    });
                });
        }
    }

    handleInputChange() {
        this.setState({
            query: this.search.value
        });
    }

    componentDidMount() {

        let identify = Helper.getIdByLink(this.props.router.asPath, '-cn');
        window.addEventListener('scroll', this.handleScroll);
        if (window.pageYOffset === 0 && !this.state.isFirstOnTop) {
            this.setState({
                isFirstOnTop: true
            });
        }
        if (identify != ID_CHANNEL_NOT_SHOW) {
            this.fetchData();
        } else {
            this.setState({
                isError: true
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
        if (this.setSelectedIndex) {
            clearTimeout(this.setSelectedIndex);
            this.setSelectedIndex = null;
        }
    }

    handleScroll() {
        if (window.pageYOffset === 0 && !this.state.isFirstOnTop) {
            this.setState({
                isFirstOnTop: true
            });
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let identify = Helper.getIdByLink(this.props.router.asPath, '-cn');
        if (identify != this.state.identify) {
            this.setState({
                identify: identify,
                selectedIndex: 0,
                isFirstOnTop: false
            }, () => {
                if (window.pageYOffset === 0 && !this.state.isFirstOnTop) {
                    this.setState({
                        isFirstOnTop: true
                    });
                }
                this.fetchData();
            });
        }
    }

    fetchData() {
        let isMyChannel = false;
        let user = Helper.getUserInfo();

        if (user && Helper.checkObjExist(user, 'channelInfo') && user.channelInfo.link) {
            let channelId = Helper.getIdByLink(user.channelInfo.link, '-cn');

            if (channelId && channelId == this.state.identify) {
                isMyChannel = true;
            }
        }
        let lastIdStr = this.state.lastIdStr;
        if (isMyChannel) {
            ChannelApi.getInfoMyChannel().then(
                (response) => {
                    if (response && response.data) {
                        Helper.checkTokenExpired(response.data);
                        Helper.renewToken(response.data);
                        if (Helper.checkArrNotEmpty(response.data, 'data.listChannel')) {
                            this.setState({
                                isLoaded: true,
                                infoChannel: response.data.data.listChannel[0]
                            });
                        } else {
                            this.setState({
                                isError: true
                            });
                        }
                    }
                },
                (error) => {
                    this.setState({
                        isError: true
                    });
                }
            );
            ChannelApi.getVideoChannel(this.state.identify, 0, lastIdStr).then(
                ({data}) => {
                    Helper.renewToken(data);

                    this.setState({
                        videoNew: Helper.checkObjExist(data, 'data.listVideo') ? data.data.listVideo : []
                    });
                }
            );
            // ChannelApi.getVideoMyChannel(this.state.identify, 0, lastIdStr).then(
            //     ({data}) => {
            //         Helper.checkTokenExpired(data);
            //         Helper.renewToken(data);
            //
            //         this.setState({
            //             videoMyChannel: Helper.checkObjExist(data, 'data.listVideo') ? data.data.listVideo : []
            //         });
            //     }
            // );
        } else {
            ChannelApi.getInfoChannel(this.state.identify).then(
                (response) => {
                    if (response && response.data) {
                        Helper.checkTokenExpired(response.data);
                        Helper.renewToken(response.data);
                        if (Helper.checkObjExist(response.data, 'data.channelInfo')) {
                            this.setState({
                                isLoaded: true,
                                infoChannel: response.data.data.channelInfo
                            });
                        } else {
                            this.setState({
                                isError: true
                            });
                        }
                    }

                },
                (error) => {
                    this.setState({
                        isError: true
                    });
                }
            );

            ChannelApi.getVideoChannel(this.state.identify, 0, lastIdStr).then(
                (response) => {
                    if (response && response.data) {
                        Helper.renewToken(response.data);
                        this.setState({
                            videoNew: Helper.checkObjExist(response.data, 'data.listVideo') ? response.data.data.listVideo : []
                        });
                    }
                }
            );
        }

        ChannelApi.getVideoHot(this.state.identify).then(
            (response) => {
                if (response && response.data) {
                    Helper.renewToken(response.data);
                    this.setState({
                        videoHot: Helper.checkObjExist(response.data, 'data') ? response.data.data : []
                    });
                }
            }
        );

        /*ChannelApi.getFilmGrp(this.state.identify).then(
            (result) => {
                if (result && result.data) {
                    Helper.renewToken(result.data);
                    this.setState({
                        videoFilm: Helper.checkObjExist(result, 'data.result') ? result.data.result : []
                    });
                }

            }
        );*/
    }

    render() {
        const {isError, isLoaded, isFirstOnTop, identify, videoNew, infoChannel, videoHot, videoFilm, isTabVideo, isToggleOn, query, queryEntered, display, videoSearch, videoMyChannel} = this.state;

        if (isError) {
            return <Error/>;
        } else if (!isFirstOnTop || !isLoaded) {
            return <LoadingScreen/>;
        } else {
            var isMyChannel = infoChannel.isMyChannel;
            if (!isToggleOn) {
                var style = 'cn-search-input';
            } else {
                style = 'cn-search-input active';
            }
            let meta = {
                title: infoChannel.name,
                image: infoChannel.url_avatar,
                description: infoChannel ? infoChannel.description : '',
                url: infoChannel ? infoChannel.link : '',
                type: 'channel',
                path: Helper.replaceUrl(infoChannel.link)
            };

            return (
                <div ref={this.myRef}>
                    <Metaseo type="channel" data={meta}/>
                    <Header/>
                    <NoSSR onSSR={<LoadingScreen/>}>
                        <section>
                        </section>
                    </NoSSR>
                    <Tabs
                        selectedIndex={this.state.selectedIndex}
                        onSelect={this.handleSelect}
                    >
                        <div className="chanel-top chanel-top2 my-chanel box-shadow-cm">
                            <ChannelInfo infoChannel={infoChannel}/>
                            <div className="my-chanel-menu">
                                <TabList>
                                    <Tab>
                                        <a>TRANG CHỦ </a>
                                    </Tab>
                                    <Tab>
                                        <a>VIDEO </a>
                                    </Tab>
                                    {infoChannel.hasFilmGroup ?
                                        (
                                            <Tab>
                                                <a>DANH SÁCH </a>
                                            </Tab>
                                        ) : ''
                                    }
                                    {infoChannel.isMyChannel ?
                                        (
                                            <Tab>
                                                <a>THỐNG KÊ</a>
                                            </Tab>
                                        ) : ''
                                    }
                                    {infoChannel.isMyChannel ? '' :
                                        <Tab>
                                            <a>THÔNG TIN</a>
                                        </Tab>
                                    }
                                    <p className="my-chanel-search">
                                        <input type="button" id="tabSearch" ref={input => this.search = input}
                                               onClick={this.handleClickShow}
                                               className="cn-search-btn" style={{top: '29px', marginLeft: '-25px'}}/>
                                        <input placeholder="Từ khóa tìm kiếm..." className={style}
                                               id="search_query" type="text" name="q" autoComplete="off"
                                               autoFocus
                                               onBlur={this.disabledSearch.bind(this)}
                                               value={query}
                                               ref={input => this.search = input}
                                               onChange={this.handleInputChange}
                                               onKeyPress={(e) => this.handleInputKeyPress(e)}/>
                                    </p>
                                </TabList>
                            </div>
                        </div>
                        <TabPanel style={{display: display}}>
                            {/*<ChannelTabHome video={isMyChannel ? videoMyChannel : videoNew} infoChannel={infoChannel}
                                            videoHot={videoHot} showAllOnclick={this.showAllVideo}/>*/}
                            <ChannelTabHome video={videoNew} infoChannel={infoChannel}
                                            videoHot={videoHot} showAllOnclick={this.showAllVideo}/>
                        </TabPanel>
                        <TabPanel style={{display: display}}>
                            {this.renderTabVideo()}
                            {/*{<ChannelTabVideo video={this.state.videoNew} idSearch={this.state.identify}/>}*/}
                        </TabPanel>
                        {infoChannel.hasFilmGroup ?
                            <TabPanel style={{display: display}}>
                                <ChannelTabPhim idSearch={identify}/>
                            </TabPanel>
                            : ''
                        }
                        {infoChannel.isMyChannel ?
                            <TabPanel style={{display: display}}>
                                <ChannelTabReport infoChannel={infoChannel}
                                                  videoNew={isMyChannel ? videoMyChannel : videoNew}
                                                  idSearch={identify}/>
                            </TabPanel>
                            : ''
                        }
                        {infoChannel.isMyChannel ? '' :
                            <TabPanel style={{display: display}}>
                                <ChannelTabInfo infoChannel={infoChannel} idSearch={identify}/>
                            </TabPanel>
                        }
                    </Tabs>
                    {display === 'none' ?
                        <SearchInChannel query={queryEntered} videos={videoSearch} identify={identify}/> : ''}

                </div>
            );
        }
    }

    renderTabVideo() {
        var isMyChannel = this.state.infoChannel.isMyChannel;
        return (
            <div>
                <VideoFeatured videoHot={this.state.videoHot}/>
                <VideoBoxAll title="TẤT CẢ VIDEO" idSearch={this.state.identify} isMyChannel={isMyChannel}
                             isChannel="1"/>
            </div>
        );
    }

    handleSelect = (index) => {
        this.setState({
            selectedIndex: index,
            display: 'block',
        });
    }
}

export default withRouter(Channel);
