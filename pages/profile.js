import React, {Component} from "react";
import {Tabs, Tab, TabPanel, TabList} from "react-web-tabs";

import Helper from "../src/utils/helpers/Helper";
import AuthService from "../src/services/auth/AuthService";
import ModalPartial from "../src/components/Modal/ModalPartial";

import {DOMAIN_IMAGE_STATIC} from "../src/config/Config.js";
const ver4_27 = DOMAIN_IMAGE_STATIC + "ver4_27.png";
const ver4_26 = DOMAIN_IMAGE_STATIC + "ver4_26.png";
const ver4_28 = DOMAIN_IMAGE_STATIC + "ver4_28.png";
const ver4_29 = DOMAIN_IMAGE_STATIC + "ver4_29.png";
const ver4_30 = DOMAIN_IMAGE_STATIC + "ver4_30.png";


 class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShow: false,

            pass: '',
            newPass: '',
            confirmPass: '',
            submitted: false,
            error: '',
            success: ''
        };
        this.toggle = this.toggle.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle() {
        this.setState({
            isShow: !this.state.isShow
        });
    }

    handleChange(e) {
        const {name, value} = e.target;

        this.setState({
            [name]: value,
            error: '',
            success: '',
            submitted: false
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({submitted: true});
        var {pass, newPass, confirmPass} = this.state;

        if (pass.length === 0) {
            this.setState({
                error: "Mật khẩu cũ chưa được nhập."
            });
            return;
        } else if (newPass.length === 0) {
            this.setState({
                error: "Mật khẩu mới chưa được nhập."
            });
            return;
        } else if (confirmPass.length === 0) {
            this.setState({
                error: "Nhắc lại mật khẩu chưa được nhập."
            });
            return;
        } else if (newPass.length < 8 || newPass.length > 32) {
            this.setState({
                error: "Mật khẩu mới phải có từ 8 - 32 ký tự."
            });
            return;
        } else if (newPass !== confirmPass) {
            this.setState({
                error: "Mật khẩu mới chưa trùng với mật khẩu nhập lại."
            });
            return;
        }

        var checkNum = Helper.hasNumber(newPass);
        if (!checkNum) {
            this.setState({
                error: 'Mật khẩu mới phải chứa ký tự số.'
            });
            return;
        }

        var checkChar = Helper.hasCharacter(newPass);
        if (!checkChar) {
            this.setState({
                error: 'Mật khẩu mới phải chứa ký tự chữ.'
            });
            return;
        }

        AuthService.changePassword(pass, newPass).then(
            user => {
                this.setState({
                    // success: "Thay đổi mật khẩu thành công - Mời bạn đăng nhập lại."
                    success: "Thay đổi mật khẩu thành công."
                });
            },
            error => {
                this.setState({
                    error: error
                });
            }
        );

    }


    render() {
        const {pass, newPass, confirmPass, submitted, error, success} = this.state;

        return (
            <div id="body">
                <div className="wrap-body-login" style={{marginTop:"150px"}}>

                    <Tabs>
                        <div className="persion-list">
                            <TabList>
                                <div className="menu-persion-wrap">

                                    <h3 className="menu-persion-h3 menu-persion-title">
                                        Cài đặt
                                    </h3>
                                    <h3 className="menu-persion-h3">
                                        Cài đặt tài khoản
                                    </h3>

                                    <ul>
                                        <li>
                                            <a id="a_changepass">
                                                <Tab tabFor="one">
                                                    <img src={ver4_27}/>
                                                    Đổi mật khẩu
                                                </Tab>
                                            </a>
                                        </li>
                                        <li>
                                            <a id="a_logout" style={{cursor: 'pointer'}} onClick={this.toggle}>
                                                <img src={ver4_26}/>
                                                Đăng xuất
                                            </a>
                                        </li>
                                    </ul>

                                </div>

                                <div className="menu-persion-wrap">
                                    <h3 className="menu-persion-h3">
                                        Thông tin bổ sung
                                    </h3>

                                    <ul>
                                        <li>
                                            <a id="a_policy">
                                                <Tab tabFor="three">
                                                    <img src={ver4_27}/>
                                                    Quy chế hoạt động
                                                </Tab>
                                            </a>
                                        </li>
                                        <li>
                                            <a id="a_rule">
                                                <Tab tabFor="four">
                                                    <img src={ver4_28}/>
                                                    Điều khoản sử dụng
                                                </Tab>
                                            </a>
                                        </li>
                                        <li>
                                            <a id="a_info">
                                                <Tab tabFor="five">
                                                    <img src={ver4_29}/>
                                                    Chính sách bảo mật
                                                </Tab>
                                            </a>
                                        </li>
                                        <li>
                                            <a id="a_help" className="active">
                                                <Tab tabFor="six">
                                                    <img src={ver4_30}/>
                                                    Giới thiệu dịch vụ
                                                </Tab>
                                            </a>
                                        </li>
                                    </ul>

                                </div>

                            </TabList>
                        </div>

                        <div className="body-login-right">
                            <div className="mocha-box-login contetn-profile">
                                <div className="content-service">

                                    <TabPanel tabId="one">
                                        <h3 className="mbl-h3" id="textContent" style={{display: 'block'}}>ĐỔI MẬT
                                            KHẨU</h3>

                                        <form onSubmit={this.handleSubmit}>
                                            <div className="mbl-form" id="changepass" style={{display: 'block'}}>
                                                <p className="mbl-form-p">
                                                    <span className="mbl-sp-left">Mật khẩu cũ</span> <span
                                                    className="mbl-sp-right">
                                                        <input type="password" className="mbl-input-txt"
                                                               name="pass" maxLength={32} value={pass}
                                                               onChange={this.handleChange}/>
                                                    </span>
                                                </p>
                                                <p className="mbl-form-p">
                                                    <span className="mbl-sp-left">Mật khẩu mới</span> <span
                                                    className="mbl-sp-right">
                                                        <input type="password" className="mbl-input-txt" name="newPass"
                                                               maxLength={32} aria-autocomplete="list" value={newPass}
                                                               onChange={this.handleChange}/>
                                                    </span>
                                                </p>
                                                <p className="mbl-form-p">
                                                    <span className="mbl-sp-left">Nhập lại mật khẩu</span> <span
                                                    className="mbl-sp-right">
                                                        <input type="password" className="mbl-input-txt"
                                                               name="confirmPass"
                                                               maxLength={32} value={confirmPass}
                                                               onChange={this.handleChange}/>
                                                    </span>
                                                </p>
                                                <p className="mbl-form-p mbl-form-btn">
                                                    <span className="mbl-sp-left">&nbsp;</span>
                                                    <span className="mbl-sp-right">
                                                        <button className="mbl-btn-login" style={{color:'white'}}>
                                                            ĐỔI MẬT KHẨU
                                                        </button>
                                                    </span>
                                                </p>
                                                <div style={{
                                                    width: '96%',
                                                    textAlign: 'justify',
                                                    color: 'Red',
                                                    paddingLeft: '30px',
                                                    paddingTop: '5px'
                                                }}>
                                                    <label id="lbError"/>

                                                    {(function () {
                                                        if (submitted && error) {
                                                            return error;
                                                        }
                                                    })()}

                                                </div>

                                                <div style={{
                                                    width: '96%',
                                                    textAlign: 'justify',
                                                    color: 'Blue',
                                                    paddingLeft: '30px',
                                                    paddingTop: '5px'
                                                }}>
                                                    {(function () {
                                                        if (submitted && success) {
                                                            return success;
                                                        }
                                                    })()}

                                                </div>

                                                <div className="mbl-info">
                                                    <p className="mbl-info-p">
                                                        Bạn chưa có mật khẩu? Soạn tin nhắn để lấy mật khẩu</p>
                                                    <ul className="mbl-info-ul">
                                                        <li>Với thuê bao Viettel, soạn: <strong>P
                                                            MK</strong> gửi <strong>5005</strong> (miễn
                                                            phí)
                                                        </li>
                                                        <li>Với thuê bao mạng khác, soạn: <strong>P
                                                            MK</strong> gửi <strong>8062</strong> (500đ/sms)
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </form>

                                    </TabPanel>

                                    <TabPanel tabId="three">
                                        <h3 className="mbl-h3" id="textContent">
                                            QUY CHẾ HOẠT ĐỘNG
                                        </h3>

                                        <div className="service-info" id="policy" style={{display: 'block'}}>
                                            <b>I. Nguyên tắc chung </b>
                                            <br/>
                                            - Website Mạng xã hội mocha.com.vn thuộc quyền quản lý và khai thác của Công
                                            ty Truyền thông Viettel – Chi nhánh Tập đoàn Viễn thông Quân đội (Viettel)
                                            với mục đích cho phép khách hàng tiếp cận thông tin và xem video các lĩnh
                                            vực gồm: giải trí, tin tức, phim, hài, âm nhạc, thiếu nhi… ngay trên di
                                            động, và được sử dụng dịch vụ với chất lượng tốt nhất.
                                            <br/>
                                            - Khách hàng tham gia website sẽ mất phí theo hình thức thuê bao
                                            (ngày/tuần/tháng).
                                            <br/>
                                            - Đối tượng sử dụng dịch vụ: Các khách hàng trên 16 tuổi có nhu cầu xem
                                            video, phim, nghe nhạc trên website.
                                            <br/>
                                            - Đối tác cung cấp nội dung: là các công ty truyền thông và nội dung số, các
                                            đài truyền hình… đồng ý hợp tác và ký kết hợp đồng với Viettel để cung cấp
                                            dịch vụ.
                                            <br/>
                                            - Cung cấp và sử dụng tài khoản thành viên:
                                            <br/>
                                            - Phạm vi cung cấp nội dung: Khi đối tác đồng ý cung cấp nội dung trên
                                            video.mocha.com.vn cũng đồng nghĩa với việc đối tác đồng ý cho Mocha sử dụng
                                            các nội dung đó trên các sản phẩm Siêu Hài, Onbox để thực hiện cung cấp và
                                            kinh doanh
                                            <br/>
                                            • Trong trường hợp khách hàng sử dụng dịch vụ 3G của Viettel, khi truy cập
                                            vào trang web, hệ thống có hỗ trợ tính năng nhận diện thuê bao giúp khách
                                            hàng không phải đăng nhập và đăng ký thông tin trước khi sử dụng dịch vụ.
                                            Nếu người dùng sử dụng mạng wifi hoặc hệ thống không nhận diện được thuê
                                            bao, người dùng sẽ được cấp tài khoản với tên sử dụng - user (là số điện
                                            thoại của Khách hàng) và mật khẩu để truy cập và sử dụng dịch vụ. Khi thực
                                            hiện thao tác đăng ký dịch vụ, hệ thống sẽ trả cho KH tin nhắn xác nhận đăng
                                            ký thành công.
                                            <br/>
                                            • Mỗi khách hàng khi truy cập trang video.mocha.com.vn để sử dụng dịch vụ
                                            phải truy cập bằng 3G/4G Viettel để được hưởng chính sách miễn cước data khi
                                            xem video. Trong trường hợp khách hàng không truy cập bằng 3G/4G mà phải sử
                                            dụng số điện thoại và password để đăng nhập thì khách hàng đó phải cân nhắc
                                            vì sự bảo mật số điện thoại của mình. Trong suốt quá trình đăng ký, Khách
                                            hàng đồng ý nhận sms thông báo, quảng cáo từ website hoặc từ chối nếu không
                                            muốn tiếp tục nhận sms.
                                            <br/>
                                            • Những video của các đối tác hiển thị trên website là thông tin từ đối tác
                                            video.mocha.com.vn cung cấp cho Ban quản trị.
                                            <br/>
                                            • Các video đăng tải trên website đáp ứng đầy đủ các quy định của pháp luật
                                            có liên quan, không thuộc các trường hợp cấm kinh doanh, cấm quảng cáo theo
                                            quy định của pháp luật.
                                            <br/>
                                            • Các thông tin video cung cấp trên website, phải được đăng công khai, minh
                                            bạch, chính xác nhằm đảm bảo quyền lợi của đối tác và khách hàng.
                                            <br/>
                                            • Tất cả các nội dung trong Quy định này tuân thủ theo hệ thống pháp luật
                                            hiện hành của Việt Nam. Khách hàng khi tham gia vào website phải cam kết
                                            thực hiện đúng những nội dung trong Quy chế của website.
                                            <br/>
                                            <b>II. Quy định chung </b>
                                            <br/>
                                            <b>1. Khái niệm: </b>Trong Quy chế này, các từ và các thuật ngữ sau đây sẽ
                                            có ý nghĩa sau đây:
                                            <br/>
                                            - Website: http://video.mocha.com.vn
                                            <br/>
                                            - Chủ sở hữu website: Công ty Truyền thông Viettel (Viettel Media)
                                            <br/>
                                            - Ban quản trị website: bộ phận do Chủ sở hữu website giao nhiệm vụ quản lý
                                            hoạt động website
                                            <br/>
                                            - Thành viên: Khách hàng có nhu cầu xem video, phim, nghe nhạc trên website.
                                            <br/>
                                            - Đối tác: Các công ty đồng ý cung cấp dịch vụ và video cho các khách hàng
                                            trên website.
                                            <br/>
                                            - Dịch vụ/hàng hóa: Những video, hình ảnh thuộc các lĩnh vực giải trí, tin
                                            tức, phim, hài, âm nhạc, thiếu nhi …
                                            <br/>
                                            <b>2. Quy định về dịch vụ: </b>
                                            <br/>
                                            <b>2.1. Điều kiện tham gia: </b>Khách hàng truy cập vào website
                                            http://video.mocha.com.vn và đăng ký/đăng nhập tài khoản thành công.
                                            <br/>
                                            <b>2.2. Cách thức tham gia: </b>Để xem video khách hàng cần làm các bước
                                            sau:
                                            <br/>
                                            - Bước 1: Khách hàng truy cập vào website http:// video.mocha.com.vn, sử
                                            dụng 3G/4G và đăng ký dịch vụ.
                                            <br/>
                                            - Bước 2: Sau khi đăng ký dịch vụ thành công, hệ thống sẽ gửi về 01 tin nhắn
                                            từ đầu số 5005 hoặc Mocha Video cho khách hàng (KH). Tin nhắn: Thông báo
                                            đăng ký thành công:
                                            <br/>
                                            Cam on Quy khach da dang ky dich vu MXH Mocha Video , goi cuoc … (gia cuoc).
                                            De xac nhan gia han va su dung DV, soan … gui 5005. Chi tiet soan HD … gui
                                            5005, truy cap http://video.mocha.com.vn hoac LH 198 (mien phi). Tran trong.
                                            <br/>
                                            - Bước 3: KH truy cập vào trang http://video.mocha.com.vn để sử dụng dịch
                                            vụ.
                                            <br/>
                                            <b>2.3. Các loại chi phí phát sinh: </b>
                                            <br/>
                                            - Phí duy trì dịch vụ.
                                            <br/>
                                            - Chính sách miễn phí data không áp dụng đối với một số quảng cáo từ các đơn
                                            vị bên ngoài.
                                            <br/>
                                            <b>III. Quy trình giao dịch </b>
                                            <br/>
                                            <b>1. Quy trình khách hàng đăng ký </b>
                                            <br/>
                                            - Bước 1: Truy cập website bằng 3G/4G Viettel để hệ thống nhận diện được số
                                            điện thoại của khách hàng.
                                            <br/>
                                            - Bước 2: Lựa chọn gói cước ngày/tuần/tháng theo nhu cầu và Đăng ký.
                                            <br/>
                                            - Bước 3: Truy cập trang http://video.mocha.com.vn để sử dụng dịch vụ.
                                            <br/>
                                            <b>2. Quy trình dành cho đối tác </b>
                                            <br/>
                                            - Bước 1: Ký hợp đồng hợp tác cung cấp bản quyền sản phẩm trên website.
                                            <br/>
                                            - Bước 2: Cung cấp cho đối tác account để truy cập hệ thống và chủ động
                                            upload video theo hợp đồng hợp tác.
                                            <br/>
                                            - Bước 3: Đối soát và thanh toán cho đối tác.
                                            <br/>
                                            <b>3. Hướng dẫn xử lý khiếu nại </b>
                                            <br/>
                                            - Bước 1: Khách hàng đăng ký trên website sẽ chủ động liên lạc và khiếu nại
                                            với Ban quản trị website để giải quyết các khiếu nại hoặc liên hệ qua tổng
                                            đài 198, qua hệ thống cửa hàng giao dịch của Viettel.
                                            <br/>
                                            Hoặc gửi thông tin khiếu nại đến địa chỉ:
                                            <br/>
                                            • Công ty/Tổ chức: Công ty Truyền thông Viettel.
                                            <br/>
                                            • Địa chỉ: Tầng 4, tòa nhà The Light, đường Tố Hữu, Hà Nội.
                                            <br/>
                                            - Bước 2: Ban quản trị sẽ liên lạc lại với người khiếu nại để giải
                                            quyết. Ban quản trị cam kết tiếp nhận hỗ trợ mọi trường hợp trong
                                            vòng 10 ngày làm việc.
                                            <br/>
                                            - Bước 3: Nếu vụ việc vượt quá thẩm quyền của mình, Ban quản trị
                                            sẽ đề nghị người bị vi phạm chuyển vụ việc cho các cơ quan chức
                                            năng có thẩm quyền. Trong trường hợp này, Ban quản trị vẫn phối hợp
                                            hỗ trợ để bảo vệ tốt nhất bên bị vi phạm.
                                            <br/>
                                            <b>IV. Quy trình thanh toán </b>
                                            <br/>
                                            - Bước 1: Khách hàng đăng ký dịch vụ trên wapsite.
                                            <br/>
                                            - Bước 2: Khách hàng xác nhận lại thông tin đăng ký bằng tin nhắn (SMS) hoặc
                                            trực tiếp trên website.
                                            <br/>
                                            - Bước 3: Nhà mạng trừ tiền vào tài khoản gốc điện thoại của Khách hàng tùy
                                            theo gói cước dịch vụ khách hàng đăng ký.
                                            <br/>
                                            - Bước 4: Khách hàng sử dụng dịch vụ.
                                            <br/>
                                            <b>V. Đảm bảo an toàn giao dịch </b>
                                            <br/>
                                            <b>1. An toàn giao dịch </b>
                                            <br/>
                                            <b>1.1. Quy định đối với Nhà bán hàng </b>
                                            <br/>
                                            a. Quy định về cung cấp thông tin
                                            <br/>
                                            - Để được kinh doanh trên Mạng xã hội mocha.com.vn Nhà Bán hàng phải ký Hợp
                                            đồng cung cấp dịch vụ dành cho Người Bán của video.mocha.com.vn. Người Bán
                                            hàng sẽ phải cung cấp đầy đủ các thông tin liên quan theo quy định tại Điều
                                            29 của Nghị định số 52/2013/NĐ-CP và phải hoàn toàn chịu trách nhiệm đối với
                                            các thông tin này. Các thông tin bao gồm:
                                            <br/>
                                            • Tên và địa chỉ trụ sở của thương nhân, tổ chức hoặc tên và địa chỉ thường
                                            trú của cá nhân.
                                            <br/>
                                            • Số, ngày cấp và nơi cấp giấy chứng nhận đăng ký kinh doanh của thương
                                            nhân, hoặc số, ngày cấp và đơn vị cấp quyết định thành lập của tổ chức, hoặc
                                            mã số thuế cá nhân của cá nhân.
                                            <br/>
                                            • Số điện thoại hoặc một phương thức liên hệ trực tuyến khác
                                            <br/>
                                            - Các hoạt động giao dịch thanh toán với Nhà Bán hàng đều được mocha.com.vn
                                            thực hiện thông qua văn bản và email và được xác nhận bởi các bên liên quan
                                            <br/>
                                            b. Quy định kiểm duyệt nội dung đăng tải
                                            <br/>
                                            - Ban quản trị website sẽ thẩm định các thông tin trước khi cho đăng tải lên
                                            hệ thống, đảm bảo tính trung thực của nội dung trên hệ thống và thuộc các
                                            đối tác hợp tác với Viettel.
                                            <br/>
                                            c. Kiểm soát giao dịch
                                            <br/>
                                            - Các giao dịch của khách hàng trên video.mocha.com.vn sẽ được Ban quản trị
                                            website xử lý và theo dõi theo một quy trình hoàn chỉnh và lưu log. Đồng
                                            thời, Ban quản trị thực hiện đánh giá chất lượng đối tác và kiểm soát thông
                                            qua phản hồi của Khách hàng.
                                            <br/>
                                            d. Cơ chế Khiếu nại của khách hàng về đối tác
                                            <br/>
                                            - Khách hàng có quyền gửi khiếu nại đến Ban quản trị website. Khi tiếp nhận
                                            những phản hồi này, Ban quản trị website sẽ xác nhận lại thông tin, trường
                                            hợp đúng như phản ánh của khách hàng tùy theo mức độ, Ban quản trị website
                                            sẽ có những biện pháp xử lý kịp thời nhằm bảo vệ quyền lợi của Khách hàng.
                                            <br/>
                                            <b>1.2. Quy định đối với Khách hàng </b>
                                            <br/>
                                            - Mọi Đăng ký của Khách hàng thông qua SMS hay trực tiếp trên Mạng xã hội
                                            mocha.com.vn đều phải qua bước Xác nhận. Khi hệ thống Mocha nhận được yêu
                                            cầu Đăng ký giao dịch của Khách hàng, hệ thống Mocha luôn luôn yêu cầu Khách
                                            hàng xác nhận ở bước 2 bằng cách Đồng Ý hay Bỏ qua để minh bạch hoá giao
                                            dịch.
                                            <br/>
                                            - Trên trang Xác nhận giao dịch hoặc tin nhắn yêu cầu xác nhận, hệ thống
                                            Mocha đều thông báo rõ giá cước để Khách hàng nắm được.
                                            <br/>
                                            - Khi Khách hàng Đăng ký giao dịch thành công, thông tin Mocha trả về luôn
                                            có cấu trúc Huỷ đăng ký giao dịch để Khách hàng nắm và chủ động trong việc
                                            Huỷ bỏ quyền lợi khi không có nhu cầu sử dụng dịch vụ nữa.
                                            <br/>
                                            <b>2. Chính sách An toàn thông tin hệ thống kỹ thuật </b>
                                            <br/>
                                            <b>2.1. Tính năng của phần mềm </b>
                                            <br/>
                                            - Hệ thống được xây dựng và phát triển trên các tiêu chuẩn An toàn thông tin
                                            và thường xuyên được rà soát định kỳ 03 đến 06 tháng một lần tổng thể toàn
                                            bộ hệ thống.
                                            <br/>
                                            - Trước mỗi lần cập nhật các chức năng, tính năng mới đều được kiểm tra đạt
                                            các yêu cầu về bảo mật, an toàn thông tin mới được triển khai.
                                            <br/>
                                            <b>2.2. Mô hình triển khai, thiết kế hệ thống </b>
                                            <br/>
                                            - Các phân hệ của hệ thống được thiết kế nằm trong các zone mạng độc lập
                                            phân tách với nhau bằng tường lửa đảm bảo việc phân tách khi có 1 máy chủ bị
                                            sự cố tấn công.
                                            <br/>
                                            - Các máy chủ được cài đặt phần mềm diệt virut có bản quyền, cập nhật dữ
                                            liệu hàng ngày. Các dữ liệu máy chủ, cơ sở dữ liệu được sao lưu định kỳ hàng
                                            ngày và chuyển về một máy chủ lưu trữ tập trung độc lập
                                            <br/>
                                            <b>2.3. Hệ thống giám sát và cảnh báo </b>
                                            <br/>
                                            - Hệ thống được giám sát cảnh báo 24/24 bằng phần mềm, bất cứ có sự cố về
                                            máy chủ, truy cập sẽ được cảnh báo bằng SMS cho quản trị viên đảm bảo hệ
                                            thống được online 24/24.
                                            <br/>
                                            - Ngoài ra còn triển khai hệ thống cảnh báo OSSEC log tập trung ( log truy
                                            cập máy chủ, log truy cập phần mềm..) khi có các hành động tấn công hoặc các
                                            log ghi nhận bất thường sẽ có cảnh báo ngay lập tức cho quản trị viên.
                                            <br/>
                                            <b>2.4. Biện pháp kỹ thuật đảm bảo </b>
                                            <br/>
                                            - Ngoài các giải pháp về đảm bảo an toàn an ninh thông tin, chúng tôi có
                                            phương án dự phòng, ví dụ như backup dữ liệu khi gặp sự cố, có máy nổ dự
                                            phòng, ổ cứng lưu trữ,….
                                            <br/>
                                            - Phân tải cho các trang Web Portal, SSO được host trên nhiều máy chủ
                                            <br/>
                                            - Xây dựng một hệ thống load balancer chạy phân tải active-active giữa phân
                                            hệ. Mô tả chi tiết: Hệ thống dịch vụ phải triển khai mô hình Active-Active
                                            là mỗi cụm phân hệ có ít nhất 2 máy chủ chạy song song. Nếu một trong hai
                                            máy chủ bị sự cố tự động chạy switch máy chủ thư 2. Đảm bảo dịch vụ và không
                                            bị gián đoạn dịch vụ
                                            <br/>
                                            - Sử dụng cơ chế Backup-Replicate dữ liệu, cho phép phục hồi dữ liệu trước
                                            thời điểm lỗi.
                                            <br/>
                                            - Cơ chế Phân tải, Active-Active, đồng bộ, backup dữ liệu, cho phép phục hồi
                                            dữ liệu trước thời điểm lỗi tạo nên hệ thống hoạt động ổn định.
                                            <br/>
                                            - Cơ chế sử dụng Webcache, memcache giúp cho hệ thống tăng sức chịu tải đáp
                                            ứng lượng connection lớn
                                            <br/>
                                            - Độ an toàn bảo mật sản phẩm:
                                            <br/>
                                            • Hệ thống được phân chia theo từng khu vực Public &amp; Private, đảm bảo
                                            tính an toàn về mạng.
                                            <br/>
                                            • Mật khẩu cung cấp cho khách hàng được sinh ngẫu nhiên và mã hóa một chiều
                                            (MD5) đảm bảo tính duy nhất. Mật khẩu trong trường hợp reset được gửi trực
                                            tiếp về điện thoại cá nhân.
                                            <br/>
                                            • Đăng nhập an toàn với cơ chế HTTPs và Capcha
                                            <br/>
                                            • Nội dung được kiểm soát với cơ chế bảo mật AntiXSS
                                            <br/>
                                            • Có cơ chế cảnh báo tức thời cho Quản trị viên trong trường hợp Hệ thống có
                                            sự cố đường truyền, phân tải
                                            <br/>
                                            - Về checklist giám sát và KPI:
                                            <br/>
                                            • Hệ thống đã có công cụ giám sát và cảnh báo ứng dụng tự động, kiểm tra một
                                            phút một lần và gửi cảnh báo qua tin nhắn cho Quản trị viên. Hệ thống cũng
                                            đã có log ứng dụng, đảm bảo log theo thời gian thực và được hậu kiểm hiệu
                                            năng, đảm bảo đáp ứng yêu cầu hiệu năng.
                                            <br/>
                                            - Thông tin về các quyền, trách nhiệm và các rủi ro khi lưu trữ trao đổi
                                            thông tin của các thành viên khi tham gia website video.mocha.com.vn được
                                            nêu rõ, chi tiết bằng các điều khoản cụ thể được thông báo cho người dùng
                                            ngay khi đăng ký là thành viên.
                                            <br/>
                                            - Cơ chế mã hóa: Thông tin user, người dùng sẽ được lưu trữ trong cơ sở dữ
                                            liệu và được hashed với thuật toán mã hóa sha512.
                                            <br/>
                                            - Cơ chế đăng nhập: sử dụng số điện thoại mạng viettel với cơ chế nhận diện
                                            thuê bao của nhà mạng đảm bảo việc chính xác user đăng nhập.
                                            <br/>
                                            - Cơ chế OTP: với mã bảo mật OTP sử dụng 1 lần, gửi trực tiếp về số điện
                                            thoại khách hàng đảm bảo các việc xác nhận chính xác người dùng.
                                            <br/>
                                            - Cơ chế dự phòng, lưu trữ: Các thông tin về hệ thống cũng như người dùng
                                            được lưu trữ và sao lưu dự phòng định kỳ hàng ngày để đảm việc luôn sẵn sàng
                                            của hệ thống với các sự cố có thể xảy ra.
                                            <br/>
                                            <b>VI. Bảo vệ thông tin cá nhân khách hàng </b>
                                            <br/>
                                            Để đảm bảo công khai, khi khách hàng gửi yêu cầu tham gia website (khi không
                                            truy cập dịch vụ 3G), Viettel sẽ thông báo nội dung thu thập thông tin sau
                                            đây để khách hàng xem xét, lựa chọn có tham gia sử dụng dịch vụ hay không.
                                            Gồm có:
                                            <br/>
                                            <b>1. Mục đích và phạm vi thu thập </b>
                                            <br/>
                                            - Việc thu thập dữ liệu chủ yếu trên video.mocha.com.vn bao gồm: tên sử dụng
                                            - user (là số điện thoại của Khách hàng) và mật khẩu. Đây là các thông tin
                                            mà website cần khách hàng cung cấp bắt buộc khi đăng ký sử dụng dịch vụ nhằm
                                            đảm bảo quyền lợi cho cho khách hàng.
                                            <br/>
                                            - Trong trường hợp khách hàng sử dụng dịch vụ 3G của Viettel, khi truy cập
                                            vào trang web, hệ thống có hỗ trợ tính năng nhận diện thuê bao giúp khách
                                            hàng không phải đăng nhập và đăng ký thông tin trước khi sử dụng dịch vụ.
                                            Nếu người dùng sử dụng mạng wifi hoặc hệ thống không nhận diện được thuê
                                            bao, người dùng sẽ được cấp tài khoản với tên sử dụng - user (là số điện
                                            thoại của Khách hàng) và mật khẩu để truy cập.
                                            <br/>
                                            - Số điện thoại khách hàng sẽ được xem như là công cụ để KH được hưởng chính
                                            sách miễn cước data 3G/4G khi sử dụng dịch vụ.
                                            <br/>
                                            - Các khách hàng sẽ tự chịu trách nhiệm về bảo mật mọi hoạt động sử dụng
                                            dịch vụ dưới tên đăng ký, mật khẩu của mình. Ngoài ra, khách hàng có trách
                                            nhiệm thông báo kịp thời cho cho Ban quản trị về những hành vi sử dụng trái
                                            phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của bên thứ
                                            ba để có biện pháp giải quyết phù hợp.
                                            <br/>
                                            <b>2. Phạm vi sử dụng thông tin </b>
                                            <br/>
                                            - Cung cấp và quản trị việc đăng ký;
                                            <br/>
                                            - Gửi thông báo, cập nhật các video mới cập nhật;
                                            <br/>
                                            - Ngăn ngừa các hoạt động phá hủy tài khoản của khách hàng hoặc các hoạt
                                            động giả mạo;
                                            <br/>
                                            - Không sử dụng thông tin cá nhân của khách hàng ngoài mục đích xác nhận và
                                            liên hệ có liên quan đến giao dịch tại website.
                                            <br/>
                                            <b>3. Thời gian lưu trữ thông tin </b>
                                            <br/>
                                            - Dữ liệu cá nhân của Khách hàng sẽ được lưu trữ cho đến khi có yêu cầu hủy
                                            bỏ hoặc tự khách hàng đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi
                                            trường hợp thông tin cá nhân khách hàng sẽ được bảo mật trên máy chủ của
                                            mocha.com.vn.
                                            <br/>
                                            <b>4. Địa chỉ của đơn vị thu thập và quản lý thông tin cá nhân </b>
                                            <br/>
                                            - Công ty/Tổ chức: Công ty Truyền thông Viettel
                                            <br/>
                                            - Địa chỉ: Tầng 4, tòa nhà The Light, Đường Tố Hữu, Hà Nội.
                                            <br/>
                                            <b>5. Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá
                                                nhân của mình. </b>
                                            <br/>
                                            - Khách hàng có quyền gửi khiếu nại về việc lộ thông tin các nhân cho bên
                                            thứ 3 đến Ban quản trị của website. Khi tiếp nhận những phản hồi này, Ban
                                            quản trị sẽ xác nhận lại thông tin, phải có trách nhiệm trả lời lý do và
                                            hướng dẫn khách hàng khôi phục và bảo mật lại thông tin.
                                            <br/>
                                            <b>6. Cam kết bảo mật thông tin cá nhân khách hàng </b>
                                            <br/>
                                            - Thông tin cá nhân của khách hàng trên trên website được cam kết bảo mật
                                            tuyệt đối theo chính sách bảo vệ thông tin cá nhân của website. Việc thu
                                            thập và sử dụng thông tin của mỗi khách hàng chỉ được thực hiện khi có sự
                                            đồng ý của khách hàng đó trừ những trường hợp pháp luật có quy định khác.
                                            <br/>
                                            - Không chia sẻ, tiết lộ, chuyển giao thông tin cho một bên thứ ba;
                                            <br/>
                                            - Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất
                                            dữ liệu cá nhân khách hàng, Mocha sẽ có trách nhiệm thông báo vụ việc cho cơ
                                            quan chức năng điều tra xử lý kịp thời và thông báo cho khách hàng được
                                            biết.
                                            <br/>
                                            <b>VII. Quản lý thông tin xấu </b>
                                            <br/>
                                            Ban quản trị sẽ không hợp tác với các đối tác, thành viên mạng xã hội cung
                                            cấp nội dung vi phạm quy định của pháp luật cụ thể như sau:
                                            <br/>
                                            - Lợi dụng việc cung cấp, sử dụng dịch vụ Internet và thông tin trên mạng
                                            nhằm mục đích:
                                            <br/>
                                            • Chống lại Nhà nước Cộng hòa Xã hội Chủ nghĩa Việt Nam; gây phương hại đến
                                            an ninh quốc gia, trật tự an toàn xã hội; phá hoại khối đại đoàn kết dân
                                            tộc; tuyên truyền chiến tranh, khủng bố; gây hận thù, mâu thuẫn giữa các dân
                                            tộc, sắc tộc, tôn giáo;
                                            <br/>
                                            • Tuyên truyền, kích động bạo lực, dâm ô, đồi trụy, tội ác, tệ nạn xã hội,
                                            mê tín dị đoan, phá hoại thuần phong, mỹ tục của dân tộc;
                                            <br/>
                                            • Tiết lộ bí mật nhà nước, bí mật quân sự, an ninh, kinh tế, đối ngoại và
                                            những bí mật khác do pháp luật quy định;
                                            <br/>
                                            • Đưa thông tin xuyên tạc, vu khống, xúc phạm uy tín của tổ chức, danh dự và
                                            nhân phẩm của cá nhân;
                                            <br/>
                                            • Quảng cáo, tuyên truyền, mua bán hàng hóa, dịch vụ bị cấm; truyền bá tác
                                            phẩm báo chí, văn học, nghệ thuật, xuất bản phẩm bị cấm;
                                            <br/>
                                            • Giả mạo tổ chức, cá nhân và phát tán thông tin giả mạo, thông tin sai sự
                                            thật xâm hại đến quyền và lợi ích hợp pháp của tổ chức, cá nhân.
                                            <br/>
                                            - Cản trở trái pháp luật việc cung cấp và truy cập thông tin hợp pháp, việc
                                            cung cấp và sử dụng các dịch vụ hợp pháp trên Internet của tổ chức, cá nhân.
                                            <br/>
                                            - Cản trở trái pháp luật hoạt động của hệ thống máy chủ tên miền quốc gia
                                            Việt Nam “.vn”, hoạt động hợp pháp của hệ thống thiết bị cung cấp dịch vụ
                                            Internet và thông tin trên mạng.
                                            <br/>
                                            - Sử dụng trái phép mật khẩu, khóa mật mã của tổ chức, cá nhân; thông tin
                                            riêng, thông tin cá nhân và tài nguyên Internet.
                                            <br/>
                                            - Tạo đường dẫn trái phép đối với tên miền hợp pháp của tổ chức, cá nhân;
                                            tạo, cài đặt, phát tán phần mềm độc hại, virus máy tính; xâm nhập trái phép,
                                            chiếm quyền điều khiển hệ thống thông tin, tạo lập công cụ tấn công trên
                                            Internet.
                                            <br/>
                                            <b>VIII. Trách nhiệm trong trường hợp phát sinh lỗi kỹ thuật </b>
                                            <br/>
                                            Viettel xây dựng hệ thống kỹ thuật và các giải pháp nhằm đảm bảo an toàn, an
                                            ninh cho thông tin cá nhân:
                                            <br/>
                                            - Công ty Truyền thông Viettel – Chi nhánh Tập đoàn Viễn thông Quân đội chịu
                                            trách nhiệm về mật khẩu và bản quyền phần mềm thiết kế, phần mềm chống
                                            virus, các kỹ thuật đảm bảo cho trang tin điện tử hoạt động, bảo trì hệ
                                            thống, xử lý kỹ thuật khi có sự cố xẩy ra.
                                            <br/>
                                            - Về cơ sở dữ liệu (CSDL): Hệ thống sử dụng CSDL MSSQL đã được update bản vá
                                            an toàn thông tin, thu hồi các quyền không cần thiết, đảm bảo các tài khoản
                                            được cấp đúng và đủ quyền nhằm ngăn chặn nguy cơ tài khoản bị chiếm quyền sử
                                            dụng. Cấu hình đảm bảo tuân thủ các quy định về an toàn thông tin.
                                            <br/>
                                            - Hệ điều hành máy chủ và mô hình mạng hệ thống đáp ứng và tuân thủ đúng quy
                                            hoạch và quy định về an toàn thông tin.
                                            <br/>
                                            - Về checklist giám sát và KPI: Hệ thống đã có công cụ giám sát và cảnh báo
                                            ứng dụng tự động, kiểm tra một phút một lần và gửi cảnh báo qua tin nhắn cho
                                            Quản trị viên. Hệ thống cũng đã có log ứng dụng, đảm bảo log theo thời gian
                                            thực và được hiệu kiểm hiệu năng, đảm bảo đáp ứng yêu cầu hiệu năng.
                                            <br/>
                                            - Cơ chế dự phòng và phân tải hệ thống: Hệ thống dự phòng theo cơ chế active
                                            – standby trong đó server chạy CMS và Media đã dựng dự phòng Web Server,
                                            trên Web Server cũng đã dựng dự phòng CMS và TT Digital.
                                            <br/>
                                            - Khi thực hiện các giao dịch trên Sàn, bắt buộc các thành viên phải thực
                                            hiện đúng theo các quy trình hướng dẫn.
                                            <br/>
                                            - Ban quản lý Mạng xã hội video.mocha.com.vn cam kết cung cấp chất lượng
                                            dịch vụ tốt nhất cho các thành viên tham gia giao dịch. Trường hợp phát sinh
                                            lỗi kỹ thuật, lỗi phần mềm hoặc các lỗi khách quan khác dẫn đến Thành viên
                                            không thể tham gia giao dịch được thì các Thành viên thông báo cho Ban quản
                                            lý Mạng xã hội qua địa chỉ số hotline trên website, Chúng tôi sẽ khắc phục
                                            lỗi trong thời gian sớm nhất, tạo điều kiện cho các Thành viên tham gia Mạng
                                            xã hội video.mocha.com.vn
                                            <br/>
                                            - Tuy nhiên, Ban quản lý Mạng xã hội video.mocha.com.vn sẽ không chịu trách
                                            nhiệm giải quyết trong trường hợp thông báo của các Thành viên không đến
                                            được Ban quản lý, phát sinh từ lỗi kỹ thuật, lỗi đường truyền, phần mềm hoặc
                                            các lỗi khác không do Ban quản lý gây ra.
                                            <br/>
                                            <b>IX. Quyền và nghĩa vụ của Ban quản trị website Mạng xã hội
                                                video.mocha.com.vn </b>
                                            <br/>
                                            <b>1. Quyền của Ban quản lý website/ứng dụng [video.mocha.com.vn] </b>
                                            <br/>
                                            - Kiểm duyệt trước các nội dung mà Nhà Bán hàng đưa lên.
                                            <br/>
                                            - Website mocha.com.vn sẽ tiến hành lưu log đăng ký của khách sau khi đăng
                                            ký dịch vụ và sẽ cấp quyền hưởng ưu đãi miễn cước data 3G/4G khi xem video
                                            trên trang video.mocha.com.vn.
                                            <br/>
                                            - Mạng xã hội mocha.com.vn sẽ tiến hành cung cấp các dịch vụ cho các Nhà Bán
                                            hàng tham gia sau khi đã hoàn thành các thủ tục và các điều kiện bắt buộc
                                            Mạng xã hội mocha.com.vn nêu ra.
                                            <br/>
                                            - Trong trường hợp có cơ sở để chứng minh đối tác cung cấp video, hình ảnh
                                            cho website video.mocha.com.vn không chính xác, sai lệch, không đầy đủ hoặc
                                            vi phạm pháp luật hay thuần phong mỹ tục Việt Nam thì mocha.com.vn có quyền
                                            từ chối, tạm ngừng hoặc chấm dứt hợp tác với đối tác đó, gỡ bỏ video trên
                                            trang.
                                            <br/>
                                            - Video.mocha.com.vn có thể chấm dứt quyền khách hàng và quyền sử dụng một
                                            hoặc tất cả các dịch vụ của khách hàng và sẽ thông báo cho khách hàng trong
                                            thời hạn ít nhất là 01 ngày trong trường hợp khách hàng vi phạm các Quy chế
                                            của Website video.mocha.com.vn hoặc có những hành vi ảnh hưởng đến hoạt động
                                            kinh doanh của đối tác video.mocha.com.vn.
                                            <br/>
                                            - Website video.mocha.com.vn sẽ xem xét việc hủy dịch vụ của khách hàng nếu
                                            thành công thanh toán cước phí liên tục trong 90 (chín mươi) ngày. Nếu muốn
                                            tiếp tục trở thành khách hàng và được cấp lại quyền sử dụng dịch vụ thì phải
                                            đăng ký lại từ đầu theo quy định.
                                            <br/>
                                            - Mạng xã hội mocha.com.vn có thể chấm dứt quyền Nhà Bán hàng và sẽ thông
                                            báo cho Nhà Bán hàng trong thời hạn ít nhất là một (1) tháng trong trường
                                            hợp Nhà Bán hàng vi phạm các Qui chế của Mạng xã hội mocha.com.vn hoặc có
                                            những hành vi ảnh hưởng đến hoạt động kinh doanh Trên Mạng xã hội
                                            mocha.com.vn.
                                            <br/>
                                            - Mạng xã hội mocha.com.vn sẽ xem xét việc chấm dứt quyền sử dụng dịch vụ và
                                            quyền của Nhà Bán hàng nếu Nhà Bán hàng không tham gia hoạt động giao dịch
                                            và trao đổi thông tin trên Mạng xã hội mocha.com.vn liên tục trong ba ( 3)
                                            tháng. Nếu muốn tiếp tục trở thành Nhà Bán hàng và được cấp lại quyền sử
                                            dụng dịch vụ thì phải đăng ký lại từ đầu theo mẫu và hợp đồng của SMạng xã
                                            hội mocha.com.vn.
                                            <br/>
                                            - Mạng xã hội mocha.com.vn có thể chấm dứt ngay quyền Nhà Bán hàng của Nhà
                                            Bán hàng nếu video.mocha.com.vn phát hiện Nhà Bán hàng đã phá sản, bị kết án
                                            hoặc đang trong thời gian thụ án, trong trường hợp Nhà Bán hàng tiếp tục
                                            hoạt động có thể gây cho Mạng xã hội mocha.com.vn trách nhiệm pháp lý, có
                                            những hoạt động lừa đảo, giả mạo, gây rối loạn thị trường, gây mất đoàn kết
                                            đối với các Nhà Bán hàng khác của Mạng xã hội mocha.com.vn, hoạt động vi
                                            phạm pháp luật hiện hành của Việt Nam. Trong trường hợp chấm dứt quyền Nhà
                                            Bán hàng thì tất cả các chứng nhận, các quyền của Nhà Bán hàng được cấp sẽ
                                            mặc nhiên hết giá trị và bị chấm dứt.
                                            <br/>
                                            - Mạng xã hội mocha.com.vn giữ bản quyền sử dụng dịch vụ và các nội dung
                                            trên Mạng xã hội mocha.com.vn theo các quy định Pháp luật về bảo hộ sở hữu
                                            trí tuệ tại Việt Nam. Tất cả các biểu tượng, nội dung theo các ngôn ngữ khác
                                            nhau đều thuộc quyền sở hữu của Mạng xã hội mocha.com.vn. Nghiêm cấm mọi
                                            hành vi sao chép, sử dụng và phổ biến bất hợp pháp các quyền sở hữu trên.
                                            <br/>
                                            - Mạng xã hội mocha.com.vn giữ quyền được thay đổi bảng, biểu giá dịch vụ và
                                            phương thức thanh toán trong thời gian cung cấp dịch vụ cho Nhà Bán hàng
                                            theo nhu cầu và điều kiện khả năng của Mocha và sẽ báo trước cho Nhà Bán
                                            hàng thời hạn là một ( 1) tháng.
                                            <br/>
                                            - Ngoài ra, nếu video thuộc kênh của bạn không phát sinh đủ lượt xem &gt; 10
                                            trong 2 tuần liên tiếp, Mocha Video có quyền xóa khỏi hệ thống và sẽ báo
                                            trước cho khách hàng thời hạn là một (1) tuần.
                                            <br/>
                                            - Thu thập thông tin khách hàng nhằm mục đích nêu trong chính sách bảo mật
                                            thông tin cá nhân khách hàng.
                                            <br/>
                                            <b>2. Trách nhiệm của Ban quản trị Mạng xã hội mocha.com.vn </b>
                                            <br/>
                                            - Tuân thủ tuyệt đối các quy định tại Điều 36 Nghị định số 52/2013/NĐ-CP về
                                            trách nhiệm của thương nhân, tổ chức cung cấp dịch vụ thương mại điện tử.
                                            <br/>
                                            - Mạng xã hội mocha.com.vn có trách nhiệm yêu cầu Nhà bán hàng cung cấp
                                            thông tin: Tên, địa chỉ trụ sở của thương nhân/tổ chức hoặc tên và địa chỉ
                                            thường chú của cá nhân; Số, ngày cấp, nơi cấp Giấy chứng nhận đăng ký kinh
                                            doanh của thương nhân hoặc số, ngày cấp, đơn vị cấp quyết định thành lập của
                                            tổ chức hoặc mã số thuế cá nhân của các nhân; Số điện thoại hoặc một phương
                                            thức liên hệ trực tuyến khác. Đồng thời Mocha xây dựng, thực hiện “cơ chế
                                            kiểm tra, giám sát để đảm bảo việc cung cấp thông tin của người bán trên sàn
                                            giao dịch thương mại điện tử được thực hiện chính xác đầy đủ” theo quy định
                                            tại Khoản 4 Điều 36 Nghị định 52/2013/NĐ-CP.
                                            <br/>
                                            - Yêu cầu Nhà Bán hàng cung cấp chứng nhận đủ điều kiện kinh doanh và quyền
                                            sở hữu hợp pháp video bán trên Mạng xã hội mocha.com.vn
                                            <br/>
                                            - Lưu trữ thông tin đăng ký của các thương nhân, tổ chức, cá nhân tham gia
                                            Mạng xã hội mocha.com.vn và thường xuyên cập nhật những thông tin thay đổi,
                                            bổ sung liên quan.
                                            <br/>
                                            - Áp dụng các biện pháp cần thiết để đảm bảo an toàn thông tin liên quan đến
                                            bí mật kinh doanh của Nhà bán hàng và thông tin cá nhân của khách hàng.
                                            <br/>
                                            - Có biện pháp xử lý kịp thời khi phát hiện hoặc nhận được phản ánh về hành
                                            vi kinh doanh vi phạm pháp luật trên Mạng xã hội.
                                            <br/>
                                            - Hỗ trợ cơ quan quản lý Nhà nước điều tra các hành vi kinh doanh vi phạm
                                            pháp luật, cung cấp thông tin đăng ký, lịch sử giao dịch và các tài liệu
                                            khác về đối tượng có hành vi vi phạm pháp luật trên Mạng xã hội mocha.com.vn
                                            <br/>
                                            - Mạng xã hội mocha.com.vn có trách nhiệm tiếp nhận thông tin phản ảnh,
                                            khiếu nại từ khách hàng và kiểm tra nội dung phản ánh, khiếu nại đó. Nếu xét
                                            thấy những phản ánh đó là đúng Sàn sẽ yêu cầu người bán phải giải trình về
                                            nhữg thông tin đó. Tuỳ theo mức độ sai phạm Mạng xã hội mocha.com.vn sẽ có
                                            phướng án yêu cầu người bán đền bù cho người mua, sẽ khóa tài khoàn thành
                                            viên người bán.
                                            <br/>
                                            - Loại bỏ khỏi Mạng xã hội mocha.com.vn những thông tin bán hàng vi phạm
                                            quyền sở hữu trí tuệ và các sản phẩm hàng hóa, dịch vụ vi phạm pháp luật
                                            khác khi phát hiện hoặc nhận được phản ánh có căn cứ xác thực về những thông
                                            tin này.
                                            <br/>
                                            - Ngăn chặn và loại bỏ khỏi Mạng xã hội những thông tin bán hàng, dịch vụ
                                            thuộc danh mục hàng hóa, dịch vụ cấm kinh doanh theo quy định của Pháp luật.
                                            <br/>
                                            <b>X. Quyền và trách nhiệm của đối tác trên website
                                                [video.mocha.com.vn] </b>
                                            <br/>
                                            <b>1. Quyền của Nhà Bán Hàng </b>
                                            <br/>
                                            - Khi đăng ký trở thành Nhà Bán Hàng của Mạng xã hội mocha.com.vn và được
                                            Mạng xã hội mocha.com.vn sẽ khởi tạo các trang hàng trực tuyến để giới thiệu
                                            video của Nhà Bán Hàng trên Mạng xã hội mocha.com.vn.
                                            <br/>
                                            - Nhà Bán Hàng sẽ được hưởng lợi nhuận từ việc các Khách hàng đăng ký thành
                                            viên VIP và xem video của mình.
                                            <br/>
                                            - Nhà Bán Hàng có quyền đóng góp ý kiến cho Mạng xã hội mocha.com.vn trong
                                            quá trình hoạt động. Các kiến nghị được gửi trực tiếp bằng thư hoặc email
                                            đến cho Mạng xã hội mocha.com.vn.
                                            <br/>
                                            <b>2. Nghĩa vụ của Nhà Bán hàng </b>
                                            <br/>
                                            - Cung cấp đầy đủ và chính xác các thông tin: Tên, địa chỉ trụ sở của thương
                                            nhân/tổ chức hoặc tên và địa chỉ thường chú của cá nhân; Số, ngày cấp, nơi
                                            cấp Giấy chứng nhận đăng ký kinh doanh của thương nhân hoặc số, ngày cấp,
                                            đơn vị cấp quyết định thành lập của tổ chức hoặc mã số thuế cá nhân của các
                                            nhân; Số điện thoại hoặc một phương thức liên hệ trực tuyến khác cho Ban
                                            quản trị Mạng xã hội mocha.com.vn
                                            <br/>
                                            - Nhà Bán Hàng cam kết có đầy đủ chức năng phân phối tất cả sản phẩm Nhà Bán
                                            Hàng đăng tải và bán trên Mạng xã hội mocha.com.vn và Nhà Bán Hàng đồng ý
                                            chịu tất cả trách nhiệm pháp lý trong trường hợp vi phạm điều khoản này.
                                            <br/>
                                            - Nhà Bán Hàng cam kết tất cả video Nhà Bán Hàng đăng tải và bán trên Mạng
                                            xã hội mocha.com.vn là sản phẩm không vi phạm bất kỳ quyền sở hữu trí tuệ
                                            nào và/hoặc bất kỳ nhãn hiệu hàng hóa nào của bất kỳ bên thứ ba nào và Nhà
                                            Bán Hàng đồng ý chịu tất cả trách nhiệm pháp lý trong trường hợp vi phạm
                                            điều khoản này.
                                            <br/>
                                            - Nhà Bán Hàng cam kết những video cung cấp cho Mạng xã hội mocha.com.vn là
                                            những video không được vi phạm bất kỳ điều cấm của Pháp luật Việt Nam như:
                                            Các nội dung chống lại Nhà nước Cộng hòa xã hội chủ nghĩa Việt Nam; gây
                                            phương hại đến an ninh quốc gia, trật tự an toàn xã hội; phá hoại khối đại
                                            đoàn kết dân tộc; tuyên truyền chiến tranh, khủng bố; gây hận thù, mâu thuẫn
                                            giữa các dân tộc, sắc tộc, tôn giáo; Các nội dung tuyên truyền, kích động
                                            bạo lực, dâm ô, đồi trụy, tội ác, tệ nạn xã hội, mê tín dị đoan, phá hoại
                                            thuần phong, mỹ tục của dân tộc; Các nội dung tiết lộ bí mật nhà nước, bí
                                            mật quân sự, an ninh, kinh tế, đối ngoại và những bí mật khác do pháp luật
                                            quy định;…
                                            <br/>
                                            - Đảm bảo tính chính xác, trung thực của thông tin về hàng hoá, dịch vụ cung
                                            cấp trên Mạng xã hội mocha.com.vn
                                            <br/>
                                            - Cung cấp thông tin về tình hình kinh doanh của mình khi có yêu cầu của cơ
                                            quan nhà nước có thẩm quyền để phục vụ hoạt động thống kê thương mại điện
                                            tử.
                                            <br/>
                                            - Tuân thủ quy định của pháp luật về thanh toán, quảng cáo, khuyến mại, bảo
                                            vệ quyền sở hữu trí tuệ, bảo vệ quyền lợi người tiêu dùng và các quy định
                                            của pháp luật có liên quan khác khi bán hàng hoá hoặc cung ứng dịch vụ trên
                                            Mạng xã hội mocha.com.vn
                                            <br/>
                                            - Không được hành động gây mất uy tín của Mạng xã hội mocha.com.vn dưới mọi
                                            hình thức như gây mất đoàn kết giữa các Nhà Bán Hàng bằng cách sử dụng tên
                                            đăng ký thứ hai, thông qua một bên thứ ba hoặc tuyên truyền, phổ biến những
                                            thông tin không có lợi cho uy tín của Mạng xã hội mocha.com.vn.
                                            <br/>
                                            <b>XI. Quyền và trách nhiệm của Khách hàng video.mocha.com.vn </b>
                                            <br/>
                                            <b>1. Quyền của khách hàng </b>
                                            <br/>
                                            - Đối với các thành viên đăng ký là Khách hàng VIP của Mạng xã hội
                                            mocha.com.vn sẽ được miễn phí xem toàn bộ nội dung trên Mocha.
                                            <br/>
                                            - Khách hàng sẽ được cấp một mật khẩu riêng để được vào sử dụng các dịch vụ.
                                            xem nội dung trên internet.
                                            <br/>
                                            - Tham gia vào trang video.mocha.com.vn để xem video miễn cước data 3G/4G.
                                            <br/>
                                            - Khách hàng sẽ được nhân viên của Mạng xã hội mocha.com.vn hỗ trợ để sử
                                            dụng được các công cụ, các tính năng phục vụ cho việc xây dựng, tiến hành
                                            giao dịch và sử dụng các dịch vụ tiện ích trên Mạng xã hội mocha.com.vn.
                                            <br/>
                                            - Khách hàng sẽ được hưởng các chính sách ưu đãi do Mạng xã hội mocha.com.vn
                                            hay các đối tác thứ ba cung cấp trên Mạng xã hội mocha.com.vn . Các chính
                                            sách ưu đãi này sẽ được đăng tải trực tiếp trên Mạng xã hội mocha.com.vn
                                            hoặc được gửi trực tiếp đến các thành viên.
                                            <br/>
                                            - Được bảo vệ thông tin cá nhân. Nếu video.mocha.com.vn cung cấp thông tin
                                            cho bên thứ 3 phải có sự cho phép của Thành viên, ngoại trừ trường hợp theo
                                            yêu cầu của cơ quan pháp luật.
                                            <br/>
                                            - Phản hồi ý kiến về chất lượng sản phẩm, dịch vụ và phản ánh về nhãn
                                            hàng/địa điểm với tinh thần xây dựng, không có thái độ sỉ nhục, phá hoại làm
                                            ảnh hưởng đến dịch vụ.
                                            <br/>
                                            - Khách hàng có quyền đóng góp ý kiến cho Mạng xã hội mocha.com.vn trong quá
                                            trình hoạt động. Các kiến nghị được gửi trực tiếp bằng thư, fax hoặc email
                                            đến cho Mạng xã hội mocha.com.vn theo địa chỉ liên lạc được công bố trên
                                            sàn.
                                            <br/>
                                            <b>2. Trách nhiệm của Khách hàng khi tham gia Mạng xã hội mocha.com.vn </b>
                                            <br/>
                                            - Khách hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ và mọi hoạt động
                                            sử dụng dịch vụ dưới tên đăng ký, mật khẩu của mình. Khách hàng có trách
                                            nhiệm thông báo kịp thời cho Ban quản trị nếu để lộ hoặc mất mật khẩu.
                                            <br/>
                                            - Khách hàng cam kết những thông tin cung cấp cho Mạng xã hội mocha.com.vn
                                            và những thông tin đăng tải lên Mạng xã hội mocha.com.vn là chính xác và
                                            hoàn chỉnh. Khách hàng đồng ý giữ và thay đổi các thông tin trên Sàn giao
                                            dịch điện tử video.mocha.com.vn là cập nhật, chính xác.
                                            <br/>
                                            - Khách hàng cam kết không tham gia Mạng xã hội mocha.com.vn với những mục
                                            đích bất hợp pháp, không hợp lý, lừa đảo, đe doạ, thăm rò thông tin bất hợp
                                            pháp, phá hoại, tạo ra và phát tán virus gây hư hại tới hệ thống, cấu hình,
                                            truyền tải thông tin của Mạng xã hội video.mocha.com.vn hay sử dụng dịch vụ
                                            của mình vào mục đích đầu cơ, lũng đoạn thị trường tạo những đơn đặt hàng,
                                            chào hàng giả, kể cả phục vụ cho việc phán đoán nhu cầu thị trường. Trong
                                            trường hợp vi phạm thì khách hàng phải chịu trách nhiệm về các hành vi của
                                            mình trước pháp luật.
                                            <br/>
                                            - Khách hàng cam kết không được thay đổi, chỉnh sửa, sao chép, truyền bá,
                                            phân phối, cung cấp và tạo những công cụ tương tự của dịch vụ do Mạng xã hội
                                            mocha.com.vn cung cấp cho một bên thứ ba nếu không được sự đồng ý của Mạng
                                            xã hội mocha.com.vn trong bản Quy chế này.
                                            <br/>
                                            - Khách hàng không được hành động gây mất uy tín của Mạng xã hội
                                            mocha.com.vn dưới mọi hình thức như gây mất đoàn kết giữa các khách hàng
                                            bằng cách sử dụng tên đăng ký thứ hai, thông qua một bên thứ ba hoặc tuyên
                                            truyền, phổ biến những thông tin không có lợi cho uy tín của Mạng xã hội
                                            mocha.com.vn.
                                            <br/>
                                            - Trả phí duy trì dịch vụ theo chu kỳ ngày/tuần/tháng.
                                            <br/>
                                            - Chịu trách nhiệm khai báo thông tin và bảo đảm tính chính xác thông tin
                                            cung cấp và bảo mật tài khoản video.mocha.com.vn.
                                            <br/>
                                            <b>XII. Điều khoản áp dụng </b>
                                            <br/>
                                            - Quy chế của Mạng xã hội mocha.com.vn chính thức có hiệu lực thi hành kể từ
                                            ngày ký Quyết định ban hành kèm theo Quy chế này. Mạng xã hội mocha.com.vn
                                            có quyền và có thể thay đổi Quy chế này bằng cách thông báo lên Mạng xã hội
                                            mocha.com.vn cho các thành viên biết. Quy chế sửa đổi có hiệu lực kể từ ngày
                                            Quyết định về việc sửa đổi Quy chế có hiệu lực. Việc thành viên tiếp tục sử
                                            dụng dịch vụ sau khi Quy chế sửa đổi được công bố và thực thi đồng nghĩa với
                                            việc họ đã chấp nhận Quy chế sửa đổi này.
                                            <br/>
                                            <b>XIII. Điều khoản cam kết </b>
                                            <br/>
                                            - Mọi thành viên và đối tác/người bán hàng khi sử dụng video.mocha.com.vn
                                            làm giao dịch mua bán trực tuyến thì đồng nghĩa việc các bên có liên quan đã
                                            chấp thuận tuân theo quy chế này. Mọi thắc mắc của khách hàng xin vui lòng
                                            liên hệ với video.mocha.com.vn theo thông tin dưới đây để được giải đáp.
                                            <br/>
                                            <b>XIV. Liên hệ </b>
                                            <br/>
                                            Địa chỉ liên lạc chính thức của website Mạng xã hội
                                            <b>video.mocha.com.vn </b>
                                            <br/>
                                            •
                                            <b>Website Mạng xã hội mocha.com.vn </b>
                                            <br/>
                                            • Công ty/Tổ chức: Công ty Truyền thông Viettel
                                            <br/>
                                            • Địa chỉ: Tòa nhà The Light, KĐT Trung Văn, Tố Hữu, Hà Nội.
                                            <br/>
                                            • Tel:
                                            024.62776789&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            - Fax: 024.62660466

                                        </div>

                                    </TabPanel>

                                    <TabPanel tabId="four">
                                        <h3 className="mbl-h3" id="textContent">
                                            ĐIỀU KHOẢN SỬ DỤNG
                                        </h3>

                                        <div className="service-info" id="rule" style={{display: 'block'}}>
                                            <b>I. CÁC NỘI DUNG CẤM TRAO ĐỔI, CHIA SẺ TRÊN MẠNG XÃ HỘI
                                                http://video.mocha.com.vn/ </b>
                                            <br/>
                                            - Tuyệt đối nghiêm cấm mọi hành vi tuyên truyền, chống phá và xuyên tạc
                                            chính quyền,
                                            thể chế chính trị, và các chính sách của nhà nước, kích động bạo lực, tuyên
                                            truyền
                                            chiến tranh xâm lược, gây hận thù giữa các dân tộc và nhân dân các nước.
                                            Không lưu
                                            giữ những nội dung dâm ô, đồi trụy, kích động tội ác, tệ nạn xã hội, mê tín
                                            dị đoan,
                                            phá hoại thuần phong mỹ tục của dân tộc, hay tiết lộ bí mật nhà nước, bí mật
                                            quân
                                            sự, an ninh, kinh tế, đối ngoại và những hành vi vi phạm pháp luật khác theo
                                            quy
                                            định của pháp luật Việt Nam. Trường hợp phát hiện, không những bị xóa bỏ tài
                                            khoản
                                            mà chúng tôi còn có thể cung cấp thông tin của người đó cho các cơ quan chức
                                            năng
                                            để xử lý theo pháp luật.

                                            <br/>
                                            - Nghiêm cấm việc xúc phạm, nhạo báng người khác dưới bất kỳ hình thức nào
                                            (nhạo
                                            báng, chê bai, kỳ thị tôn giáo, giới tính, sắc tộc...).

                                            <br/>
                                            - Nghiêm cấm mọi hành vi mạo nhận hay cố ý làm người khác tưởng lầm mình là
                                            một
                                            người khác trên http://video.mocha.com.vn/. Mọi hành vi vi phạm sẽ bị xử lý
                                            hoặc tước bỏ tài khoản.

                                            <br/>
                                            - Không được gửi lên http://video.mocha.com.vn/ bất kỳ thông tin bất hợp
                                            pháp, lừa gạt, bôi nhọ,
                                            sỉ nhục, tục tĩu, khiêu dâm, xúc phạm, đe dọa, lăng mạ, thù hận, kích động…
                                            hoặc
                                            trái với chuẩn mực đạo đức chung của xã hội.

                                            <br/>
                                            - Nghiêm cấm những nội dung miêu tả tỉ mỉ những hành động dâm ô, bạo lực,
                                            giết người
                                            rùng rợn; đăng, phát các hình ảnh phản cảm, thiếu tính nhân văn; cung cấp
                                            nội dung,
                                            hình ảnh, tranh khỏa thân có tính chất kích dục, thiếu thẩm mỹ, không phù
                                            hợp với
                                            thuần phong, mỹ tục Việt Nam.

                                            <br/>
                                            - Nghiêm cấm tuyên truyền những thông điệp mang tính quảng cáo, mời gọi,
                                            quảng bá
                                            cơ hội đầu tư hay bất kỳ dạng liên lạc nào có mục đích thương mại mà người
                                            dùng
                                            không mong muốn; vi phạm hoặc xâm phạm các quyền của những người khác, trong
                                            đó
                                            bao gồm cả tài liệu xâm phạm đến quyền riêng tư hoặc công khai, hoặc tài
                                            liệu được
                                            bảo vệ bản quyền, tên thương mại hoặc quyền sở hữu khác, các sản phẩm phái
                                            sinh
                                            mà không được sự cho phép của người có quyền hợp pháp.
                                            <br/>
                                            - Không được gửi hoặc truyền tải bất kỳ thông tin nào không thuộc quyền sở
                                            hữu của
                                            người sử dụng trừ khi đó là thông tin được cung cấp miễn phí.

                                            <br/>
                                            - Tuyệt đối nghiêm cấm việc gửi bất kỳ thông tin nào có chứa các loại virus,
                                            trojan,
                                            bọ hay các thành phần gây nguy hại đến hệ thống http://video.mocha.com.vn/,
                                            máy tính, mạng Internet
                                            và các thông tin bảo mật của http://video.mocha.com.vn/ và của các thành
                                            viên trên http://video.mocha.com.vn/.

                                            <br/>
                                            - Không được sử dụng, cung cấp thông tin vi phạm các quy định về sở hữu trí
                                            tuệ,
                                            về giao dịch thương mại điện tử và các quy định khác của pháp luật hiện
                                            hành.

                                            <br/>
                                            - Không được xâm phạm, xâm nhập, tiếp cận, sử dụng bất kỳ phần nào trong máy
                                            chủ
                                            của chúng tôi, và bất kỳ dữ liệu nào nếu không được chúng tôi cho phép,
                                            nghiêm cấm
                                            các hành vi lợi dụng lỗi hệ thống để trục lợi cá nhân gây thiệt hại đến nhà
                                            cung
                                            cấp dịch vụ và các thành viên của http://video.mocha.com.vn/.

                                            <br/>
                                            - Không được phá vỡ luồng thông tin bình thường trong một tương tác, đưa ra
                                            tuyên
                                            bố hoặc đại diện cho một doanh nghiệp, hiệp hội, thể chế hay tổ chức nào
                                            khác mà
                                            người sử dụng không được uỷ quyền phát ngôn hoặc nhằm mục đích trục lợi cá
                                            nhân
                                            gây sự nhầm lẫn đối với những thành viên cùng tham gia
                                            http://video.mocha.com.vn/.

                                            <br/>
                                            - Không được tạo ra các thông tin giả mạo cá nhân, tổ chức, doanh nghiệp
                                            khác; thông
                                            tin sai sự thật xâm hại đến quyền và lợi ích hợp pháp của các cá nhân, tổ
                                            chức khác.

                                            <br/>
                                            - Không được có các hành động nhằm hạn chế hoặc cấm đoán bất kỳ người dùng
                                            nào khác
                                            sử dụng http://video.mocha.com.vn/.

                                            <br/>
                                            - Không được truyền bá tác phẩm báo chí, văn học, nghệ thuật, xuất bản phẩm
                                            vi phạm
                                            các quy định của pháp luật.
                                            <br/>
                                            <br/>
                                            <b>II. QUYỀN, TRÁCH NHIỆM CỦA NGƯỜI SỬ DỤNG DỊCH VỤ MẠNG XÃ HỘI </b>
                                            <br/>
                                            <b>1. Quyền của người sử dụng dịch vụ mạng xã hội
                                                http://video.mocha.com.vn/ </b>
                                            <br/>
                                            - Được quyền tham gia sử dụng dịch vụ mạng xã hội
                                            http://video.mocha.com.vn/, nếu đồng ý với nội
                                            dung của Thỏa thuận cung cấp và sử dụng dịch vụ mạng xã hội
                                            http://video.mocha.com.vn/.

                                            <br/>
                                            - Được quyền đăng ký tài khoản người dùng trên http://video.mocha.com.vn/.
                                            <br/>
                                            - Được quyền tham gia diễn đàn, sử dụng và cung cấp thông tin lên diễn đàn
                                            http://video.mocha.com.vn/.

                                            <br/>
                                            - Có quyền chia sẻ thông tin dưới các định dạng http://video.mocha.com.vn/
                                            đã quy định.
                                            <br/>
                                            <br/>
                                            <b>2. Trách nhiệm của người sử dụng dịch vụ mạng xã hội
                                                http://video.mocha.com.vn/ </b>
                                            <br/>
                                            - Có trách nhiệm tuân thủ mọi quy định của http://video.mocha.com.vn/.

                                            <br/>
                                            - Chịu trách nhiệm đối với các nội dung, video clip và bất kỳ sự chia sẻ nào
                                            với
                                            các cá nhân, tổ chức khác trên http://video.mocha.com.vn/.

                                            <br/>
                                            - Không sử dụng http://video.mocha.com.vn/ vào mục đích thương mại mà không
                                            có sự cho phép bằng
                                            văn bản của chúng tôi trước đó.

                                            <br/>
                                            - Không được hạn chế hoặc cấm đoán bất kỳ người dùng nào khác sử dụng và đọc
                                            thông
                                            tin trên http://video.mocha.com.vn/.

                                            <br/>
                                            - Cho phép cho http://video.mocha.com.vn/ sử dụng, trưng bày, tái sản xuất,
                                            chỉnh sửa, làm cho phù
                                            hợp, xuất bản, cung cấp, xúc tiến, dịch và tạo ra các phiên bản phái sinh
                                            hoặc tổ
                                            hợp khác, một phần hoặc toàn bộ, trên phạm vi toàn cầu mà không đòi hỏi thù
                                            lao.
                                            Việc này được áp dụng với bất cứ dạng thức, phương tiện, công nghệ nào đã
                                            được biết
                                            đến hoặc phát triển sau này; người sử dụng cần lưu ý rằng, các video clip
                                            trên http://video.mocha.com.vn/
                                            có thể tiếp tục nằm trên http://video.mocha.com.vn/ ngay cả khi tài khoản
                                            của người sử dụng đã bị
                                            xóa vì bất kỳ lý do gì.

                                            <br/>
                                            - Không yêu cầu về bất kỳ khoản tiền thưởng, phí, nhuận bút, lệ phí và các
                                            kiểu
                                            chi trả khác liên quan đến việc http://video.mocha.com.vn/ sử dụng, tiết lộ,
                                            áp dụng, hoặc chỉnh
                                            sửa Phản hồi của người sử dụng.

                                            <br/>
                                            - Người sử dụng đồng ý rằng http://video.mocha.com.vn/ và những bên tham gia
                                            cung cấp dịch vụ đều
                                            không cung cấp những tư vấn chuyên khoa, việc sử dụng những thông tin tư vấn
                                            đó
                                            hoặc bất kỳ thông tin nào khác đều thuộc quyền quyết định của người sử dụng
                                            và các
                                            yếu tố rủi ro của nó sẽ không thuộc trách nhiệm của
                                            http://video.mocha.com.vn/. Nếu người sử dụng
                                            đặt niềm tin vào những thông tin trên http://video.mocha.com.vn/ hãy thực
                                            hiện trên cơ sở sự hiểu
                                            biết của người sử dụng và hoàn toàn chịu trách nhiệm về các nguy cơ, hậu quả
                                            có
                                            thể xảy ra.

                                            <br/>
                                            - Không sử dụng trái phép mật khẩu, khóa mật mã và thông tin riêng của thành
                                            viên
                                            khác trên http://video.mocha.com.vn/. Không gây cản trở việc sử dụng bình
                                            thường tài khoản, mật
                                            khẩu của người sử dụng khác.

                                            <br/>
                                            - Không xâm phạm, tiếp cận hay sử dụng bất kỳ phần nào trong máy chủ của
                                            http://video.mocha.com.vn/.
                                            Nghiêm cấm mọi hành vi lợi dụng lỗi hệ thống để trục lợi cá nhân gây thiệt
                                            hại đến
                                            nhà cung cấp dịch vụ.

                                            <br/>
                                            <br/>
                                            <b>III. QUYỀN, TRÁCH NHIỆM NHÀ CUNG CẤP DỊCH VỤ MẠNG XÃ HỘI
                                                http://video.mocha.com.vn/ </b>
                                            <br/>
                                            <b>1. Quyền của nhà cung cấp dịch vụ mạng xã hội
                                                http://video.mocha.com.vn/ </b>
                                            <br/>
                                            - Nhà cung cấp có toàn quyền xóa bỏ bất kỳ thông tin nào cũng như thay đổi
                                            giao
                                            diện, sự trình bày, thành phần hoặc chức năng, nội dung của trang
                                            http://video.mocha.com.vn/ tại
                                            bất kỳ thời điểm nào mà không cần báo trước.

                                            <br/>
                                            - Nhà cung cấp có toàn quyền thay đổi những nội dung trong Thỏa thuận này mà
                                            không
                                            cần báo trước. Với việc tiếp tục sử dụng http://video.mocha.com.vn/ sau
                                            những sửa đổi đó, người
                                            sử dụng mặc nhiên đồng ý chấp hành các nội dung đã được sửa đổi này.

                                            <br/>
                                            - Nhà cung cấp được quyền giữ lại hoặc tiết lộ bất kì nội dung nào đưa lên,
                                            bao
                                            gồm cả các thông tin cá nhân khi đăng ký tài khoản, nếu pháp luật, cơ quan
                                            có thẩm
                                            quyền yêu cầu.

                                            <br/>
                                            - Nhà cung cấp có toàn quyền từ chối truy cập của người sử dụng vào
                                            http://video.mocha.com.vn/ hoặc
                                            bất kỳ phần nào của website/wapsite ngay lập tức mà không cần báo trước nếu
                                            chúng
                                            tôi cho rằng người sử dụng đã vi phạm bất cứ quy định nào trong Thỏa thuận
                                            này.
                                            <br/>
                                            - Nhà cung cấp có quyền xử lý các thông tin đăng tải cho phù hợp với thuần
                                            phong
                                            mỹ tục, các quy tắc đạo đức và các quy tắc đảm bảo an ninh quốc gia. Theo
                                            đó, chúng
                                            tôi có toàn quyền cho phép hoặc không cho phép bài viết của người sử dụng
                                            xuất hiện
                                            hay tồn tại trên http://video.mocha.com.vn/.

                                            <br/>
                                            - Nhà cung cấp được quyền sử dụng, tiết lộ, áp dụng và sửa đổi bất kỳ ý
                                            tưởng, khái
                                            niệm, cách thức, đề xuất, gợi ý, bình luận hoặc hình thức thông báo nào khác
                                            mà
                                            người sử dụng cung cấp cho chúng tôi (“Phản hồi”) một cách hoàn toàn miễn
                                            phí.

                                            <br/>
                                            - Nhà cung cấp có quyền không xác thực bất cứ một vấn đề, biến cố hoặc sự
                                            kiện nào
                                            và với bất kỳ mục đích nào, tương ứng với bất kỳ người, sản phẩm hoặc dịch
                                            vụ nào
                                            của một bên thứ ba.

                                            <br/>
                                            - Nhà cung cấp không bảo đảm mỗi phần của http://video.mocha.com.vn/ không
                                            bị gián đoạn, không có
                                            lỗi, không có virus, đúng giờ, an toàn, chính xác, ổn định hay bất kỳ nội
                                            dung nào
                                            là an toàn cho người sử dụng.

                                            <br/>
                                            - Nhà cung cấp có quyền từ chối xuất bản, loại bỏ hoặc ngăn truy cập tới bất
                                            kỳ
                                            Nội dung nào mà người sử dụng đã cung cấp trên http://video.mocha.com.vn/
                                            vào bất cứ lúc nào, với
                                            bất cứ lý do nào, có hoặc không có thông báo.

                                            <br/>
                                            <br/>
                                            <b>2. Trách nhiệm của nhà cung cấp dịch vụ mạng xã hội
                                                http://video.mocha.com.vn/</b>
                                            <br/>
                                            - Cung cấp dịch vụ mạng xã hội http://video.mocha.com.vn/ theo đúng những
                                            cam kết, mục tiêu đã đề
                                            ra trong Thỏa thuận này.

                                            <br/>
                                            - Kiểm soát các nội dung được người sử dụng đưa lên trang nhằm đảm bảo các
                                            nội dung
                                            này không vi phạm các quy định trong Thỏa thuận này và các quy định khác của
                                            pháp
                                            luật nước Cộng hòa xã hội chủ nghĩa Việt Nam.

                                            <br/>
                                            - Tất cả thông tin chỉ dành cho việc tham khảo tổng quát và thảo luận với
                                            cộng đồng
                                            của người sử dụng. http://video.mocha.com.vn/ không nhận bất cứ trách nhiệm
                                            nào về những thông tin
                                            đó.

                                            <br/>
                                            - Toàn bộ trách nhiệm pháp lý của nhà cung cấp nằm trong diễn đàn và bị giới
                                            hạn
                                            bởi pháp luật, vì vậy Nhà cung cấp không chứng thực, bảo đảm hoặc bảo hành
                                            cho bất
                                            cứ thông tin, vật liệu, sản phẩm hoặc dịch vụ nào giao dịch thông qua
                                            http://video.mocha.com.vn/.

                                            <br/>
                                            - Trong bất kỳ trường hợp nào, Nhà cung cấp cũng không chịu trách nhiệm pháp
                                            lý
                                            về người sử dụng hoặc bất kỳ cá nhân, tổ chức nào cho những tổn thất, dù là
                                            trực
                                            tiếp, gián tiếp, hoặc là hậu quả của sự kiện khác, kể cả thiệt hại về kinh
                                            tế phát
                                            sinh từ việc sử dụng hoặc không thể sử dụng thông tin trên
                                            http://video.mocha.com.vn/, ngay cả khi
                                            http://video.mocha.com.vn/ không có khuyến cáo về khả năng xảy ra các tổn
                                            thất đó.

                                            <br/>
                                            - Nhà cung cấp không chịu trách nhiệm nếu thông tin được các thành viên của
                                            mạng
                                            đưa lên không được cập nhật theo mong muốn của người sử dụng. Theo đó, Nhà
                                            cung
                                            cấp không chịu trách nhiệm về các thông tin mà thành viên của
                                            http://video.mocha.com.vn/ đăng tải
                                            lên.

                                            <br/>
                                            - Các liên kết trên http://video.mocha.com.vn/ có thể dẫn người sử dụng tới
                                            các web/wapsite khác
                                            và người sử dụng thừa nhận và đồng ý rằng http://video.mocha.com.vn/ không
                                            chịu trách nhiệm về sự
                                            chính xác hoặc giá trị của bất kỳ thông tin nào do các web/wapsite liên kết
                                            cung
                                            cấp.
                                            <br/>
                                            - Không cung cấp thông tin của người sử dụng cho bên thứ 3.
                                            <br/>
                                            <br/>
                                            <b>IV. CƠ CHẾ XỬ LÝ ĐỐI VỚI THÀNH VIÊN VI PHẠM THỎA THUẬN CUNG CẤP VÀ SỬ
                                                DỤNG DỊCH VỤ
                                                MẠNG XÃ HỘI http://video.mocha.com.vn/ </b>
                                            <br/>
                                            - Nếu người sử dụng đã đăng ký là thành viên Mạng Xã hội
                                            http://video.mocha.com.vn/ nhưng không
                                            tuân thủ các quy định sử dụng này, người sử dụng sẽ bị dừng tư cách thành
                                            viên ngay
                                            lập tức mà không cần thông báo trước.

                                            <br/>
                                            - Tùy theo mức độ vi phạm, các thành viên sẽ được nhắc nhở bằng tin nhắn hay
                                            email
                                            hoặc xóa tài khoản vĩnh viễn mà không cần thông báo trước.

                                            <br/>
                                            - Tối đa với mỗi lỗi vi phạm, thành viên sẽ được nhắc nhở 1 lần. Nếu thành
                                            viên
                                            vẫn tái phạm thì bình luận sẽ bị xóa mà không báo trước.

                                            <br/>
                                            - Trường hợp người sử dụng vi phạm phạm pháp luật, gây hậu quả nghiêm trọng,
                                            Nhà
                                            cung cấp có thể đưa vụ việc ra cơ quan quản lý Nhà nước để xử lý theo quy
                                            định của
                                            pháp luật nước Cộng hòa xã hội chủ nghĩa Việt Nam.
                                            <br/>
                                            <br/>
                                            <b>V. CẢNH BÁO VỀ CÁC RỦI RO KHI LƯU TRỮ, TRAO ĐỔI VÀ CHIA SẺ THÔNG TIN TRÊN
                                                MẠNG
                                            </b>
                                            <br/>
                                            - Dịch vụ mạng xã hội
                                            http://video.mocha.com.vn/
                                            cho phép người sử dụng tải lên, đệ trình, lưu trữ, gửi hoặc nhận nội dung.
                                            Người
                                            sử dụng giữ mọi quyền sở hữu trí tuệ đối với nội dung đó. Tuy nhiên, nhà
                                            cung cấp
                                            không đảm bảo những thông tin người sử dụng tải lên, đệ trình, lưu trữ, gửi
                                            trên
                                            http://video.mocha.com.vn/
                                            là không có rủi ro về an toàn thông tin.

                                            <br/>
                                            - Nhà cung cấp có thể phân tích nội dung của người sử dụng (bao gồm cả
                                            email) để
                                            cung cấp cho người sử dụng những tính năng sản phẩm có liên quan đến cá
                                            nhân, chẳng
                                            hạn như kết quả tìm kiếm tùy chỉnh, quảng cáo tùy chỉnh cũng như phát hiện
                                            spam
                                            và phần mềm độc hại. Phân tích này diễn ra khi nội dung được gửi, nhận cũng
                                            như
                                            khi được lưu trữ.

                                            <br/>
                                            - Nếu người sử dụng có tài khoản trên
                                            http://video.mocha.com.vn/
                                            , nhà cung cấp có thể hiển thị tên Profile (tên hiển thị), ảnh Profile (ảnh
                                            hiển
                                            thị) của người sử dụng và các hành động người sử dụng thực hiện trên
                                            http://video.mocha.com.vn/
                                            hoặc trên các ứng dụng của
                                            http://video.mocha.com.vn/
                                            (chẳng hạn như các bài đánh giá người sử dụng viết và những nhận xét người
                                            sử dụng
                                            đăng).

                                            <br/>
                                            <br/>
                                            <b>VI. CƠ CHẾ GIẢI QUYẾT KHIẾU NẠI, TRANH CHẤP GIỮA CÁC THÀNH VIÊN MẠNG XÃ
                                                HỘI VỚI NHÀ
                                                CUNG CẤP DỊCH VỤ MẠNG XÃ HỘI
                                                http://video.mocha.com.vn/
                                                HOẶC VỚI CÁC TỔ CHỨC, CÁ NHÂN KHÁC </b>
                                            <br/>
                                            - Mọi khiếu nại, tranh chấp giữa các thành viên Mạng xã hội
                                            http://video.mocha.com.vn/
                                            phải được gửi trực tiếp tới nhà cung cấp dịch vụ Mạng xã hội
                                            http://video.mocha.com.vn/
                                            qua các hình thức email, số hotline hoặc gặp trực tiếp.

                                            <br/>
                                            - Sau khi tiếp nhận khiếu nại của các thành viên Mạng xã hội
                                            http://video.mocha.com.vn/, nhà cung cấp dịch
                                            vụ Mạng xã hội phải có phản hồi chậm nhất trong vòng 72 giờ kể từ thời điểm
                                            tiếp
                                            nhận khiếu nại, tranh chấp.

                                            <br/>
                                            - Trong trường hợp các bên tham gia khiếu nại, tranh chấp chưa thỏa mãn với
                                            kết
                                            quả giải quyết khiếu nại của nhà cung cấp thì có thể gửi lại yêu cầu giải
                                            quyết
                                            khiếu nại tối đa 03 (ba) lần. Nếu quá 03 lần mà khiếu nại vẫn chưa được giải
                                            quyết
                                            dứt điểm thì căn cứ vào phạm vi và mức độ vi phạm của các bên, nhà cung cấp
                                            có toàn
                                            quyền xử lý khiếu nại, tranh chấp trên cơ sở các quy định tại Thỏa thuận
                                            này.
                                            <br/>
                                        </div>

                                    </TabPanel>

                                    <TabPanel tabId="five">
                                        <h3 className="mbl-h3">Chính sách bảo mật thông tin</h3>

                                        <div className="service-info" id="info" style={{display: 'block'}}>
                                            - Để đảm bảo công khai, khi khách hàng gửi yêu cầu đăng ký tham gia website
                                            (khi
                                            không truy cập dịch vụ 3G), Viettel sẽ thông báo nội dung thu thập thông tin
                                            sau
                                            đây để khách hàng xem xét, lựa chọn có tham gia sử dụng dịch vụ hay không.
                                            Gồm có:

                                            <br/>
                                            <p className="content_lv2">
                                                + Mục đích và phạm vi thu thập:</p>
                                            <p className="content_lv3">
                                                • Việc thu thập dữ liệu chủ yếu trên trang bao gồm: tên sử dụng - user
                                                (là số điện
                                                thoại của khách hàng) và mật khẩu. Đây là các thông tin mà
                                                http://video.mocha.com.vn/
                                                cần khách hàng cung cấp bắt buộc khi đăng ký sử dụng dịch vụ trên
                                                website nhằm đảm
                                                bảo quyền lợi cho khách hàng.
                                            </p>
                                            <p className="content_lv3">
                                                • Trong trường hợp khách hàng sử dụng dịch vụ 3G của Viettel, khi truy
                                                cập vào trang
                                                web, hệ thống có hỗ trợ tính năng nhận diện thuê bao giúp khách hàng
                                                không phải
                                                đăng nhập và đăng ký thông tin trước khi sử dụng dịch vụ. Nếu người dùng
                                                sử dụng
                                                mạng wifi hoặc hệ thống không nhận diện được thuê bao, người dùng sẽ
                                                được cấp tài
                                                khoản với tên sử dụng - user (là số điện thoại của khách hàng) và mật
                                                khẩu để truy
                                                cập và sử dụng dịch vụ.
                                            </p>
                                            <p className="content_lv3">
                                                • Các Khách hàng sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt
                                                động sử dụng
                                                dịch vụ dưới tên đăng ký, mật khẩu của mình. Ngoài ra, khách hàng có
                                                trách nhiệm
                                                thông báo kịp thời cho
                                                http://video.mocha.com.vn/
                                                về những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ
                                                tên đăng
                                                ký và mật khẩu của bên thứ ba để có biện pháp giải quyết phù hợp.
                                            </p>
                                            <p className="content_lv2">
                                                + Phạm vi sử dụng thông tin:
                                                http://video.mocha.com.vn/
                                                sử dụng thông tin khách hàng cung cấp để:</p>
                                            <p className="content_lv3">
                                                • Gửi các thông báo về cập nhật các nội dung phim mới.</p>
                                            <p className="content_lv3">
                                                • Liên lạc và giải quyết với khách hàng trong những trường hợp đặc
                                                biệt.</p>
                                            <p className="content_lv3">
                                                • Không sử dụng thông tin cá nhân của khách hàng ngoài mục đích xác nhận
                                                và liên
                                                hệ có liên quan đến giao dịch tại
                                                http://video.mocha.com.vn/.
                                            </p>
                                            <p className="content_lv3">
                                                •Trong trường hợp có yêu cầu của pháp luật:
                                                http://video.mocha.com.vn/
                                                có trách nhiệm hợp tác cung cấp thông tin cá nhân khách hàng khi có yêu
                                                cầu từ cơ
                                                quan tư pháp bao gồm: Viện kiểm sát, tòa án, cơ quan công an điều tra
                                                liên quan
                                                đến hành vi vi phạm pháp luật nào đó của khách hàng. Ngoài ra, không ai
                                                có quyền
                                                xâm phạm vào thông tin cá nhân của khách hàng.
                                            </p>
                                            <p className="content_lv2">
                                                + Thời gian lưu trữ thông tin:</p>
                                            <p className="content_lv3">
                                                • Dữ liệu cá nhân của khách hàng sẽ được lưu trữ cho đến khi có yêu cầu
                                                hủy bỏ hoặc
                                                tự khách hàng đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường
                                                hợp thông
                                                tin cá nhân khách hàng sẽ được bảo mật trên máy chủ của
                                                http://video.mocha.com.vn/.
                                            </p>
                                            <p className="content_lv2">
                                                + Địa chỉ của đơn vị thu thập và quản lý thông tin cá nhân:</p>
                                            <p className="content_lv3">
                                                • Công ty/Tổ chức: Công ty Truyền thông Viettel</p>
                                            <p className="content_lv3">
                                                • Địa chỉ: Tòa nhà The Light, CT2- Tố Hữu, Hà Nội</p>
                                            <p className="content_lv3">
                                                • Tel: 098.3542548 &nbsp;Email: hangnt1@viettel.com.vn</p>
                                            <p className="content_lv2">
                                                + Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá
                                                nhân của
                                                mình.
                                            </p>
                                            <p className="content_lv3">
                                                • Khách hàng có quyền tự kiểm tra, cập nhật, điều chỉnh hoặc hủy bỏ
                                                thông tin cá
                                                nhân của mình bằng cách đăng nhập vào tài khoản và chỉnh sửa thông tin
                                                cá nhân hoặc
                                                yêu cầu
                                                http://video.mocha.com.vn/
                                                thực hiện việc này.

                                                <br/>
                                            </p>
                                            <p className="content_lv2">
                                                + Cam kết bảo mật thông tin cá nhân khách hàng:</p>
                                            <p className="content_lv3">
                                                • Thông tin cá nhân của khách hàng cam kết bảo mật tuyệt đối theo chính
                                                sách bảo
                                                vệ thông tin cá nhân. Việc thu thập và sử dụng thông tin của mỗi khách
                                                hàng chỉ
                                                được thực hiện khi có sự đồng ý của khách hàng đó trừ những trường hợp
                                                pháp luật
                                                có quy định khác.
                                            </p>
                                            <p className="content_lv3">
                                                • Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ 3
                                                nào về thông
                                                tin cá nhân của khách hàng khi không có sự cho phép đồng ý.
                                            </p>
                                            <p className="content_lv3">
                                                • Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến
                                                mất dữ liệu
                                                cá nhân khách hàng,
                                                http://video.mocha.com.vn/
                                                sẽ có trách nhiệm thông báo vụ việc cho cơ quan chức năng điều tra xử lý
                                                kịp thời
                                                và thông báo cho khách hàng được biết.
                                            </p>
                                            <p className="content_lv3">
                                                •
                                                http://video.mocha.com.vn/
                                                yêu cầu các cá nhân khi tham gia sử dụng dịch vụ tại địa điểm/nhãn hàng,
                                                phải cung
                                                cấp mã khuyến mãi, hoặc nếu cung cấp số điện thoại để xác thực phải chịu
                                                trách nhiệm
                                                về tính pháp lý của những thông tin trên. Ban quản trị
                                                http://video.mocha.com.vn/
                                                không chịu trách nhiệm cũng như không giải quyết mọi khiếu nại có liên
                                                quan đến
                                                quyền lợi của khách hàng đó nếu xét thấy tất cả thông tin cá nhân của
                                                khách hàng
                                                đó cung cấp khi đăng ký ban đầu là không chính xác.
                                            </p>
                                            - Ngoài ra, Viettel xây dựng hệ thống kỹ thuật và các giải pháp nhằm đảm bảo
                                            an
                                            toàn, an ninh cho thông tin cá nhân:

                                            <br/>
                                            <p className="content_lv2">
                                                + Công ty Truyền thông Viettel – Chi nhánh Tập đoàn Viễn thông Quân đội
                                                chịu trách
                                                nhiệm về mật khẩu và bản quyền phần mềm thiết kế, phần mềm chống virus,
                                                các kỹ thuật
                                                đảm bảo cho trang tin điện tử hoạt động, bảo trì hệ thống, xử lý kỹ
                                                thuật khi có
                                                sự cố xẩy ra.
                                            </p>
                                            <p className="content_lv2">
                                                + Về cơ sở dữ liệu (CSDL): Hệ thống sử dụng CSDL MSSQL đã được update
                                                bản vá an
                                                toàn thông tin, thu hồi các quyền không cần thiết, đảm bảo các tài khoản
                                                được cấp
                                                đúng và đủ quyền nhằm ngăn chặn nguy cơ tài khoản bị chiếm quyền sử
                                                dụng. Cấu hình
                                                đảm bảo tuân thủ các quy định về an toàn thông tin.
                                            </p>
                                            <p className="content_lv2">
                                                + Hệ điều hành máy chủ và mô hình mạng hệ thống đáp ứng và tuân thủ đúng
                                                quy hoạch
                                                và quy định về an toàn thông tin.
                                            </p>
                                            <p className="content_lv2">
                                                + Về checklist giám sát và KPI: Hệ thống đã có công cụ giám sát và cảnh
                                                báo ứng
                                                dụng tự động, kiểm tra một phút một lần và gửi cảnh báo qua tin nhắn cho
                                                Quản trị
                                                viên. Hệ thống cũng đã có log ứng dụng, đảm bảo log theo thời gian thực
                                                và được
                                                hiệu kiểm hiệu năng, đảm bảo đáp ứng yêu cầu hiệu năng.
                                            </p>
                                            <p className="content_lv2">
                                                + Cơ chế dự phòng và phân tải hệ thống: Hệ thống dự phòng theo cơ chế
                                                active – standby
                                                trong đó server chạy CMS và Media đã dựng dự phòng Web Server, trên Web
                                                Server cũng
                                                đã dựng dự phòng CMS và Digital.
                                            </p>
                                            <p className="content_lv2">
                                                + Trong trường hợp hệ thống thông tin bị tấn công làm phát sinh nguy cơ
                                                mất thông
                                                tin của khách hàng, Viettel sẽ thông báo cho cơ quan chức năng trong
                                                vòng 24 (hai
                                                mươi bốn) giờ sau khi phát hiện sự cố.
                                            </p>
                                            - Có cơ chế tiếp nhận và giải quyết khiếu nại của khách hàng liên quan đến
                                            việc
                                            thông tin cá nhân bị sử dụng sai mục đích hoặc phạm vi đã thông báo:

                                            <br/>
                                            <p className="content_lv2">
                                                + Khi khách hàng có bất kỳ khiếu nại liên quan đến việc sử dụng sai mục
                                                đích hoặc
                                                phạm vi đã thông báo có thể khiếu nại cho Viettel bằng hình thức gửi thư
                                                điện tử
                                                trong phần liên hệ Ban quản trị website.
                                            </p>
                                            <p className="content_lv2">
                                                + Viettel giải quyết khiếu nại và trả lời cho khách hàng trong thời gian
                                                không quá
                                                10 ngày làm việc.
                                            </p>
                                            - Có cơ chế định kỳ kiểm tra, cập nhật và điều chỉnh thông tin cá nhân khi
                                            chủ thể
                                            thông tin yêu cầu kiểm tra, cập nhật, điều chỉnh hoặc hủy bỏ thông tin cá
                                            nhân của
                                            mình.

                                            <br/>
                                        </div>

                                    </TabPanel>

                                    <TabPanel tabId="six">

                                        <h3 className="mbl-h3">Giới thiệu dịch vụ</h3>

                                        <div className="service-info" id="help">
                                            <b>
                                                <p>
                                                    Mạng xã hội chia sẻ video Mocha video
                                                </p>
                                            </b>
                                            <br/>
                                            <p>
                                                Đơn vị chủ quản: Công ty Truyền thông Viettel (Viettel Media).
                                            </p>
                                            <br/>
                                            <p>
                                                Trụ sở: Tầng 4, tòa nhà The Light, đường Tố Hữu, quận Nam Từ Liêm, Hà
                                                Nội.
                                            </p>
                                            <br/>
                                            <p>
                                                Chăm sóc khách hàng: 198 (Miễn phí)
                                            </p>
                                            <br/>
                                            <p>
                                                Email: mochavideo@viettel.com.vn
                                            </p>
                                            <br/>
                                        </div>

                                    </TabPanel>

                                </div>
                            </div>
                        </div>

                    </Tabs>

                    <ModalPartial isOpen={this.state.isShow}
                                  body={'Bạn muốn đăng xuất khỏi tài khoản này?'}
                                  toggle={this.toggle}
                    />

                </div>
            </div>

        );
    }
}

export default Profile;
