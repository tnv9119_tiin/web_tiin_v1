import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class QuaySo19 extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta charSet="utf-8"/>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                            <title>Thể lệ chương trình quay số</title>
                            <link
                                href="http://live84.keeng.net/playnow/images/static/web_static/tet/css/Style.css"
                                rel="stylesheet" type="text/css"/>
                    </head>
                    <body className="content-thele-game" style={{padding:'unset'}}>
                    <div className="header">
                        <p>THỂ LỆ CHƯƠNG TRÌNH QUAY SỐ</p>
                        <p>TRÚNG QUÀ TẶNG ĐẶC BIỆT</p>
                        <p>GAME "HEO VÀNG MAY MẮN"</p>
                    </div>
                    <div className="box-content-1" style={{paddingTop: '100px'}}>
                        <p>
                            <i>
                                Chường trình quay số trúng quà của game "Heo vàng may mắn"
                                là cơ hội tuyệt vời mà Viettel muốn dành cho Quý khách hàng
                                đã tham gia game để trúng một bộ quà tặng hết sức giá trị
                                và thời lượng bao gồm 1 iPhone XS Max 256Gb và một Apple Watch Series 4 GPS (44mm)
                                ngay sau khi thời gian chơi game kết thúc.
                            </i>
                        </p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>1. Thời gian diễn ra chương trình</b></p>
                        <p>
                            - Thời gian dự kiến chương trình từ ngày 28/01/2019 đến 05/03/2019.
                        </p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>2. Đối tượng tham gia</b></p>
                        <p>
                            - Tất cả khách hàng là thuê bao di động trả trước, trả sau Viettel
                            đang hoạt động 02 chiều tại thời điểm tham gia chơi và thời điểm nhận quà.
                        </p>
                        <p>
                            - Chương trình không áp dụng cho thuê bao Dcom, Homephone.
                        </p>
                    </div>
                    <div className="box-the-le">
                        <p className="no-indent"><b>3. Nội dung chương trình</b></p>
                        <p>
                            <i className="fas fa-arrows-alt"></i> Vào thời gian game “Heo vàng may mắn” kết thúc,
                            Viettel sẽ tổng hợp tất cả các mã quay số trúng quà của khách hàng đã giành được trong game
                            để tiến hành quay số trúng một bộ quà gồm 01 iPhone Xs Max 256Gb và 01 Apple Watch Series 4
                            GPS (44mm).
                        </p>
                        <p>
                            <b><i><i className="fas fa-arrows-alt"></i> Thông báo trúng quà:</i></b>
                        </p>
                        <p>
                            - Nhân viên Viettel sẽ liên hệ với khách hàng trúng quà để làm các thủ tục nhận quà. Khách
                            hàng có trách nhiệm cung cấp các thông tin, giấy tờ cá nhân (chứng minh thư, hộ khẩu), nộp
                            thuế theo quy định của Nhà nước để nhận quà. Tên khách hàng nhận được quà đặc biệt này sẽ
                            được công bố trên website vietteltelecom.vn.
                        </p>
                        <p>
                            <b><i className="fas fa-arrows-alt"></i> Thời gian địa điểm, cách thức và thủ tục trao
                                quà</b>
                        </p>
                        <p>
                            - Bộ quà tặng sẽ được trao cho khách hàng trong vòng 45 ngày kể từ khi kết thúc chương
                            trình.
                        </p>
                        <p>
                            - Tổng công ty Viễn thông Viettel liên hệ với khách hàng nhận được quà, hướng dẫn khách hàng
                            các thủ tục nhận quà và tổ chức trao quà.
                        </p>
                        <p className="indent-2">
                            + Thủ tục cần có để nhận quà: Người nhận được quà là hiện vật phải là chính chủ thuê bao:
                            Khi nhận quà chủ thuê bao phải xuất trình CMND/Hộ chiếu còn hiệu lực theo quy định (CMND
                            không quá 15 năm kể từ ngày được cấp, hộ chiếu phải còn hiệu lực trong vòng 06 tháng tính
                            đến thời điểm nhận quà hoặc các giấy tờ khác thay thế nhưng phải có ảnh và có xác nhận của
                            chính quyền địa phương trong vòng 06 tháng tính đến thời điểm nhận quà). Nếu ủy quyền cho
                            người khác nhận quà thì người đi nhận phải có CMND bản photo và bản gốc của chính chủ trúng
                            quà, CMND bản photo và bản gốc của người được ủy quyền, giấy ủy quyền đi nhận giải quà của
                            người trúng quà. Trường hợp người trúng quà đặc biệt hiện vật không phải là chính chủ thuê
                            bao: Khi nhận, người nhận được quà phải xuất trình CMND, cung cấp ít nhất 05 số điện thoại
                            thường xuyên liên lạc trong vòng 06 tháng gần nhất.
                        </p>
                        <p className="indent-2">
                            + Quà tặng sẽ được trao tại địa chỉ của khách hàng hoặc tại các cửa hàng Viettel tỉnh/TP.
                        </p>
                        <p className="indent-2">
                            + Trong trường hợp quá 30 ngày kể từ ngày kết thúc chương trình mà Tổng Công ty Viễn thông
                            Viettel không thể liên lạc được với khách hàng hoặc khách hàng không liên hệ với Tổng Công
                            ty Viễn thông Viettel để làm thủ tục nhận quà thì quà tặng sẽ không được trao.
                        </p>
                        <p>
                            <b><i className="fas fa-arrows-alt"></i> Các quy định khác:</b>
                        </p>
                        <p>
                            - Khách hàng tham gia chương trình quay số trúng bộ quà đặc biệt Game “Heo vàng may mắn” vẫn
                            được hưởng quyền lợi từ các chương trình khuyến mại khác của Tổng Công ty Viễn thông
                            Viettel (nếu đủ điều kiện).
                        </p>
                        <p>
                            - Tổng Công ty Viễn thông Viettel hoàn toàn chịu trách nhiệm trong việc quản lý tính chính
                            xác của bằng chứng xác định trúng quà và đưa bằng chứng xác định trúng quà vào lưu thông
                            trong chương trình khuyến mại. Trường hợp bằng chứng xác định trúng quà của Tổng Công ty
                            phát hành có sai sót gây hiểu nhầm cho khách hàng trong việc trúng quà, Tổng Công ty Viễn
                            thông Viettel có trách nhiệm trao quà này cho khách hàng trúng quà. Việc tổ chức chương
                            trình khuyến mại phải đảm bảo tính khách quan và công khai.
                        </p>
                        <p>
                            - Tổng Công ty Viễn thông Viettel được sử dụng tên và hình ảnh của khách hàng trúng quà cho
                            mục đích quảng cáo thương mại.
                        </p>
                        <p>
                            - Trong trường hợp xảy ra tranh chấp liên quan đến chương trình khuyến mại này, Tổng Công
                            ty Viễn thông Viettel có trách nhiệm trực tiếp giải quyết, nếu không thỏa thuận được tranh
                            chấp sẽ được xử lý theo quy định của pháp luật.
                        </p>
                        <p>
                            - Quyết định của Ban Tổ chức (Viettel) là quyết định cuối cùng. Chi tiết liên hệ 198 (Miễn
                            phí).
                        </p>
                    </div>
                    </body>
                </div>
            );
    }
}

export default QuaySo19;
