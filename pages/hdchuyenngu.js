import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class Hdchuyenngu extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
                        <meta name="keywords" content="Hướng dẫn, sử dụng, tính năn, chuyển ngữ"/>
                        <meta property="og:site_name" content="HƯỚNG DẪN SỬ DỤNG TÍNH NĂNG CHUYỂN NGỮ"/>
                        <meta name="revisit" content="1 days"/>
                        <meta name="language" content="vi-VN"/>
                        <meta name="language" content="vi-VN"/>
                        <meta name="robots" content="index,follow,noodp"/>
                        <meta name="Googlebot" content="index,follow"/>
                        <title>HƯỚNG DẪN SỬ DỤNG TÍNH NĂNG CHUYỂN NGỮ</title>
                    </head>

                    <body style={{lineHeight: '30px'}}>
                    <div className="box-rule">
                        <h3>HƯỚNG DẪN SỬ DỤNG TÍNH NĂNG CHUYỂN NGỮ</h3>
                        <p><b>1. Cài đặt ngôn ngữ đích cần chuyển</b></p>
                        <p>- Ngôn ngữ đích: là ngôn ngữ mà tính năng chuyển ngữ sẽ hỗ trợ dịch sang cho bạn</p>
                        <p>- Mặc định hệ thống tự động cài đặt cho bạn: nếu bạn đang sử dụng ngôn ngữ tiếng Việt thì
                            ngôn ngữ đích mặc định là tiếng Việt, nếu bạn đang sử dụng ngôn ngữ khác thì ngôn ngữ đích
                            mặc định là tiếng Anh</p>
                        <p>- Để thay đổi ngôn ngữ đích, bạn vào Cài đặt / Chuyển ngữ và lựa chọn ngôn ngữ mong muốn</p>
                        <p><b>2. Sử dụng tính năng</b></p>
                        <p>- Tính năng hỗ trợ chuyển ngữ sang ngôn ngữ đích với tin nhắn văn bản</p>
                        <p>- Để chuyển ngữ một tin nhắn bất kỳ, bạn tiến hành giữ tin nhắn đó</p>
                        <p>- Khi màn hình pop-up hiện lên, bạn chọn nút “Dịch”</p>
                        <p>- Tin nhắn sẽ được tự động chuyển sang ngôn ngữ đích</p>
                        <p><img width="70%" alt="hướng dẫn sử dụng tính năng chuyển ngữ"
                                src="http://cdn2.keeng.net/playnow/images/20181002/hdchuyenngu.png"/></p>
                    </div>
                    </body>
                </div>
            );
    }
}

export default Hdchuyenngu;
