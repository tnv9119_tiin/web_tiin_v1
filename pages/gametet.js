import React, {Component} from 'react';
import Link from "next/link";
import Loading from "../src/components/Global/Loading/LoadingScreen";
import {DOMAIN_IMAGE_STATIC} from "../src/config/Config";

 class Gametet extends Component {
    constructor(props) {
        super(props);
    }
    render() {
            return (
                <div>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <title>Game Tết – giải thưởng 10,7 tỷ đồng</title>

                        <meta property="al:ios:url"
                              content="mocha://mochavideo?ref=http%3A%2F%2Fm.video.mocha.com.vn%2Fgametet.html"/>
                        <meta property="al:ios:app_store_id" content="946275483"/>
                        <meta property="al:ios:app_name" content="Mocha"/>

                        <meta property="al:android:url"
                              content="mocha://mochavideo?ref=http%3A%2F%2Fm.video.mocha.com.vn%2Fgametet.html"/>
                            <meta property="al:android:package" content="com.viettel.mocha.app"/>
                                <meta property="al:android:app_name" content="Mocha"/>

                                    <meta property="og:title" content="Game Tết –  giải thưởng 10,7 tỷ đồng"/>
                                    <meta property="og:type" content="website"/>

                                    <meta property="og:url" content="http://m.video.mocha.com.vn/gametet.html"/>
                                    <meta property="fb:app_id" content="482452638561234"/>

                                    <meta property="og:image"
                                          content="http://live84.keeng.net/playnow/images/static/web_static/tet/img/Thumbnail_1.jpg"/>
                                        <meta property="og:description"
                                              content="19 iPhone XS Max, 19 Apple Watch,... Chơi ngay!"/>
                                            <meta
                                                content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"
                                                name="viewport"/>

                                                <link href="http://live84.keeng.net/playnow/images/static/web_static/tet/css/Style.css"
                                                    rel="stylesheet" type="text/css"/>
                    </head>
                    <body>
                    <div className="content">
                        <div className="logo-viettel">
                            <img
                                src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/logo-viettiel.png"
                                title="" alt=""/>
                        </div>
                        <div className="box-heo-vang">
                            <div className="heo-vang">
                                <img
                                    src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/Vector-Smart-Object-copy-27.png"
                                    title="" alt=""/>
                            </div>
                            <div className="text-button">
                                <a href="http://mocha.com.vn/app" title="Tải app">
                                    <img
                                        src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/Tai-app.png"
                                        title="" alt=""/>
                                </a>
                            </div>
                        </div>
                        <div className="tai-app-choi-game">
                            <img
                                src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/txtTaiApChoiGame.png"
                                title="" alt=""/>
                        </div>
                        <div className="product">
                            <table>
                                <tr>
                                    <td className="td-1">
                                        <img
                                            src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/product-1.png"
                                            title="" alt=""/>
                                    </td>
                                    <td><img
                                        src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/product-2.png"
                                        title="" alt=""/></td>
                                </tr>
                                <tr>
                                    <td><img
                                        src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/product-3.png"
                                        title="" alt=""/></td>
                                    <td className="td-4"><img
                                        src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/product-4.png"
                                        title="" alt=""/></td>
                                </tr>
                            </table>
                        </div>
                        <div className="box-btn">
                            <a href="http://mocha.com.vn/app" title="Tải app">
                                <img
                                    src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/btnTaiApp.png"
                                    alt="" title=""/>
                            </a>
                            <a href="http://mocha.com.vn/thelegametet.html" title="Thể lệ">
                                <img
                                    src="http://live84.keeng.net/playnow/images/static/web_static/tet/img/btnTheLe.png"
                                    alt="" title=""/>
                            </a>
                        </div>
                    </div>
                    </body>
                </div>
            );
    }
}

export default Gametet;
